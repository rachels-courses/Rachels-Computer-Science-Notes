\chapter{Iterators}

    When building data structures, you might be used to iterating through
    a list of elements like this:

\begin{lstlisting}[style=code]
for ( int i = 0; i < elementCount; i++ )
{
    element[i].DoThing();
}
\end{lstlisting}

    Or, for linked structures, like this:
    \footnote{In C++, \texttt{currentNode->DoThing();}}

\begin{lstlisting}[style=code]
Node currentNode = firstNode;
while ( current != null )
{
    currentNode.DoThing();
    currentNode = currentNode.next;
}
\end{lstlisting} 

    You can streamline accessing elements by using \textbf{Iterators},
    which allows access to one element at a time, without worrying about
    logic of iterating through the list manually: You just need to worry
    about if there is a \textit{next item}.

\begin{lstlisting}[style=code]
for ( Element el : list )
{
    iterator.DoThing();
}
\end{lstlisting}

    Anything you can iterate over
    in this manner is actually implementing an \textbf{Iterator}
    interface.

    You can also handle iterators more explicitly, as needed:

\begin{lstlisting}[style=code]
Iterator<Element> it = list.iterator();
while ( it.hasNext() )
{
    it.next().DoThing();
}
\end{lstlisting}

    But the bottom-line is, if you want to be able to use
    a for-each loop with your structure, it needs to implement
    the \textbf{Iterable} interface.

    \newpage
    \subsection{Adding Iterators to your data structure}
    
    Let's say you're building out a data structure called
    \texttt{BestListEver} and you want to make it iterable.
    
    First, you'll have to implement the \textbf{Iterable}
    interface in the \texttt{BestListEver} class itself,
    and second, you'll have to define your own
    \textbf{custom iterator} class that implements the \textbf{Iterator}
    interface.
    
        \subsubsection{Implementing the Iterable interface}
        
        The data structure itself will be the one with the 
        \textbf{Iterable} interface.
        
\begin{lstlisting}[style=code]
class BestListEver implements Iterable<>
{
    // Implementing the iterator() method
    public Iterator<> iterator()
    {
        return new BestIteratorEver<>( this );
    }

    // ... All your other class stuff
}
\end{lstlisting}

        The only method that the \textbf{Iterable} interface
        requires that you implement is this \texttt{iterator()}
        class, which returns an \textbf{Iterator} object
        that is used to access each element one at a time.
        
        
        \begin{center}
            \begin{tabular}{l p{10cm}}
                \textbf{Modifier and Type}  & \textbf{Method and Description} 
                \\ \\
                
                \texttt{Iterator$<$T$>$}    & \texttt{iterator()}
                \\                          & Returns an iterator over elements of type \texttt{T}.

            \end{tabular} ~\\ 
            
            \footnotesize
            Partial method summary for the \textbf{Iterator$<$E$>$} interface,
            from https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html
        \end{center}
        
        

        \subsubsection{Implementing the Iterator interface}
        
        When creating your own Iterator, you'll implement the
        \textbf{Iterator} interface. This interface
        requires the implementation of the methods 
        \texttt{hasNext()} and \texttt{next()}, and
        \texttt{remove()} can be implemented optionally.
        
        \begin{center}
            \begin{tabular}{l p{10cm}}
                \textbf{Modifier and Type}  & \textbf{Method and Description} 
                \\ \\
                
                \texttt{boolean}            & \texttt{hasNext()}
                \\
                                            & Returns \texttt{true} if the iterator has more elements.
                \\ \\
                \texttt{E}                  & \texttt{next()}
                \\                          & Returns the next element in the iteration.
                \\ \\
                \texttt{void}               & \texttt{remove()}
                \\                          & Removes from the underlying collection the last element
                                                returned by this iterator (optional operation).
            \end{tabular} ~\\ 
            
            \footnotesize
            Method summary for the \textbf{Iterator$<$E$>$} interface,
            from https://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html
        \end{center}
    
\begin{lstlisting}[style=code]
class BestIteratorEver<> implements Iterator<>
{
    BestIteratorEver<>( BestListEver list )
    {
        // Initialize the iterator cursor
        // (generally to first item of the list)
    }
    
    public boolean hasNext()
    {
        // Logic to see if there are items
        // next in the list
    }
    
    public T next()
    {
        // Logic to move the iterator cursor to the
        // next item in the list and return that element
    }
    
    public void remove()
    {
        // Logic to remove the element that the
        // iterator cursor is currently on
    }
}
\end{lstlisting}
    

