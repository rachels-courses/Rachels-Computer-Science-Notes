In previous programming courses you've probably worked with \textbf{arrays}
to store data. You've probably encountered out-of-bounds errors and
had to deal with array indices, moving items around, and generally
micro-managing your array's data as the program goes.

What we're going to do here is \textbf{wrap} our array - and
everything we need to work with it - inside of a class,
creating our first data structure: A slightly smarter fixed-length array.

% Array functionality and error checks, interface

\subsection{Functionality for our data type}

What kind of things do we want to \textit{do} to a data structure?
What is some functionality that others will need to store and retrieve
data, without having to see inside the class? How do we keep the data stored?
~\\

\paragraph{Creating the array:}
For example, if we weren't storing our array inside a class, we might
declare it in a program like this:

\begin{lstlisting}[style=code]
// Create an array + helper variables
const int ARR_SIZE = 100;
int totalItems = 0;
string data[ARR_SIZE];
\end{lstlisting}

With a fixed-length array, we have to keep track of the array size \texttt{ARRAY\_SIZE}
to ensure we don't go out-of-bounds in the array... valid indices will be \texttt{0}
until \texttt{ARRAY\_SIZE - 1}.

We are also keeping track of the \texttt{totalItems} in the array, because
even though we have an array with some size, it could be useful to know that
currently no data is stored in it. Then, when new data is added, we could
increment \texttt{totalItems} by 1.

\begin{center}
    An empty array: ~\\    
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{""} & \texttt{""} & \texttt{""} &
        \texttt{""} & \texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 0
\end{center}

\paragraph{Adding onto the array:}
Adding on to the array would mean choosing an index to put new data.
If we were wanting to fill the array from \textbf{left-to-right},
we would start at index 0, then 1, then 2, then 3, and so on.

The \texttt{totalItems} variable begins at 0. Once the first item is added,
it is then 1. Once a second item is added, it is then 2. Basically, the
\texttt{totalItems} variable corresponds to the \textit{next index to store
new data at}...

\begin{lstlisting}[style=code]
// Add new item
data[totalItems] = myNewData;
totalItems++;
\end{lstlisting}

\begin{center}
    One item added: ~\\    
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \cellcolor{colorblind_light_blue}\texttt{"Cats"} & \texttt{""} & \texttt{""} &
        \texttt{""} & \texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 1
\end{center}

\newpage
\paragraph{Removing items from the array:}
We may also want to remove an item at a specific index from the array.
There's not really a way to ``delete'' an item from an array, but we can
overwrite it, if we wanted to...

\begin{lstlisting}[style=code]
// Remove an item
int index;
cout << "Remove which index? ";
cin >> index;

data[index] = "";
totalItems--;
\end{lstlisting}

But if we did it this way we would create an array with gaps in it.
Additionally now \texttt{totalItems} doesn't align to the \textit{next index to insert at}.

\begin{center}
    Before removing an item: ~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \texttt{"Birds"} & \texttt{"Snakes"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 6
\end{center}

\begin{center}
    After removing an item: ~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \cellcolor{colorblind_light_blue}\texttt{""} & \texttt{"Snakes"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 4
\end{center}

We could design our array data structure this way, but to find a new
place to add an item we'd have to use a for loop to look for an empty
spot to use - this means every time we add a new item it would be less efficient.
~\\

Instead, we could design our arrays so that after a remove, we shift elements
backwards to ``overwrite'' the empty spot. This also solves our issue with
\texttt{totalItems} not aligning to the \textit{next-index-to-insert-at}.
~\\

\begin{center}
    After removing an item with a ``Shift Left'': ~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \cellcolor{colorblind_light_blue}\texttt{"Snakes"} & \texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 4
\end{center}

\paragraph{Searching for an item:}
Another thing a user of this data structure might want to do is search for an item -
is this thing stored in the array? If so, what is the index?
If the array is \textbf{unsorted} (we're not going to do sorting yet),
then really the only way to search for the item is to start at the beginning
and keep searching until the end. 

~\\ If we find a match, we return that index number.

~\\ If we get to the end of the loop and nothing has been returned,
that means it isn't in the array so we would have to return something
to symbolize ``it's not in the array'' -- such as returning \texttt{-1},
or perhaps throwing an exception.

\begin{center}
    Populated array: ~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \texttt{"Birds"} & \texttt{"Snakes"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 5
\end{center}

Searching through the array would look like this:

\begin{lstlisting}[style=code]
// Search for an item
string findme;
cout << "Find what? ";
getline( cin, findme );

int foundIndex;

for ( int i = 0; i < totalItems; i++ )
{
    if ( data[i] == findme )
    {
        foundIndex = i;   // Found it
        break;
    }
}

// Did not find it
foundIndex = -1;
\end{lstlisting}

Once we implement this structure within a class, we will turn this into
a function that returns data - this is just an example as if we were
writing a program in main().

\paragraph{Retrieving an item at some index:}
Besides adding, removing, and searching for items, users will
want to retrieve the data located at some index. This would be a simple
return once we're writing our class, but generally accessing \textit{item at index}
looks like:

\begin{lstlisting}[style=code]
// Display element at index
int index;
cout << "Which index? ";
cin >> index;
cout << data[index] << endl;
\end{lstlisting}

\paragraph{Visualizing the array:}
Another feature could be to display all elements of the array, which
might look something like this:

\begin{lstlisting}[style=code]
// Display all items
for ( int i = 0; i < totalItems; i++ )
{
    cout << i << ". " << data[i] << endl;
}
\end{lstlisting}



\subsection{Predicting errors/exceptions}

\paragraph{Invalid indices:}
When working with arrays one of the most common errors encountered
are \textbf{out-of-bounds} errors: When we have an index that is less
than 0, or equal to or greater than the size of the array...

\begin{lstlisting}[style=code]
// Out of bounds!
cout << data[-1]        << endl;    // error!
cout << data[ARR_SIZE]  << endl;    // error!
\end{lstlisting}

Additionally, if the user tries to access an index $\geq$ \texttt{totalItems}
then they would retrieve data that is invalid, though this wouldn't
crash the program if it is still less than \texttt{ARR\_SIZE}.

\paragraph{Array is full:}
With the fixed-length array, we could also run out of space, and we
would have to design a way to deal with it. For example,
if \texttt{totalItems} was equal to \texttt{ARR\_SIZE}, then we are out
of space and doing this would also cause an out-of-bounds error.

\begin{lstlisting}[style=code]
data[totalItems] = "Ferrets";       // error if full!
totalItems++;
\end{lstlisting}

As part of designing our data structure, we need to make sure we account
for reasonable scenarios that would cause the program to crash and
guard against logic errors. This would all be part of how we implement
the functionality.


\subsection{Creating a class to wrap the array}

For our wrapper class we will need the array and array size / item count
integers as part of the private member variables. The public functions
would include the ``interface'' of what the user will want to do with
the array, and we could also add private/protected helper functions.

\begin{center}
    \begin{tabular}{| l l l |} \hline
        \multicolumn{3}{|c|}{ \textbf{SmartFixedArray$<$TYPE$>$} } \\ \hline
        + \texttt{SmartFixedArray()}                            
        \\ & & \\
        + \texttt{PushFront( newData : TYPE )}                  & : & \texttt{void} \\
        + \texttt{PushBack( newData : TYPE ) }                  & : & \texttt{void} \\
        + \texttt{PushAtIndex( index : int, newData : TYPE )}   & : & \texttt{void}
        \\ & & \\
        + \texttt{PopFront()}                                   & : & \texttt{void} \\
        + \texttt{PopBack()}                                    & : & \texttt{void} \\
        + \texttt{PopAtIndex( index : int )}                    & : & \texttt{void}
        \\ & & \\   
        + \texttt{GetFront()}                                   & : & \texttt{TYPE\&} \\
        + \texttt{GetBack()}                                    & : & \texttt{TYPE\&} \\
        + \texttt{GetAtIndex( index : int )}                    & : & \texttt{TYPE\&}
        \\ & & \\   
        + \texttt{Search( item : T )}                           & : & \texttt{int} \\
        + \texttt{Clear()}                                      & : & \texttt{void} \\
        + \texttt{IsEmpty()}                                    & : & \texttt{bool} \\
        + \texttt{IsFull()}                                     & : & \texttt{bool} \\
        + \texttt{Size()}                                       & : & \texttt{int}
        \\ & & \\   
        - \texttt{ShiftLeft()}                                  & : & \texttt{int} \\
        - \texttt{ShiftRight()}                                 & : & \texttt{int} \\
        - \texttt{IsValidIndex( index : int )}                  & : & \texttt{bool}
            
        \\ \hline   
        - \texttt{m\_array}                                     & : & \texttt{TYPE[100]} \\
        - \texttt{m\_itemCount}                                 & : & \texttt{int} \\
        - \texttt{m\_arraySize}                                 & : & \texttt{const int} \\ \hline
    \end{tabular}
\end{center}

\newpage
        
The class declaration could look like this:
\begin{lstlisting}[style=code]
template <typename T>
class SmartFixedArray
{
public:
    SmartFixedArray();

    void PushBack( T newItem );
    void PushFront( T newItem );
    void PushAt( T newItem, int index );

    void PopBack();
    void PopFront();
    void PopAt( int index );

    T GetBack() const;
    T GetFront() const;
    T GetAt( int index ) const;

    int Search( T item ) const;
    void Display() const;
    int Size() const;
    bool IsEmpty() const;
    bool IsFull() const;

private:
    const int m_arraySize;
    T m_array[100];
    int m_itemCount;
    
    void ShiftLeft( int index );
    void ShiftRight( int index );
    bool IsValidIndex( int index );
};
\end{lstlisting}
~\\
For this example data structure, we are hard-coding the array size to 100 elements. 
It is unlikely that anyone would use this data structure for anything,
but I wanted to start off with a basic fixed-length array. Next,
we will move to a dynamic array, which \textit{will} be more useful.

~\\
With this class structure set up, lets look into how to
actually implement the functionality.

\newpage
\subsection{Constructor - Preparing the array to be used}
When the constructor is called, we need to set the value of \texttt{m\_arraySize}
as part of the initializer list since it is a const member and needs to be
initialized right away.

Otherwise, we can initialize \texttt{m\_itemCount} to 0 as well,
since are creating an empty array.

\begin{lstlisting}[style=code]
template <typename T>
SmartFixedArray<T>::SmartFixedArray()
    : m_arraySize( 100 )
{
    m_itemCount = 0;
}
\end{lstlisting}




\subsection{Helper functions}
The helper functions here are easier to implement, and we will be
using them in the main interface functions.

\subsubsection{int Size() const}
This function returns the value from \texttt{m\_itemCount} - the current
amount of items in the array.

\vspace{1cm}

\subsubsection{bool IsEmpty() const}
Returns \texttt{true} if the array is empty and \texttt{false} if not.
The array is empty if \texttt{m\_itemCount} is 0.

\vspace{1cm}

\subsubsection{bool IsFull() const}
Returns \texttt{true} if the array is full and \texttt{false} if not.
The array is full if \texttt{m\_itemCount} is equal to \texttt{m\_arraySize}.

\newpage
\subsubsection{bool IsValidIndex( int index )}
Returns \texttt{true} if the index is valid and \texttt{false} otherwise.

~\\ Valid index: The index is valid if $0 \leq $ \texttt{index} and \texttt{index} $ < $ \texttt{m\_itemCount}.

~\\ Invalid index: The index is invalid if \texttt{index} $<$ 0 or \texttt{index} $\geq$ \texttt{m\_itemCount}.

\begin{lstlisting}[style=code]
bool SmartFixedArray<T>::IsValidIndex( int index )
{
    return ( 0 <= index && index < m_itemCount );
}
\end{lstlisting}

\vspace{0.5cm}

\subsubsection{void ShiftRight( int index )}

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the array is full, then throw an exception.
    \item   If the index is invalid, then throw an exception.
\end{itemize}

\subparagraph{Functionality:}
\begin{itemize}
    \item   Iterate over the array, starting at the index of the first empty space.
            Moving backwards, copy each item over from the previous index.
            Loop until hitting the \texttt{index}.
\end{itemize}


\begin{lstlisting}[style=code]
void SmartFixedArray<T>::ShiftRight( int index )
{
    if ( !IsValidIndex( index ) )
    {
        throw out_of_range( "Index is out of bounds!" );
    }
    else if ( IsFull() )
    {
        throw length_error( "Array is full!" );
    }

    for ( int i = m_itemCount; i > index; i-- )
    {
        m_array[i] = m_array[i-1];
    }
}
\end{lstlisting}

\subsubsection{void ShiftLeft( int index )}

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the index is invalid, then throw an exception.
\end{itemize}

\subparagraph{Functionality:}
\begin{itemize}
    \item   Iterate over the array, starting at the index given and going
            until the last element. Copy each neighbor from the right
            to the current index until we get to the end.
\end{itemize}

\begin{lstlisting}[style=code]
void SmartFixedArray<T>::ShiftLeft( int index )
{
    if ( !IsValidIndex( index ) )
    {
        throw out_of_range( "Index is out of bounds!" );
    }

    for ( int i = index; i < m_itemCount-1; i++ )
    {
        m_array[i] = m_array[i+1];
    }
}
\end{lstlisting}

\vspace{1cm}

\subsubsection{void Display() const}
Iterates over all the elements of the array displaying each item.

\begin{lstlisting}[style=code]
void SmartFixedArray<T>::Display() const 
{
    for ( int i = 0; i < m_itemCount; i++ ) 
    {
        cout << i << ". " << m_array[i] << endl;
    }
}
\end{lstlisting}

\newpage
\subsubsection{int Search( T item ) const}
Iterates over all the elements of the array searching for the \texttt{item}.
If a match is found, it returns the index of that element.
Otherwise, if it finishes looping over the array and has not yet returned,
it means that nothing was found. Return -1 in this case, or
throw an exception (this is a design decision).

\begin{lstlisting}[style=code]
int SmartFixedArray<T>::Search( T item ) const
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( m_array[i] == item )
        {
            return i;
        }
    }
    throw runtime_error( "Could not find item!" );
}
\end{lstlisting}

\vspace{1cm}

\subsection{Push - adding items to the array}

We have three different Push functions in order to add an item to
different parts of the array, which will each require slightly
different logic:

\paragraph{PushBack:} Add a new item to the end of the list of elements...

\begin{center}
    Before: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \texttt{"Birds"} & \cellcolor{colorblind_light_blue}\texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 4
    ~\\~\\
    After: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & \texttt{"Dogs"} & \texttt{"Mice"} &
        \texttt{"Birds"} & \cellcolor{colorblind_light_blue}\texttt{"Ferrets"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 5
\end{center}

\newpage
\paragraph{PushFront:} Add a new item to the beginning of the list of elements.
This requires shifting everything forward to make space for the new item...

\begin{center}
    Before: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \cellcolor{colorblind_light_orange}\texttt{"Cats"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Dogs"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Mice"} &
        \cellcolor{colorblind_light_orange}\texttt{"Birds"} & 
        \texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 4
    ~\\~\\
    After: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \cellcolor{colorblind_light_blue}\texttt{"Ferrets"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Cats"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Dogs"} &
        \cellcolor{colorblind_light_orange}\texttt{"Mice"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Birds"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 5
\end{center}

\vspace{0.5cm}
\paragraph{PushAt:} Add a new item to the index given.
This requires shifting everything forward to make space for the new item...

\begin{center}
    Before: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & 
        \texttt{"Dogs"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Mice"} &
        \texttt{"Birds"} & 
        \texttt{""} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 4
    ~\\~\\
    After: ~\\~\\
    \begin{tabular}{| c | c | c | c | c | c |} \hline
        0 & 1 & 2 & 3 & 4 & 5 \\ \hline
        \texttt{"Cats"} & 
        \texttt{"Dogs"} & 
        \cellcolor{colorblind_light_blue}\texttt{"Ferrets"} &
        \cellcolor{colorblind_light_orange}\texttt{"Mice"} & 
        \cellcolor{colorblind_light_orange}\texttt{"Birds"} & \texttt{""} \\ \hline
    \end{tabular}
    ~\\~\\
    \texttt{totalItems} = 5
\end{center}

\paragraph{Additional design considerations:}

The \texttt{PushFront} and \texttt{PushAt} functions require shifting other elements
over, so I've added a helper function called \texttt{ShiftRight} so that we
don't write the same functionality in both of these functions.
~\\

Additionally, for each of these functions we will need to check to see 
if the array is full prior to adding any new items or shifting things.
Implementing a \texttt{IsFull} function can help with this as well.



\subsubsection{Implementing \texttt{void PushBack( T newItem )}}

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the array is full, then throw an exception.
\end{itemize}
\begin{lstlisting}[style=code]template <typename T>
if ( IsFull() )
    throw length_error( "Array is full!" );
\end{lstlisting}

\subparagraph{Functionality:}
\begin{itemize}
    \item   Add the new item to the first empty space. 
    \item   Increment the item count.
\end{itemize}
\begin{lstlisting}[style=code]template <typename T>
m_array[ m_itemCount ] = newItem;
m_itemCount++;
\end{lstlisting}

\hrulefill

\vspace{1cm}
\subsubsection{Implementing \texttt{void PushFront( T newItem )}}

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the array is full, then throw an exception.
\end{itemize}

\subparagraph{Preparation:}
\begin{itemize}
    \item   Shift everything to the right, starting at index 0.
\end{itemize}
\begin{lstlisting}[style=code]template <typename T>
ShiftRight( 0 );
\end{lstlisting}

\subparagraph{Functionality:}
\begin{itemize}
    \item   Add the new item at position 0.
    \item   Increment the item count.
\end{itemize}



\newpage
\subsubsection{Implementing \texttt{void PushAt( T newItem )}}

\subparagraph{DRY (Don't Repeat Yourself) checks:}
\begin{itemize}
    \item   If the index is 0, call PushFront instead.
    \item   If the index is m\_itemCount, call PushBack instead.
\end{itemize}

\subparagraph{Error checks:}
\begin{itemize}
    \item   If the array is full, then throw an exception.
    \item   If the index is invalid, throw an exception.
\end{itemize}

\subparagraph{Preparation:}
\begin{itemize}
    \item   Shift everything to the right, starting at the index.
\end{itemize}
\begin{lstlisting}[style=code]template <typename T>
ShiftRight( index );
\end{lstlisting}

\subparagraph{Functionality:}
\begin{itemize}
    \item   Add the new item at position index.
    \item   Increment the item count.
\end{itemize}

\newpage
\subsection{Pop - removing items from the array}

\subsubsection{Implementing \texttt{void PopBack()}}
\subparagraph{Error checks:} If the array is empty, throw exception.
\subparagraph{Functionality:} Decrement m\_itemCount.{}

\hrulefill
\subsubsection{Implementing \texttt{void PopFront()}}
\subparagraph{Error checks:} If the array is empty, throw exception.
\subparagraph{Functionality:} Call ShiftLeft at 0. Decrement m\_itemCount.

\hrulefill
\subsubsection{Implementing \texttt{void PopAt( int index )}}
\subparagraph{Error checks:} If the array is empty, throw exception. If the index is invalid, throw an exception.
\subparagraph{Functionality:} Call ShiftLeft at index. Decrement m\_itemCount.



\newpage

\subsection{Get - accessing items in the array}

\subsubsection{Implementing \texttt{T GetBack()}}
\subparagraph{Error checks:} If the array is empty, throw exception.
\subparagraph{Functionality:} Return the item in the array at position m\_itemCount-1.

\hrulefill
\subsubsection{Implementing \texttt{T GetFront()}}
\subparagraph{Error checks:} If the array is empty, throw exception.
\subparagraph{Functionality:} Return the item in the array at position 0.

\hrulefill
\subsubsection{Implementing \texttt{T GetAt( int index )}}
\subparagraph{Error checks:} If the array is empty, throw exception. If the index is invalid, throw an exception.
\subparagraph{Functionality:} Return the item in the array at position index.

\hrulefill
\subsection{Search}
Use a for loop to iterate over all elements of the array (0 to m\_itemCount). If the
item is found, return its index. Otherwise, after the loop has concluded, throw an exception
if nothing was found.

\begin{lstlisting}[style=code]
int SmartFixedArray<T>::Search( T item ) const
{
    for ( int i = 0; i < m_itemCount; i++ )
    {
        if ( m_array[i] == item )
        {
            return i;
        }
    }
    throw ItemNotFoundException( "SmartFixedArray<T>::Search", "Could not find item!" );
}
\end{lstlisting}

\newpage
\subsection{SmartFixedArray code}


\begin{lstlisting}[style=code]
template <typename T>
class SmartFixedArray
{
public:
  SmartFixedArray();
  ~SmartFixedArray();

  void PushBack( T newItem );
  void PushFront( T newItem );
  void PushAt( T newItem, int index );

  void PopBack();
  void PopFront();
  void PopAt( int index );

  T GetBack() const;
  T GetFront() const;
  T GetAt( int index ) const;

  int Search( T item ) const;
  void Display() const;
  void Display( ostream& outstream ) const;
  int Size() const;
  bool IsEmpty() const;
  bool IsFull() const;
  void Clear();

private:
  T m_array[100];
  const int ARRAY_SIZE;
  int m_itemCount;

  void ShiftLeft( int index );
  void ShiftRight( int index );
};

template <typename T>
SmartFixedArray<T>::SmartFixedArray()
  : ARRAY_SIZE( 100 )
{
  Clear();
}

template <typename T>
SmartFixedArray<T>::~SmartFixedArray()
{
  Clear();
}

template <typename T>
void SmartFixedArray<T>::Clear()
{
  m_itemCount = 0;
}

template <typename T>
int SmartFixedArray<T>::Size() const
{
  return m_itemCount;
}

template <typename T>
bool SmartFixedArray<T>::IsFull() const
{
  return ( m_itemCount == ARRAY_SIZE );
}

template <typename T>
bool SmartFixedArray<T>::IsEmpty() const
{
  return ( m_itemCount == 0 );
}

template <typename T>
void SmartFixedArray<T>::Display() const
{
  Display( cout );
}

template <typename T>
void SmartFixedArray<T>::Display( ostream& outstream ) const
{
  for ( int i = 0; i < m_itemCount; i++ )
  {
    outstream << i << ". " << m_array[i] << endl;
  }
}

template <typename T>
void SmartFixedArray<T>::ShiftLeft( int index )
{
  if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }

  for ( int i = index; i < m_itemCount-1; i++ )
  {
    m_array[i] = m_array[i+1];
  }
}

template <typename T>
void SmartFixedArray<T>::ShiftRight( int index )
{
  if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }
  else if ( m_itemCount == ARRAY_SIZE )
  {
    // THROW EXCEPTION
  }

  for ( int i = m_itemCount; i > index; i-- )
  {
    m_array[i] = m_array[i-1];
  }
}

template <typename T>
void SmartFixedArray<T>::PushBack( T newItem )
{
  if ( IsFull() )
  {
    // THROW EXCEPTION
  }

  m_array[ m_itemCount ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PushFront( T newItem )
{
  if ( IsFull() )
  {
    // THROW EXCEPTION
  }
  if ( !IsEmpty() )
  {
    ShiftRight( 0 );
  }

  m_array[ 0 ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PushAt( T newItem, int index )
{
  if ( index == 0 )
  {
    PushFront( newItem );
    return;
  }
  else if ( index == m_itemCount )
  {
    PushBack( newItem );
    return;
  }

  if ( index < 0 || index > m_itemCount )
  {
    // THROW EXCEPTION
  }

  if ( IsFull() )
  {
    // THROW EXCEPTION
  }

  ShiftRight( index );
  m_array[ index ] = newItem;
  m_itemCount++;
}

template <typename T>
void SmartFixedArray<T>::PopBack()
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  m_itemCount--;
}

template <typename T>
void SmartFixedArray<T>::PopFront()
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  ShiftLeft( 0 );
  m_itemCount--;
}

template <typename T>
void SmartFixedArray<T>::PopAt( int index )
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  else if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }
  ShiftLeft( index );
  m_itemCount--;
}

template <typename T>
T SmartFixedArray<T>::GetBack() const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }

  return m_array[m_itemCount - 1];
}

template <typename T>
T SmartFixedArray<T>::GetFront() const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }

  return m_array[0];
}

template <typename T>
T SmartFixedArray<T>::GetAt( int index ) const
{
  if ( IsEmpty() )
  {
    // THROW EXCEPTION
  }
  else if ( index < 0 || index >= m_itemCount )
  {
    // THROW EXCEPTION
  }

  return m_array[index];
}

template <typename T>
int SmartFixedArray<T>::Search( T item ) const
{
  for ( int i = 0; i < m_itemCount; i++ )
  {
    if ( m_array[i] == item )
    {
      return i;
    }
  }
  // THROW EXCEPTION
}
\end{lstlisting}
