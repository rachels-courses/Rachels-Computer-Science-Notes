

    \begin{center}
        \includegraphics[width=12cm]{images/title-image.png}
    \end{center}

    \section{What are data structures?}
    
                A \textbf{data structure} is an object (a class, a struct - some
                type of \textbf{structured} thing) that holds \textbf{data}.
                In particular, the entire job of a data structure object is
                to store data and provide an interface for \textbf{adding, removing,}
                and \textbf{accessing} that data.
                
                \paragraph{``Don't all the classes we write hold data? How is this different
                from other objects I've defined?''} ~\\
                
        
                
\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
    
                In the past, you may have written classes to represent objects,
                like perhaps a book. A book could have member variables like
                its title, isbn, and year published, and some methods that
                help us interface with a book.
                
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
    
        \centering
                        \begin{tabular}{ | c | } \hline
                                \textbf{Book} \\ \hline
                                -title : string ~\\
                                -isbn : string ~\\
                                -year : int ~\\ \hline
                                +Setup() : void ~\\
                                +Display() : void ~\\ \hline
                        \end{tabular}
                        
    \end{subfigure}
\end{figure}
                
                \begin{center}
                        However - this is not a data structure. ~\\
                        We \textit{could}, however, use a data structure to
                        \textit{store} a list of books.
                \end{center}
                
                \newpage
                
\begin{figure}[h]
    \centering
    \begin{subfigure}{.4\textwidth}
    
                A data structure will store a series of some sort of data.
                Often, it will use an \textbf{array} or a \textbf{linked list}
                as a base structure and functionality will be built on top.
                ~\\
                
                Ideally, the user doesn't care about \textit{how} the data
                structure works, they just care that they can \textbf{add, remove,}
                and \textbf{access} data they want to store.
                
    \end{subfigure}%
    \begin{subfigure}{.6\textwidth}
    
        \centering
                        \begin{tabular}{ | c | } \hline
                                \textbf{SmartBookArray} \\ \hline
                                -bookArray : Book[ ] ~\\
                                -arraySize : int ~\\
                                -bookCount : int ~\\ \hline
                                +AddBook( Book newBook ) : void ~\\
                                +RemoveBook( int index ) : void ~\\
                                +RemoveBook( string title ) : void ~\\
                                +GetBook( int index ) : Book ~\\
                                +FindBookIndex( string title ) : int ~\\
                                +DisplayAll() : void ~\\
                                +Size() : int ~\\
                                +IsEmpty() : bool ~\\ \hline
                        \end{tabular}
                        
    \end{subfigure}
\end{figure}

                Ideally, when we are writing a data structure, it should be:
                
                \begin{itemize}
                        \item   Generic, so you could store \textit{any data type} in it.
                        \item   Reusable, so that the data structure can be used in many different programs.
                        \item   Robust, offering exception handling to prevent the program from
                                        crashing when something goes wrong with it.
                        \item   Encapsulated, handling the inner-workings of dealing with the data,
                                        without the user (or other programmers working outside the data structure)
                                        having to write special code to perform certain operations.
                \end{itemize}
                
                The way I try to conceptualize the work I'm doing on a data structure
                is to pretend that I'm a developer that is going to create and sell a
                C++ library of data structures that \textit{other developers at other companies}
                can use in their own, completely separate, software projects. If I'm
                selling my data structures package to other businesses, my code should
                be dependable, stable, efficient, and relatively easy to use.

                \begin{center}
                        \includegraphics[width=10cm]{Introduction/images/data-structures-ad.png}
                \end{center}

        \section{What is algorithm analysis?}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
    
                Algorithm Analysis is the process of figuring out how
                \textbf{efficient} a function is and how it scales over time,
                given more and more data to operate on.
                
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
    
        \centering
                \includegraphics[width=6cm]{Introduction/images/complexity.png}
                        
    \end{subfigure}
\end{figure}
                
                This is another important part of dealing with data structures,
                as different structures will offer different trade offs when
                it comes to the efficiency of \textbf{data access} functions,
                \textbf{data searching} functions, \textbf{data adding} functions,
                and \textbf{data removal} functions. 
                Every operation takes
                a little bit of processing time, and as you iterate over data, that
                processing time is multiplied by the amount of times you go through a loop.
                                
                \paragraph{Example: Scalability} ~\\
                Let's say we have a sorting algorithm that, for every $n$ records,
                it takes $n^2$ program units to find an object.
                If we had 100 records, then it would take $100^2 = 10,000$ time-units to
                sort the set of data. ~\\
                
                What the time-units are could vary - an older
                machine might take longer to execute one instruction, and a newer
                computer might process an instruction much more quickly, but we think of
                algorithm complexity in this sort of generic form.
                
                \begin{center}
                        \includegraphics[width=12cm]{Introduction/images/search-sort-ad.png}
                \end{center}
                
                \newpage
                \paragraph{Example: Efficiency of an Array-based structure} ~\\
                
                \begin{center}
                        \begin{tikzpicture}
                                \draw (0,0) -- (10,0) -- (10,1) -- (0,1) -- (0,0);
                                \draw (2,0) -- (2,1); \draw (4,0) -- (4,1); \draw (6,0) -- (6,1); \draw (8,0) -- (8,1); 
                                \node at (1,0.5) {kansas}; 
                                \node at (3,0.5) {missouri}; 
                                \node at (5,0.5) {arkansas}; 
                                \node at (7,0.5) {ohio}; 
                                \node at (9,0.5) {oklahoma}; 
                                \node at (1,-0.5) {0}; 
                                \node at (3,-0.5) {1}; 
                                \node at (5,-0.5) {2}; 
                                \node at (7,-0.5) {3}; 
                                \node at (9,-0.5) {4}; 
                        \end{tikzpicture}
                \end{center}
                
                In an array-based structure, we have a series of \textbf{elements} in a row,
                and each element is accessible via its \textbf{index} (its position in the array).
                Arrays allow for random-access, so \textbf{accessing} element \#2 is instant:
                
\begin{lstlisting}[style=code]
cout << arr[2];
\end{lstlisting}

                No matter how many elements there are ($n$), we can access the element at
                index 2 without doing any looping. We state that this is $O(1)$ (``Big-O of 1'')
                time complexity for an \textbf{access} operation on an \textbf{array}.
                ~\\
                
                However, if we were \textbf{searching} through the unsorted array for an item, we would
                have to start at the beginning and look at each item, one at a time, until
                we either found what we're looking for, or hit the end of the array:
                
\begin{lstlisting}[style=code]
for ( int i = 0; i < ARR_SIZE; i++ )
{
    if ( arr[i] == searchTerm )
    {
            return i; // found at this position
    }
}
return -1;      // not found
\end{lstlisting}

                For \textbf{search}, the \textit{worst-case scenario} is having to
                look at \textit{all elements of the array} to ensure what we're
                looking for isn't there. Given $n$ items in the array, we have to
                iterate through the loop $n$ times. This would end up being
                $O(n)$ (``Big-O of $n$'') time complexity for a \textbf{search}
                operation on an \textbf{array}.
                ~\\~\\
                
                We can build our data structures on top of an array, but there
                is also a type of structure called a \textbf{linked} structure,
                which offers its own pros and cons to go with it. We will learn
                more about algorithm analysis and types of structures later on.


        \newpage
        \section{The point of a Data Structures class}
        
                Of course, plenty of data structures have already been written
                and are available out there for you to use. C++ even has the
                \textbf{Standard Template Library} full of structures
                already built and optimized!
                
                \begin{center}
                        \begin{tabular}{l l}
                                \texttt{vector} & http://www.cplusplus.com/reference/vector/vector/ \\
                                \texttt{list} & http://www.cplusplus.com/reference/list/list/ \\
                                \texttt{map} & http://www.cplusplus.com/reference/map/map/
                        \end{tabular}
                \end{center}

                \paragraph{``So why are we learning to write data structures if
                they've already been written for us?''} ~\\
                
                While you generally \textit{won't} be rolling your own linked list
                for projects or on the job, it \textit{is} important to know how
                the inner-workings of these structures operate. Knowing how each structure
                works, its tradeoffs in efficiency, and how it structures its data
                will help you \textbf{choose} what structures to use when faced with
                a \textbf{design decision} for your software.
        
                \begin{center}
                        \includegraphics[width=14cm]{Introduction/images/which-structure.png}
                \end{center}
        
        %\newpage
        %\section{Object Oriented Design concepts}
        %Object Oriented Design concepts really come into play when we are
        %designing and implementing code that is intended to be reused in
        %multiple other locations. 
        
        %If you're writing something once to do
        %something and that code won't be reused elseware - just write it
        %and get it over with; don't over-engineer it. If, however, you're
        %writing something that you'll reuse across multiple projects, or
        %that you will have to \textbf{maintain} for an extended period of time,
        %then keeping these design principles in mind is important.
        
        %\paragraph{Abstraction} 
        
        %\paragraph{Encapsulation}
        
        %\paragraph{Information Hiding}
        
        %\paragraph{Interfaces}
        
        %\paragraph{High Cohesion}
        
        %\paragraph{Loose Coupling}

