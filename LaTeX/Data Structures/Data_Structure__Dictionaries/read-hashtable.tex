
    \begin{center}
        \includegraphics[width=13cm]{images/hash-table.png}
    \end{center}
    
    \newpage
        \section{Back to arrays...}
            So far we've looked at array-based structures and link-based structures
            and their tradeoffs in efficiency. Recall that:
            
            \begin{center}
                \begin{tabular}{| p{6cm} | p{6cm} |}  \hline
                    \textbf{Arrays} & \textbf{Linked lists} \\ \hline
                    \textbf{Access} to an arbitrary index is instant - $O(1)$ - it's just the address of the first element, plus the index times the size of the data type. 
                    & \textbf{Access} to an arbitrary index requires traversing the list - $O(n)$. \\ \hline
                    \textbf{Inserting} is instant if the array isn't full,
                    if the array is full, we need to resize and copy all of the data, which is $O(n)$.
                    &
                    \textbf{Inserting} is instant - $O(1)$ - just creating a node and updating pointers. \\ \hline
                    \textbf{Searching} is $O(n)$ in an unsorted array - start at the beginning and search until the end.
                    &
                    \textbf{Searching} is $O(n)$ in an unsorted linked list - start at the beginning and search until the end.
                    \\ \hline
                \end{tabular}
            \end{center}
            
            What makes an array so fast to access is that we do a simple math operation
            to find the address of a given index in the array:
            
            \begin{center}
                $address_{i} = address_{0} + i \cdot sizeof(data)$
            \end{center}
            
            With this next structure, the \textbf{Hash Table} (aka associative array, map, or dictionary),
            we're going to leverage the speed of an array, and also make searching essentially $O(1)$
            as well, with a little extra math.
        
        \newpage
        
                
        \section{Concepts}
        
            \subsection{Key-value pairs}
            
                Dictionaries are often thought of as “key-value pairs”, where we have some data to store (this is the value), and we can look it up by its key (some searchable identifier).
            
                When designing a class around this, we can reuse some piece of
                information as the \textbf{key}, as long as it's going to be
                unique in the table.
                
                \begin{center}
                    \begin{tabular}{| c | p{6cm} |} \hline
                        \textbf{Employee ID (key)} & \textbf{Employee object (value)} \\ \hline
                        1024 & 
                        Name: Amina
                        
                        Employee ID: 1024
                        
                        Department: Dev
                        \\ \hline
                        2048 &
                        Name: Benita
                        
                        Employee ID: 2048
                        
                        Department: Sales
                        \\ \hline
                        1280 &
                        Name: Caiyun
                        
                        Employee ID: 1280
                        
                        Department: QA
                        \\ \hline
                    \end{tabular}
                \end{center}
                
                The \textbf{key} should be a datatype that can be easily
                put into a mathematical function - such as an integer.
                Strings can also be used, but a complex data type
                like a class wouldn't work very well as a key.
                ~\\
                
                The \textbf{value} can be any data type, usually a specific
                class that stores more data.
                ~\\
                
                Some examples of key-value uses are:
                
                \begin{itemize}
                    \item   People lookup - Associating an employee ID, student ID, etc. (key) to an Employee or Student object (value).
                    \item   Address book - Assocating a phone number (key) with a contact entry (value).
                    \item   Dictionaries - searching for a word (key), finding the definition (value).
                    \item   Book lookup - Associating an ISBN (key) to a Book object (value).
                    \item   Word counter - Associating each word (key) with the amount of times it has shown up in a document (value).
                \end{itemize}
            
            \newpage
            \subsection{Hashing the key to get the index}
            
                With our Hash Tables, each \textbf{element} in the array will
                be associated with a \textbf{key} - similar to our Binary Search Trees.
                The \textbf{key} is a unique identifier, allowing us to access a set
                of data mapped to the key.
                
                \begin{center}
                    \includegraphics[width=0.8\textwidth]{Data_Structure__Dictionaries/images/hash-employees.png}
                \end{center}
            
                The way we do this is we use this \textbf{key} as the input of
                a function that converts the key into an \textbf{index} - 
                somewhere from \texttt{0} (the beginning of the array). to \texttt{n-1} (the end of the array).
                This is known as the \textbf{hash function}.
                A simple hash function could be taking the $key$ and using the \textbf{modulus operator}
                with the size of the array to get an index.
                
                \begin{center}
                    \includegraphics[width=0.9\textwidth]{Data_Structure__Dictionaries/images/hash-function.png}
                \end{center}
                
                Because of this hash function, the efficiency of both \textbf{inserting}
                and \textbf{searching} is $O(1)$ - unless there are \textbf{collisions},
                where multiple keys map to the same index. This also means that,
                with minimal collisions, the speed to insert/search is going to be 
                near instantaneous whether we have 10 items or 10,000 items. 
                
            
        \newpage
        \subsection{Hashing functions}
            
                The simplest hashing function will just take the \textbf{key}
                and the \textbf{size of the array} and use 
                \textbf{modulus} 
                \footnote{ Modulus gives us the \textbf{remainder} that is the result of a 
                division between the two numbers. For example, 5 divided by 2 gives us 2 with the remainder of 1. ~\\ Therefore, 5 mod 2 = 1. } 
                to find a resulting index...
                
                $$ Hash(key) = key\ \% \ ARRAY\_SIZE $$
            
                Using the modulus operator
                restricts the resulting indices so
                that they will be between \texttt{0} and \texttt{ARRAY\_SIZE-1}.
                For example, if we had an array of size 5 and were generating
                indices, everything would be between 0 and 4:
                
                \begin{center}
                    \begin{tabular}{ | l | c | c | c | c | c | } \hline
                        \textbf{index}  & \texttt{0} & \texttt{1} & \texttt{2} & \texttt{3} & \texttt{4}
                        \\ \hline
                        \textbf{key}    & 0 & 1 & 2 & 3 & 4
                        \\              & 5 & 6 & 7 & 8 & 9                                        
                        \\              & 10 & 11 & 12 & 13 & 14
                        \\              & 15 & 16 & 17 & 18 & 19
                        \\ \hline
                    \end{tabular}
                \end{center}
                
                Looking at the table above, if we had something with the key
                5 and the key 10, this would cause a \textbf{collision} - they would both map
                to the index position 0. If we do encounter a collision, we will need a strategy to find a new index
                -- more on that later.
                ~\\~\\
                
                As an example of a hash function, let’s say we’re going to store employees in a Hash Table by their unique employee IDs. Employee IDs begin at 1000 (not 0 like the array index) and can be any 4 digit number. They’re not sequential.
                We want to assign an array index to each employee ID so we can quickly search for an employee by their ID. For this, we need a hash function. 
                The simple hash function would look like this:

\begin{lstlisting}[style=code]
// Input: employee ID integer (key)
// Output: array index 
int Hash( int employeeId )
{
    return employeeId % ARRAY_SIZE;
}
\end{lstlisting}
                


        \newpage
        \subsection{Collisions}
            Sooner or later a collision will occur, and you will need a way to take care of it
            - basically, to generate a \textbf{different index} than the one that our hash function
            gives us. There are several ways we can design our hash tables and collision stratgies.
            
                \subsubsection{Linear probing (open addressing):}
                    With linear probing, the solution to a collision is simply to go forward by 
                    one index and see if that position is free. 
                    If not, continue stepping forward by 1 until an available space is found.
                    
                    \begin{center}
                        \begin{tabular}{ p{6cm} p{6cm} }
                            1. Initial hash table &
                            \begin{tabular}{ | l | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}    & \texttt{2}    & \texttt{3}    & \texttt{4}    \\ \hline
                                value       &             & 6           & 12            &               &               \\ \hline
                            \end{tabular}
                            \\ \\ \\
                            2. Insert new employee ID 16...
                                H(16) = 16 \% 5 = 1
                                
                                \color{red}COLLISION\color{black}&
                            \begin{tabular}{ | l | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \cellcolor{red!50}{\texttt{1}}    & \texttt{2}    & \texttt{3}    & \texttt{4}    \\ \hline
                                value       &             & \cellcolor{red!50}{6}           & 12            &               &               \\ \hline
                            \end{tabular}
                            \\ \\
                            3. Move forward by 1...
                            
                            Check index 2...
                            
                            \color{red}COLLISION\color{black}&
                            \begin{tabular}{ | l | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}    & \cellcolor{red!50}{\texttt{2}}    & \texttt{3}    & \texttt{4}    \\ \hline
                                value       &             & 6           & \cellcolor{red!50}{12}            &               &               \\ \hline
                            \end{tabular}
                            \\ \\
                            4. Move forward by 1...
                            
                            Check index 3...
                            
                            \color{blue}OK\color{black}&
                            \begin{tabular}{ | l | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}    & \texttt{2}    & \cellcolor{green!50}{\texttt{3}}    & \texttt{4}    \\ \hline
                                value       &             & 6           & 12            & \cellcolor{green!50}{16}              &               \\ \hline
                            \end{tabular}
                        \end{tabular}   
                    \end{center}
                    
                    An example of the implementation of this function would just
                    take into account what the \textbf{original index} was, and
                    \textbf{how many collisions occurred}, allowing us to step
                    forward 1 space for each collision that occurs.
                    
\begin{lstlisting}[style=code]
int LinearProbe( int originalIndex, int collisionCount )
{
    return originalIndex + collisionCount;
}
\end{lstlisting}
        
                \newpage
                \subsubsection{Quadratic probing (open addressing):}
                    With quadratic probing, we will keep stepping forward until we find an available spot, just like with linear probing. However, the amount we step forward by changes each time we hit a collision on an insert.
                    
                    \begin{enumerate}
                        \item   Initial hash table: ~\\~\\
                            \begin{tabular}{ | l | c | c | c | c | c | c | c | c | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}    & \texttt{2}    & \texttt{3}    & \texttt{4} 
                                            & \texttt{5}  & \texttt{6}    & \texttt{7}    & \texttt{8}    & \texttt{9} 
                                            & \texttt{10} & \texttt{11} & \texttt{12}
                                \\ \hline
                                value       &               % 0
                                            & 14            % 1
                                            & 41            % 2
                                            &               % 3
                                            &               % 4
                                            & 18            % 5
                                            &               % 6
                                            &               % 7
                                            &               % 8
                                            &               % 9
                                            &               % 10
                                            &               % 11
                                            &               % 12
                                \\ \hline
                            \end{tabular}   ~\\~\\
                            
                        \item   Insert new employee ID 27... ~\\
                                H(27) = 27 \% 13 = 1 ~\\
                                \color{red} COLLISION \color{black} ~\\~\\
                            \begin{tabular}{ | l | c | c | c | c | c | c | c | c | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \cellcolor{red!50}{\texttt{1}}   
                                            & \texttt{2}    
                                            & \texttt{3}    & \texttt{4} 
                                            & \texttt{5}  & \texttt{6}    & \texttt{7}    & \texttt{8}    & \texttt{9} 
                                            & \texttt{10} & \texttt{11} & \texttt{12}
                                \\ \hline
                                value       &               % 0
                                            & \cellcolor{red!50}{14}            % 1
                                            & 41            % 2
                                            &               % 3
                                            &               % 4
                                            & 18            % 5
                                            &               % 6
                                            &               % 7
                                            &               % 8
                                            &               % 9
                                            &               % 10
                                            &               % 11
                                            &               % 12
                                \\ \hline
                            \end{tabular}    ~\\~\\
                            
                        \item   Collisions = 1, Shift over by collisions$^{2}$...~\\
                                Try 1 (original index) + $1^{2}$ = 2... ~\\
                                \color{red} COLLISION \color{black} ~\\~\\
                            \begin{tabular}{ | l | c | c | c | c | c | c | c | c | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}
                                            & \cellcolor{red!50}{\texttt{2}   } 
                                            & \texttt{3}    & \texttt{4} 
                                            & \texttt{5}  & \texttt{6}    & \texttt{7}    & \texttt{8}    & \texttt{9} 
                                            & \texttt{10} & \texttt{11} & \texttt{12}
                                \\ \hline
                                value       &               % 0
                                            & 14            % 1
                                            & \cellcolor{red!50}{41}            % 2
                                            &               % 3
                                            &               % 4
                                            & 18            % 5
                                            &               % 6
                                            &               % 7
                                            &               % 8
                                            &               % 9
                                            &               % 10
                                            &               % 11
                                            &               % 12
                                \\ \hline
                            \end{tabular}    ~\\~\\
                            
                        \item   Collisions = 2, Shift over by $2^{2}$ ~\\
                                Try 1 + $2^{2}$ = 5... ~\\
                                \color{red} COLLISION \color{black} ~\\~\\
                            \begin{tabular}{ | l | c | c | c | c | c | c | c | c | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}
                                            & \texttt{2} 
                                            & \texttt{3}    & \texttt{4} 
                                            & \cellcolor{red!50}{\texttt{5}}  & \texttt{6}    & \texttt{7}    & \texttt{8}    & \texttt{9} 
                                            & \texttt{10} & \texttt{11} & \texttt{12}
                                \\ \hline
                                value       &               % 0
                                            & 14            % 1
                                            & 41            % 2
                                            &               % 3
                                            &               % 4
                                            & \cellcolor{red!50}{18}            % 5
                                            &               % 6
                                            &               % 7
                                            &               % 8
                                            &               % 9
                                            &               % 10
                                            &               % 11
                                            &               % 12
                                \\ \hline
                            \end{tabular}    ~\\~\\
                            
                        \item   Collisions = 3, Shift over by $3^{2}$ ~\\
                                Try 1 + $3^{2}$ = 10... ~\\
                                \color{blue} OK \color{black} ~\\~\\
                            \begin{tabular}{ | l | c | c | c | c | c | c | c | c | c | c | c | c | c | } \hline
                                index       & \texttt{0}  & \texttt{1}
                                            & \texttt{2} 
                                            & \texttt{3}    & \texttt{4} 
                                            & \texttt{5}  & \texttt{6}    & \texttt{7}    & \texttt{8}    & \texttt{9} 
                                            & \cellcolor{green!50}{\texttt{10}} & \texttt{11} & \texttt{12}
                                \\ \hline
                                value       &               % 0
                                            & 14            % 1
                                            & 41            % 2
                                            &               % 3
                                            &               % 4
                                            & 18            % 5
                                            &               % 6
                                            &               % 7
                                            &               % 8
                                            &               % 9
                                            & \cellcolor{green!50}{27}              % 10
                                            &               % 11
                                            &               % 12
                                \\ \hline
                            \end{tabular} 
                    \end{enumerate}
                    
                    Each time there's a collision, we search further from
                    the original hashed index: +1 space, then +4 spaces, then +9 spaces.

                    \begin{center}
                        \includegraphics[width=\textwidth]{Data_Structure__Dictionaries/images/collisions.png}
                    \end{center}
                    
                    The probing function would look something like this:
                      
\begin{lstlisting}[style=code]
int QuadraticProbe( int originalIndex, 
                    int collisionCount )
{
    return originalIndex + pow( collisionCount, 2 );
}
\end{lstlisting}
                    
                    
                    \hrulefill
                    \subsubsection{Double hashing (open addressing):}
                    
                        With double hashing, we have two hash functions. 
                        If the first Hash function 
                        returns an index that is already taken, then we 
                        use the \textbf{second Hash function} 
                        on the key to generate an \textbf{offset},
                        instead of just using a linear or quadratic formula.
                        
                        With this method, our new index after a collision will be:
                        
                        \begin{center}
                            \texttt{newIndex = Hash(key) + collisionCount * Hash2(key)}
                        \end{center}
                        
                        It is up to our design to figure out a second hash function,
                        but generally it should also work with prime numbers. For example:
                        
                             
\begin{lstlisting}[style=code]
int Hash( int key )
{
    return key % ARRAY_SIZE;
}

int Hash2( int key )
{
    return 7 - ( key % 7 );
}
\end{lstlisting}
                    
                    
                  
                    \subsubsection{Note: Index wrap-around}
                        Note that with all of these operations, we will also
                        do an additional operation of \texttt{ index \% ARRAY\_SIZE }
                        to make sure the index stays within the bounds of the array.
        
                
                    \subsubsection{The problem of clustering}
                        
                        Clustering is a problem that arises, particularly when using linear probing. If we’re only stepping forward by 1 index every time there’s a collision, then we tend to get a Dictionary table where all the elements are clustered together in groups...
                        
                        \begin{center}
                            \begin{tabular}{| c | c | c | c | c | c | c | c | c | c |} \hline
                                \textbf{0} & \textbf{1} & \textbf{2} & \textbf{3} & \textbf{4} & 
                                \textbf{5} & \textbf{6} & \textbf{7} & \textbf{8} & \textbf{9} \\ \hline
                                \cellcolor{black!25} & A & B & C & \cellcolor{black!25} & \cellcolor{black!25} & \cellcolor{black!25} & X & Y & Z \\ \hline
                            \end{tabular}
                        \end{center}
                        
                        Because of this, the more full the Hash Table gets, the less 
                        efficient it becomes when using linear probing.


                \subsection{Sizing a Hash Table array}

                    When we’re writing a hash function, we will want to take into account how 
                    likely a collision will be. A general design rule is that 
                    making your array size a \textbf{prime number} helps reduce collisions.
        
    \newpage
    \section{Hash Table efficiency}
    
        In an ideal world with no collisions, a Hash Table would have an $O(1)$
        growth rate for its Search, Insert, and Deletion no matter how many items
        it stores. However, since collisions may occur, the efficiency of the
        Hash Table can vary.
        
        \paragraph{Pros:}
        \begin{itemize}
            \item   \textbf{Searching and Inserting is relatively quick:}
                    Since we use hash functions and collision functions to find
                    the index to insert at \textit{and} to access, it is
                    just one math operation (at best) to find an index,
                    and with collisions, hopefully just a few more calculations.
                    
            \item   \textbf{Mapping a key to a value is useful:}
                    With a normal Array vs. Linked List, there's not really
                    additional logic that goes into the storage - we just need
                    to store data and it is thrown into a structure.
                    With a Hash Table, we can have a more intentional design
                    for our data.
                    
        \end{itemize}
        
        \paragraph{Cons:}
        \begin{itemize}
            \item   \textbf{Many things affect Hash Table efficiency:}
                    The efficiency of finding an index
                    can vary based on the size of the array, how full it is,
                    the quality of the hashing function and collision strategy,
                    and even the data being stored in it.
                    
            \item   \textbf{A bad hash function can cause more collisions:}
                    Different hash functions can affect how many collisions occur,
                    so different functions can cause different speeds.
                    
            \item   \textbf{They are difficult to resize:}
                    If we were to resize a hash table, not only would we have
                    to copy all the data over (which would be $O(n)$),
                    we would also need to \textit{rehash} all of the keys
                    to get new indices based on the new table size.
        \end{itemize}
        
        Because resizing a hash table is expensive, if we can reasonably estimate
        the needed size of the hash table ahead of time, that only helps us out.
        If we under-estimate the size, that can be costly in having to
        resize things.

    \newpage
    \section{See also...}
            
            You can find out about more hashing techniques by looking on Wikipedia.
            
            \begin{itemize}
                \item   Separate chaining
                \item   Coalesced hashing
                \item   Robin-hood hashing
            \end{itemize}
