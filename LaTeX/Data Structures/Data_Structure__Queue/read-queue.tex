

    \begin{center}
        \includegraphics[width=14cm]{images/queue.png}
    \end{center}
    
    \newpage
    %--------------------------------------------------------------------%
    \section{What are Queues?} %-----------------------------------------%

        Queues are a type of structure that only allows new items to be
        added to the \textit{end} of the queue, and only allows items to
        be removed at the \textit{beginning} of the queue. It is commonly
        called \textbf{FIFO: First In First Out}. A queue is basically
        any line you have to stand in at a store.

        \begin{center}
            \includegraphics[width=8cm]{images/groceryline.png}
        \end{center}

        Remember that a \textbf{data structure} will have functionality
        to \textbf{add}, \textbf{remove}, and \textbf{access} (and sometimes
        \textbf{search}) the structure, but some data structures are for special
        types of applications. A queue has add, remove, and access, but
        these are all restricted - you can't \textit{insert} in the middle or beginning of
        the queue, or \textit{remove} from the middle or end of the queue.

        \newpage
        \subsection{Building a Queue}
            A Queue, in and of itself, is just an idea. We can use our
            \textbf{dynamic array} structure or our \textbf{linked list}
            structure to build a Queue, and only allowing certain functions
            to be called and others to be unavailable. We could simply do
            this by writing a Queue class that inherits from or contains a \texttt{LinkedList}
            or \texttt{Vector}, and only providing access to certain methods.

            Since a Queue only has one place items can be added,
            we would only have a simple \texttt{Push} function instead of
            having a \texttt{PushFront} and \texttt{PushBack}, and same
            for the rest of the functions.

            \begin{center}
                \begin{tabular}{| c | c | c | c |} \hline
                    \textbf{Vector}     & \textbf{List}   & \textbf{Queue} & \textbf{Stack}    \\ \hline
                        \texttt{PushFront}  &
                        \texttt{PushFront}  &
                        -    &
                        -
                        \\ \hline
                        \cellcolor{purple!25} \texttt{PushBack}   &
                        \cellcolor{purple!25} \texttt{PushBack}   &
                        \cellcolor{colorblind_light_blue} \texttt{Push}       &
                        \cellcolor{colorblind_light_orange}  \texttt{Push}
                        \\ \hline
                        \texttt{InsertAt}   &
                        -*                  &
                        -                   &
                        -
                        \\ \hline
                    \multicolumn{4}{| c |}{ \cellcolor{gray} \tab } \\ \hline
                        \cellcolor{colorblind_light_blue} \texttt{PopFront}   &
                        \cellcolor{colorblind_light_blue} \texttt{PopFront}     &
                        \cellcolor{colorblind_light_blue} \texttt{Pop} &
                        -
                        \\ \hline
                        \cellcolor{colorblind_light_orange} \texttt{PopBack}    &
                        \cellcolor{colorblind_light_orange} \texttt{PopBack}      &
                        - &
                        \cellcolor{colorblind_light_orange} \texttt{Pop}
                        \\ \hline
                        \texttt{RemoveAt}   &
                        -*     &
                        - &
                        -
                        \\ \hline
                    \multicolumn{4}{| c |}{ \cellcolor{gray} \tab } \\ \hline
                        \cellcolor{colorblind_light_blue} \texttt{GetFront}   &
                        \cellcolor{colorblind_light_blue} \texttt{GetFront}     &
                        \cellcolor{colorblind_light_blue} \texttt{Peek} & -

                        \\ \hline
                        \cellcolor{colorblind_light_orange} \texttt{GetBack}    &
                        \cellcolor{colorblind_light_orange} \texttt{GetBack}    &
                        - &
                        \cellcolor{colorblind_light_orange} \texttt{Top}
                        \\ \hline
                        \texttt{GetAt}      &
                        -*     &
                        - &
                        -
                        \\ \hline
                \end{tabular}
            \end{center}

            ~\\ \footnotesize {* You could implement an insert/remove/get to a list, but in the STL List object it is not available.}
            \normalsize

            We will cover Stacks in another chapter, but it is a cousin of Queues -
            they also have restricted access, but they're \textbf{LIFO} (Last In First Out).
            
            \begin{center}
                \includegraphics[width=10cm]{Data_Structure__Queue/images/stackqueue.png}
            \end{center}

        \newpage
        \subsection{Adding to a Queue (Push/Enqueue)}
            The Queue will add items
            using \texttt{PushBack} functionality, adding each item to the
            \textit{end} of the list. Let's say we're running the following commands
            in order:

            \begin{center}
                \texttt{Push( "A" );} \tab[0.5cm]
                \texttt{Push( "B" );} \tab[0.5cm]
                \texttt{Push( "C" );} \tab[0.5cm]
                \texttt{Push( "D" );}
            \end{center}

            \begin{center}
                1. \texttt{Push("A"):} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    %\node at (Element2value) {\texttt{"A"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    %\node at (Element3value) {\texttt{"A"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"A"}};
                \end{tikzpicture} ~\\~\\

                2. \texttt{Push("B"):} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"B"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    %\node at (Element3value) {\texttt{"A"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"A"}};
                \end{tikzpicture}~\\~\\

                3. \texttt{Push("C"):} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"B"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    \node at (Element3value) {\texttt{"C"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"A"}};
                \end{tikzpicture}~\\~\\

                4. \texttt{Push("D"):} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"B"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    \node at (Element3value) {\texttt{"C"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    \node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture}
            \end{center}


        \newpage
        \subsection{Removing from a Queue (Pop/Dequeue)}

            When we remove items from the Queue, the front item will be
            removed, and each item will be shifted one space toward the front.

            \begin{center}
                \texttt{Pop();} \tab[0.5cm]
                \texttt{Pop();} \tab[0.5cm]
                \texttt{Pop();}
            \end{center}

            \begin{center}
                1. \texttt{Initial state:} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"B"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    \node at (Element3value) {\texttt{"C"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    \node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture} ~\\~\\

                2. \texttt{Pop():} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"B"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"C"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    \node at (Element3value) {\texttt{"D"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture}~\\~\\

                3. \texttt{Pop():} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"C"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"D"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    %\node at (Element3value) {\texttt{"D"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture}~\\~\\

                4. \texttt{Pop:} ~\\~\\
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"D"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    %\node at (Element2value) {\texttt{"C"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    %\node at (Element3value) {\texttt{"D"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    %\node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture}
            \end{center}

        \newpage
        \subsection{Access with a Queue (Get/Peek)}

            When we call the function to access from a Queue, we can only
            access one item: the front-most item.

            \begin{center}
                \texttt{Get();}
            \end{center}

            \begin{center}
                \begin{tikzpicture}[framed]
                    \coordinate (Front) at (-2.5,0.5);
                    \coordinate (Back)  at (2.5,0.5);
                    \node[rotate=90] at (Front) {front};
                    \node[rotate=90] at (Back)  {back};

                    \coordinate (Element1a) at (-2,0);
                    \coordinate (Element1b) at (-1,0);
                    \coordinate (Element1c) at (-1,1);
                    \coordinate (Element1d) at (-2,1);
                    \coordinate (Element1index) at (-1.5,1.5);
                    \coordinate (Element1value) at (-1.5,0.5);
                    \draw[fill=colorblind_light_blue] (Element1a) -- (Element1b) -- (Element1c) -- (Element1d) -- (Element1a);
                    \node at (Element1index) {0};
                    \node at (Element1value) {\texttt{"A"}};

                    \coordinate (Element2a) at (-1,0);
                    \coordinate (Element2b) at (0 ,0);
                    \coordinate (Element2c) at (0 ,1);
                    \coordinate (Element2d) at (-1,1);
                    \coordinate (Element2index) at (-0.5,1.5);
                    \coordinate (Element2value) at (-0.5,0.5);
                    \draw (Element2a) -- (Element2b) -- (Element2c) -- (Element2d) -- (Element2a);
                    \node at (Element2index) {1};
                    \node at (Element2value) {\texttt{"B"}};

                    \coordinate (Element3a) at (0,0);
                    \coordinate (Element3b) at (1,0);
                    \coordinate (Element3c) at (1,1);
                    \coordinate (Element3d) at (0,1);
                    \coordinate (Element3index) at (0.5,1.5);
                    \coordinate (Element3value) at (0.5,0.5);
                    \draw (Element3a) -- (Element3b) -- (Element3c) -- (Element3d) -- (Element3a);
                    \node at (Element3index) {2};
                    \node at (Element3value) {\texttt{"C"}};

                    \coordinate (Element4a) at (1,0);
                    \coordinate (Element4b) at (2,0);
                    \coordinate (Element4c) at (2,1);
                    \coordinate (Element4d) at (1,1);
                    \coordinate (Element4index) at (1.5,1.5);
                    \coordinate (Element4value) at (1.5,0.5);
                    \draw (Element4a) -- (Element4b) -- (Element4c) -- (Element4d) -- (Element4a);
                    \node at (Element4index) {3};
                    \node at (Element4value) {\texttt{"D"}};
                \end{tikzpicture}
            \end{center}
            
        \hrulefill
        \subsection{Use of a Queue in software}
        
            With this single-minded, ``access the front item of the Queue,
            then discard that and access the next front item'', what is a Queue
            structure used for?
            
            Similarly to in real life, it's a way to make items wait their
            turn for some form of processing. At the grocery store, there are
            limited resources (cash registers). At each register, people
            queue up, waiting for their time until they can go through the
            process of having their items rung up and paying for them.
            And, we wait in a ``first come, first served'' order (people
            will generally get angry if you try to enter at the \textit{front}
            of the line). ~\\
            
            The same is true of a Queue. Perhaps we have many different things
            we want to process, but a limited amount of resources. So,
            as these items come in, if we're busy processing something,
            everything else \textit{queues up} and waits its turn.

            \begin{center}
                \includegraphics[width=14cm]{Data_Structure__Queue/images/boxqueue.png}
            \end{center}


    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    \section{Implementing a Queue} %-------------------------------------%
    
        A Queue will be simplest to implement if you build the class on top
        of an existing structure.

