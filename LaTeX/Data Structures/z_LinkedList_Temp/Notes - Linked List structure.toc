\contentsline {chapter}{\numberline {1}Coming from arrays...}{2}%
\contentsline {section}{\numberline {1.1}Investigating arrays and memory addresses:}{2}%
\contentsline {section}{\numberline {1.2}Pros and Cons of arrays for storing data:}{5}%
\contentsline {paragraph}{Pros:}{5}%
\contentsline {paragraph}{Cons:}{5}%
\contentsline {chapter}{\numberline {2}Anatomy of a Node}{6}%
\contentsline {paragraph}{Singly-linked List's Nodes}{7}%
\contentsline {paragraph}{Doubly-linked List's Nodes}{7}%
\contentsline {paragraph}{A doubly-linked Node:}{8}%
\contentsline {paragraph}{A singly-linked Node:}{8}%
\contentsline {paragraph}{Single vs. Double?}{8}%
\contentsline {chapter}{\numberline {3}Anatomy of a List}{9}%
\contentsline {paragraph}{Linked List declaration:}{11}%
\contentsline {chapter}{\numberline {4}Functionality of a Doubly-Linked List}{12}%
\contentsline {section}{\numberline {4.1}The Constructor}{12}%
\contentsline {section}{\numberline {4.2}The Destructor}{12}%
\contentsline {section}{\numberline {4.3}Adding new data}{13}%
\contentsline {subsection}{\numberline {4.3.1}PushFront}{13}%
\contentsline {subsubsection}{Push Front - Scenario 1 - Adding to an empty list:}{13}%
\contentsline {paragraph}{Initial state:}{13}%
\contentsline {paragraph}{Step 1: Create Node}{14}%
\contentsline {subsubsection}{Step 2: Update Pointers}{14}%
\contentsline {subsubsection}{Push Front - Scenario 2 - Adding to a non-empty list:}{15}%
\contentsline {paragraph}{Initial state:}{15}%
\contentsline {paragraph}{Step 1: Create Node}{16}%
\contentsline {paragraph}{Step 2: Update Pointers}{17}%
\contentsline {subsection}{\numberline {4.3.2}PushBack}{18}%
\contentsline {subsubsection}{Push Back - Scenario 1 - Adding to an empty list:}{18}%
\contentsline {subparagraph}{Initial state:}{18}%
\contentsline {subparagraph}{Step 1: Create Node}{19}%
\contentsline {subparagraph}{Step 2: Update pointers}{19}%
\contentsline {subsubsection}{Push Back - Scenario 2 - Adding to a non-empty list:}{20}%
\contentsline {subparagraph}{Initial state:}{20}%
\contentsline {subparagraph}{Step 1: Create Node}{21}%
\contentsline {subparagraph}{Step 2: Update pointers}{22}%
\contentsline {section}{\numberline {4.4}Removing data}{23}%
\contentsline {subsection}{\numberline {4.4.1}PopFront}{23}%
\contentsline {subsubsection}{Pop Front - Scenario 1 - Removing the last item:}{23}%
\contentsline {subparagraph}{Initial state:}{23}%
\contentsline {paragraph}{Step 1: Free the memory}{24}%
\contentsline {paragraph}{Step 2: Update pointers}{24}%
\contentsline {subsubsection}{Pop Front - Scenario 2 - There are 2 or more items in the list:}{25}%
\contentsline {paragraph}{Initial state:}{25}%
\contentsline {paragraph}{Step 1: Set the new first}{25}%
\contentsline {paragraph}{Step 2: Delete the old first Node}{26}%
\contentsline {paragraph}{Step 3: Update \texttt {ptrFirst}'s \texttt {ptrPrev} to point to nullptr.}{26}%
\contentsline {subsection}{\numberline {4.4.2}PopBack}{27}%
\contentsline {subsubsection}{Pop Back - Scenario 1 - Removing the last item:}{27}%
\contentsline {subparagraph}{Initial state:}{27}%
\contentsline {paragraph}{Step 1: Free the memory}{28}%
\contentsline {paragraph}{Step 2: Update pointers}{28}%
\contentsline {subsubsection}{Pop Back - Scenario 2 - There are 2 or more items in the list:}{29}%
\contentsline {paragraph}{Initial state:}{29}%
\contentsline {paragraph}{Step 1: Set the new last}{29}%
\contentsline {paragraph}{Step 2: Delete the old last Node}{30}%
\contentsline {paragraph}{Step 3: Update \texttt {ptrLast}'s \texttt {ptrNext} to point to nullptr.}{30}%
\contentsline {section}{\numberline {4.5}Accessing data}{31}%
\contentsline {subsection}{\numberline {4.5.1}GetFront}{31}%
\contentsline {subsection}{\numberline {4.5.2}GetBack}{31}%
\contentsline {subsection}{\numberline {4.5.3}GetAtIndex}{32}%
\contentsline {paragraph}{Step 1: Creating a pointer to walk the list}{33}%
\contentsline {paragraph}{Step 2: Writing a loop}{33}%
\contentsline {paragraph}{Step 3: Return the data}{34}%
\contentsline {section}{\numberline {4.6}Helper functions}{35}%
\contentsline {subsection}{\numberline {4.6.1}Clear}{35}%
\contentsline {subsection}{\numberline {4.6.2}IsEmpty}{35}%
\contentsline {subsection}{\numberline {4.6.3}Size}{35}%
\contentsline {chapter}{\numberline {5}Types of Linked Lists}{36}%
