\begin{center}
    \includegraphics[width=12cm]{images/nodes.png}
\end{center}

        The \textbf{Node} is one of the two structures we build when
        implementing a linked list or other linked type structure.
        The node is responsible for storing:
        
        \begin{itemize}
            \item   The \textbf{data} itself ~\\ 
                    (e.g., \texttt{T data})
            \item   A pointer to the \textbf{next node} ~\\
                    (e.g. \texttt{Node<T>* ptrNext}
            \item   A pointer to the \textbf{previous node} if it's a node for a doubly-linked-list. ~\\
                    (e.g., \texttt{Node<T>* ptrPrev})
        \end{itemize}
        
        \vspace{1cm}
        
        \begin{tabular}{| c | c |} \hline
            Node for a & Node for a \\
            Singly-linked list & Doubly-linked list \\
            \includegraphics[width=0.4\textwidth]{images/node-single.png}
            &
            \includegraphics[width=0.4\textwidth]{images/node-double.png}
            \\
            \footnotesize Only points to the next item
            &
            \footnotesize Points to both previous and next items
            \\ \hline
        \end{tabular}

        \newpage
        \paragraph{Singly-linked List's Nodes}

        \begin{center}
            \includegraphics[width=14cm]{images/singly-linked-nodes.png}
        \end{center}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        With a \textbf{singly-linked list}, the nodes only
        point to the \textbf{next Node} after them.
        These nodes don't know what comes before each of themselves.

    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \begin{tabular}{| l l l |} \hline
            \multicolumn{3}{|c|}{ \cellcolor{blue!25} \textbf{SinglyLinkedNode$<$TYPE$>$} } \\ \hline
            \multicolumn{3}{|l|}{ + \texttt{SinglyLinkedNode<TYPE>()} } \\ \hline
            + \texttt{data} & : & \texttt{TYPE} \\
            + \texttt{ptrNext} & : & \texttt{Node<TYPE>*} \\ \hline
        \end{tabular}
    \end{subfigure}
\end{figure}

        \hrulefill
        \paragraph{Doubly-linked List's Nodes}

        \begin{center}
            \includegraphics[width=14cm]{images/doubly-linked-nodes.png}
        \end{center}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \begin{tabular}{| l l l |} \hline
            \multicolumn{3}{|c|}{ \cellcolor{blue!25} \textbf{DoublyLinkedNode$<$TYPE$>$} } \\ \hline
            \multicolumn{3}{|l|}{ + \texttt{DoublyLinkedNode<TYPE>()} } \\ \hline
            + \texttt{data} & : & \texttt{TYPE} \\
            + \texttt{ptrNext} & : & \texttt{Node<TYPE>*} \\
            + \texttt{ptrPrev} & : & \texttt{Node<TYPE>*} \\ \hline
        \end{tabular}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        With a \textbf{doubly-linked list}, the nodes
        point to both the \textbf{next Node} and the \textbf{previous node}.
    \end{subfigure}
\end{figure}

        \begin{error}{Warning!}
        Remember that any class with pointers in it should be setting
        those pointers to \texttt{nullptr} in its constructor!
        \end{error}

        \newpage
        A Node is simple enough that you could implement it as a \textbf{struct}
        instead of a \textbf{class}, but it's up to you. A Node is
        only used by the List (or linked structure) itself, the user wouldn't
        be accessing the Node or know about it.

        \paragraph{A doubly-linked Node:} ~\\
\begin{lstlisting}[style=code]
template <typename T>
struct Node
{
public:
    Node();

    Node<T>* ptrNext;
    Node<T>* ptrPrev;

    T data;
};
\end{lstlisting}

        \paragraph{A singly-linked Node:} ~\\
\begin{lstlisting}[style=code]
template <typename T>
struct Node
{
public:
    Node();

    Node<T>* ptrNext;

    T data;
};
\end{lstlisting}

        \paragraph{Single vs. Double?} ~\\
        I generally prefer to teach how to implement a doubly-linked list
        first, because the functionality is easier to implement when each
        Node is aware of what comes before itself.

        Once you understand how a doubly-linked list works, you can figure
        out a singly-linked list; it just means having to iterate through
        the list more often.

