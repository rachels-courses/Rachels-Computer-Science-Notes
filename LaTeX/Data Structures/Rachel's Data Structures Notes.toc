\contentsline {chapter}{\numberline {1}Introduction to Data Structures and Algorithm Analysis}{4}%
\contentsline {section}{\numberline {1.1}What are data structures?}{5}%
\contentsline {paragraph}{``Don't all the classes we write hold data? How is this different from other objects I've defined?''}{5}%
\contentsline {section}{\numberline {1.2}What is algorithm analysis?}{7}%
\contentsline {paragraph}{Example: Scalability}{7}%
\contentsline {paragraph}{Example: Efficiency of an Array-based structure}{8}%
\contentsline {section}{\numberline {1.3}The point of a Data Structures class}{9}%
\contentsline {paragraph}{``So why are we learning to write data structures if they've already been written for us?''}{9}%
\contentsline {chapter}{\numberline {2}Our first data structures}{10}%
\contentsline {section}{\numberline {2.1}Fixed-length array structure}{10}%
\contentsline {subsection}{Functionality for our data type}{11}%
\contentsline {paragraph}{Creating the array:}{11}%
\contentsline {paragraph}{Adding onto the array:}{11}%
\contentsline {paragraph}{Removing items from the array:}{12}%
\contentsline {paragraph}{Searching for an item:}{13}%
\contentsline {paragraph}{Retrieving an item at some index:}{14}%
\contentsline {paragraph}{Visualizing the array:}{14}%
\contentsline {subsection}{Predicting errors/exceptions}{14}%
\contentsline {paragraph}{Invalid indices:}{14}%
\contentsline {paragraph}{Array is full:}{14}%
\contentsline {subsection}{Creating a class to wrap the array}{15}%
\contentsline {subsection}{Constructor - Preparing the array to be used}{17}%
\contentsline {subsection}{Helper functions}{17}%
\contentsline {subsubsection}{int Size() const}{17}%
\contentsline {subsubsection}{bool IsEmpty() const}{17}%
\contentsline {subsubsection}{bool IsFull() const}{17}%
\contentsline {subsubsection}{bool IsValidIndex( int index )}{18}%
\contentsline {subsubsection}{void ShiftRight( int index )}{18}%
\contentsline {subparagraph}{Error checks:}{18}%
\contentsline {subparagraph}{Functionality:}{18}%
\contentsline {subsubsection}{void ShiftLeft( int index )}{19}%
\contentsline {subparagraph}{Error checks:}{19}%
\contentsline {subparagraph}{Functionality:}{19}%
\contentsline {subsubsection}{void Display() const}{19}%
\contentsline {subsubsection}{int Search( T item ) const}{20}%
\contentsline {subsection}{Push - adding items to the array}{20}%
\contentsline {paragraph}{PushBack:}{20}%
\contentsline {paragraph}{PushFront:}{21}%
\contentsline {paragraph}{PushAt:}{21}%
\contentsline {paragraph}{Additional design considerations:}{21}%
\contentsline {subsubsection}{Implementing \texttt {void PushBack( T newItem )}}{22}%
\contentsline {subparagraph}{Error checks:}{22}%
\contentsline {subparagraph}{Functionality:}{22}%
\contentsline {subsubsection}{Implementing \texttt {void PushFront( T newItem )}}{22}%
\contentsline {subparagraph}{Error checks:}{22}%
\contentsline {subparagraph}{Preparation:}{22}%
\contentsline {subparagraph}{Functionality:}{22}%
\contentsline {subsubsection}{Implementing \texttt {void PushAt( T newItem )}}{23}%
\contentsline {subparagraph}{DRY (Don't Repeat Yourself) checks:}{23}%
\contentsline {subparagraph}{Error checks:}{23}%
\contentsline {subparagraph}{Preparation:}{23}%
\contentsline {subparagraph}{Functionality:}{23}%
\contentsline {subsection}{Pop - removing items from the array}{24}%
\contentsline {subsubsection}{Implementing \texttt {void PopBack()}}{24}%
\contentsline {subparagraph}{Error checks:}{24}%
\contentsline {subparagraph}{Functionality:}{24}%
\contentsline {subsubsection}{Implementing \texttt {void PopFront()}}{24}%
\contentsline {subparagraph}{Error checks:}{24}%
\contentsline {subparagraph}{Functionality:}{24}%
\contentsline {subsubsection}{Implementing \texttt {void PopAt( int index )}}{24}%
\contentsline {subparagraph}{Error checks:}{24}%
\contentsline {subparagraph}{Functionality:}{24}%
\contentsline {subsection}{Get - accessing items in the array}{25}%
\contentsline {subsubsection}{Implementing \texttt {T GetBack()}}{25}%
\contentsline {subparagraph}{Error checks:}{25}%
\contentsline {subparagraph}{Functionality:}{25}%
\contentsline {subsubsection}{Implementing \texttt {T GetFront()}}{25}%
\contentsline {subparagraph}{Error checks:}{25}%
\contentsline {subparagraph}{Functionality:}{25}%
\contentsline {subsubsection}{Implementing \texttt {T GetAt( int index )}}{25}%
\contentsline {subparagraph}{Error checks:}{25}%
\contentsline {subparagraph}{Functionality:}{25}%
\contentsline {subsection}{Search}{25}%
\contentsline {subsection}{SmartFixedArray code}{26}%
\contentsline {section}{\numberline {2.2}Dynamic array structure}{33}%
\contentsline {subsection}{Constructor - Preparing the array to be used}{35}%
\contentsline {subsection}{Destructor - Cleaning up the array}{35}%
\contentsline {subsection}{void AllocateMemory( int size )}{36}%
\contentsline {subparagraph}{Error checks:}{36}%
\contentsline {subparagraph}{Functionality:}{36}%
\contentsline {subsection}{void Resize( int newSize )}{37}%
\contentsline {subparagraph}{Error checks:}{37}%
\contentsline {subparagraph}{Functionality:}{37}%
\contentsline {subsection}{Updating other functions}{38}%
\contentsline {chapter}{\numberline {3}Linked Lists}{39}%
\contentsline {section}{\numberline {3.1}Coming from arrays...}{40}%
\contentsline {subsection}{Investigating arrays and memory addresses:}{40}%
\contentsline {subsection}{Pros and cons of arrays for storing data:}{43}%
\contentsline {paragraph}{Pros:}{43}%
\contentsline {paragraph}{Cons:}{43}%
\contentsline {section}{\numberline {3.2}Anatomy of a Node}{44}%
\contentsline {paragraph}{Singly-linked List's Nodes}{45}%
\contentsline {paragraph}{Doubly-linked List's Nodes}{45}%
\contentsline {paragraph}{A doubly-linked Node:}{46}%
\contentsline {paragraph}{A singly-linked Node:}{46}%
\contentsline {paragraph}{Single vs. Double?}{46}%
\contentsline {section}{\numberline {3.3}Anatomy of a List}{47}%
\contentsline {paragraph}{Linked List declaration:}{49}%
\contentsline {section}{\numberline {3.4}Functionality of a Doubly-Linked List}{50}%
\contentsline {subsection}{The Constructor}{50}%
\contentsline {subsection}{The Destructor}{50}%
\contentsline {subsection}{Adding new data}{51}%
\contentsline {subsubsection}{PushFront}{51}%
\contentsline {paragraph}{Push Front - Scenario 1 - Adding to an empty list:}{51}%
\contentsline {subparagraph}{Initial state:}{51}%
\contentsline {subparagraph}{Step 1: Create Node}{52}%
\contentsline {subparagraph}{Step 2: Update Pointers}{52}%
\contentsline {paragraph}{Push Front - Scenario 2 - Adding to a non-empty list:}{53}%
\contentsline {subparagraph}{Initial state:}{53}%
\contentsline {subparagraph}{Step 1: Create Node}{54}%
\contentsline {subparagraph}{Step 2: Update Pointers}{55}%
\contentsline {subsubsection}{PushBack}{56}%
\contentsline {paragraph}{Push Back - Scenario 1 - Adding to an empty list:}{56}%
\contentsline {subparagraph}{Initial state:}{56}%
\contentsline {subparagraph}{Step 1: Create Node}{57}%
\contentsline {subparagraph}{Step 2: Update pointers}{57}%
\contentsline {paragraph}{Push Back - Scenario 2 - Adding to a non-empty list:}{58}%
\contentsline {subparagraph}{Initial state:}{58}%
\contentsline {subparagraph}{Step 1: Create Node}{59}%
\contentsline {subparagraph}{Step 2: Update pointers}{60}%
\contentsline {subsection}{Removing data}{61}%
\contentsline {subsubsection}{PopFront}{61}%
\contentsline {paragraph}{Pop Front - Scenario 1 - Removing the last item:}{61}%
\contentsline {subparagraph}{Initial state:}{61}%
\contentsline {paragraph}{Step 1: Free the memory}{62}%
\contentsline {subparagraph}{Step 2: Update pointers}{62}%
\contentsline {paragraph}{Pop Front - Scenario 2 - There are 2 or more items in the list:}{63}%
\contentsline {subparagraph}{Initial state:}{63}%
\contentsline {subparagraph}{Step 1: Set the new first}{63}%
\contentsline {subparagraph}{Step 2: Delete the old first Node}{64}%
\contentsline {subparagraph}{Step 3: Update \texttt {ptrFirst}'s \texttt {ptrPrev} to point to nullptr.}{64}%
\contentsline {subsubsection}{PopBack}{65}%
\contentsline {paragraph}{Pop Back - Scenario 1 - Removing the last item:}{65}%
\contentsline {subparagraph}{Initial state:}{65}%
\contentsline {subparagraph}{Step 1: Free the memory}{66}%
\contentsline {subparagraph}{Step 2: Update pointers}{66}%
\contentsline {paragraph}{Pop Back - Scenario 2 - There are 2 or more items in the list:}{67}%
\contentsline {subparagraph}{Initial state:}{67}%
\contentsline {subparagraph}{Step 1: Set the new last}{67}%
\contentsline {subparagraph}{Step 2: Delete the old last Node}{68}%
\contentsline {subparagraph}{Step 3: Update \texttt {ptrLast}'s \texttt {ptrNext} to point to nullptr.}{68}%
\contentsline {subsection}{Accessing data}{69}%
\contentsline {subsubsection}{GetFront}{69}%
\contentsline {subsubsection}{GetBack}{69}%
\contentsline {subsubsection}{GetAtIndex}{70}%
\contentsline {paragraph}{Step 1: Creating a pointer to walk the list}{71}%
\contentsline {paragraph}{Step 2: Writing a loop}{71}%
\contentsline {paragraph}{Step 3: Return the data}{72}%
\contentsline {subsection}{Helper functions}{73}%
\contentsline {subsubsection}{Clear}{73}%
\contentsline {subsubsection}{IsEmpty}{73}%
\contentsline {subsubsection}{Size}{73}%
\contentsline {chapter}{\numberline {4}Queues}{74}%
\contentsline {section}{\numberline {4.1}What are Queues?}{75}%
\contentsline {subsection}{Building a Queue}{76}%
\contentsline {subsection}{Adding to a Queue (Push/Enqueue)}{77}%
\contentsline {subsection}{Removing from a Queue (Pop/Dequeue)}{78}%
\contentsline {subsection}{Access with a Queue (Get/Peek)}{79}%
\contentsline {subsection}{Use of a Queue in software}{79}%
\contentsline {section}{\numberline {4.2}Implementing a Queue}{80}%
\contentsline {chapter}{\numberline {5}Stacks}{81}%
\contentsline {section}{\numberline {5.1}What are Stacks?}{82}%
\contentsline {subsection}{Adding to a Stack (Push)}{84}%
\contentsline {subsection}{Removing from a Stack (Pop)}{85}%
\contentsline {subsection}{Access with a Stack (Get/Peek)}{86}%
\contentsline {subsection}{Use of a Stack in software}{86}%
\contentsline {section}{\numberline {5.2}Implementing a Stack}{88}%
\contentsline {chapter}{\numberline {6}Trees}{89}%
\contentsline {section}{\numberline {6.1}Linear structures vs. tree structure}{90}%
\contentsline {section}{\numberline {6.2}Basic terminology}{90}%
\contentsline {paragraph}{Node:}{91}%
\contentsline {paragraph}{Edge:}{91}%
\contentsline {paragraph}{Root:}{91}%
\contentsline {paragraph}{Leaf:}{91}%
\contentsline {paragraph}{Parent:}{91}%
\contentsline {paragraph}{Child:}{92}%
\contentsline {paragraph}{Sibling:}{92}%
\contentsline {paragraph}{Ancestors:}{93}%
\contentsline {paragraph}{Descendants:}{93}%
\contentsline {paragraph}{Subtree:}{93}%
\contentsline {paragraph}{Height of a node $n$:}{94}%
\contentsline {paragraph}{Height of a tree:}{94}%
\contentsline {paragraph}{Depth of a node $n$:}{94}%
\contentsline {section}{\numberline {6.3}Types of trees}{95}%
\contentsline {paragraph}{General tree:}{95}%
\contentsline {paragraph}{$n$-ary tree:}{95}%
\contentsline {paragraph}{Binary tree:}{95}%
\contentsline {paragraph}{Binary search tree:}{95}%
\contentsline {subsection}{Tree fullness, completeness, and balance}{96}%
\contentsline {paragraph}{Full binary tree:}{96}%
\contentsline {paragraph}{Complete binary tree:}{96}%
\contentsline {paragraph}{Balanced binary tree:}{97}%
\contentsline {section}{\numberline {6.4}Binary tree traversals}{97}%
\contentsline {subsection}{Preorder traversal}{98}%
\contentsline {subsection}{Inorder traversal}{98}%
\contentsline {subsection}{Postorder traversal}{98}%
\contentsline {subsection}{Step-by-step example: Preorder traversal}{99}%
\contentsline {paragraph}{Algorithm:}{99}%
\contentsline {subsection}{Step-by-step example: Inorder traversal}{100}%
\contentsline {paragraph}{Algorithm:}{100}%
\contentsline {subsection}{Step-by-step example: Postorder traversal}{101}%
\contentsline {paragraph}{Algorithm:}{101}%
\contentsline {chapter}{\numberline {7}Binary Search Trees}{102}%
\contentsline {section}{\numberline {7.1}Introduction to Binary Search Trees}{103}%
\contentsline {section}{\numberline {7.2}Architecture of a Binary Search Tree}{104}%
\contentsline {subsection}{BinarySearchTreeNode in C++:}{106}%
\contentsline {subsection}{BinarySearchTree in C++:}{107}%
\contentsline {section}{\numberline {7.3}Efficiency of a Binary Search Tree}{108}%
\contentsline {section}{\numberline {7.4}The Binary Search Tree and Recursion}{109}%
\contentsline {section}{\numberline {7.5}Functionality of a Binary Search Tree}{110}%
\contentsline {subsection}{The Constructor}{110}%
\contentsline {subsection}{The Destructor}{110}%
\contentsline {subsection}{Push}{111}%
\contentsline {subsection}{RecursivePush}{112}%
\contentsline {subsubsection}{Terminating cases:}{112}%
\contentsline {subsubsection}{Recursive cases:}{112}%
\contentsline {subsection}{Contains}{113}%
\contentsline {subsection}{RecursiveContains}{113}%
\contentsline {subsubsection}{Terminating cases:}{113}%
\contentsline {subsubsection}{Recursive cases:}{113}%
\contentsline {subsection}{GetData}{114}%
\contentsline {subsection}{FindNode}{114}%
\contentsline {subsection}{RecursiveFindNode}{115}%
\contentsline {subsubsection}{Terminating cases:}{115}%
\contentsline {subsubsection}{Recursive cases:}{115}%
\contentsline {subsection}{GetMinKey}{116}%
\contentsline {subsection}{RecursiveGetMin}{116}%
\contentsline {subsubsection}{Terminating cases:}{116}%
\contentsline {subsubsection}{Recursive case:}{116}%
\contentsline {subsection}{GetMaxKey}{117}%
\contentsline {subsection}{RecursiveGetMax}{117}%
\contentsline {subsubsection}{Terminating cases:}{117}%
\contentsline {subsubsection}{Recursive case:}{117}%
\contentsline {subsection}{GetHeight}{118}%
\contentsline {subsection}{RecursiveGetHeight}{118}%
\contentsline {subsection}{GetInOrder}{119}%
\contentsline {subsection}{RecursiveGetInOrder}{119}%
\contentsline {subsubsection}{Terminating case:}{119}%
\contentsline {subsubsection}{Recursive case:}{119}%
\contentsline {subsection}{GetPreOrder}{120}%
\contentsline {subsection}{RecursiveGetPreOrder}{120}%
\contentsline {subsubsection}{Terminating case:}{120}%
\contentsline {subsubsection}{Recursive case:}{120}%
\contentsline {subsection}{GetPostOrder}{121}%
\contentsline {subsection}{RecursiveGetPostOrder}{121}%
\contentsline {subsubsection}{Terminating case:}{121}%
\contentsline {subsubsection}{Recursive case:}{121}%
\contentsline {chapter}{\numberline {8}Hash Tables}{122}%
\contentsline {section}{\numberline {8.1}Back to arrays...}{123}%
\contentsline {section}{\numberline {8.2}Concepts}{124}%
\contentsline {subsection}{Key-value pairs}{124}%
\contentsline {subsection}{Hashing the key to get the index}{125}%
\contentsline {subsection}{Hashing functions}{126}%
\contentsline {subsection}{Collisions}{127}%
\contentsline {subsubsection}{Linear probing (open addressing):}{127}%
\contentsline {subsubsection}{Quadratic probing (open addressing):}{128}%
\contentsline {subsubsection}{Double hashing (open addressing):}{129}%
\contentsline {subsubsection}{Note: Index wrap-around}{130}%
\contentsline {subsubsection}{The problem of clustering}{130}%
\contentsline {subsection}{Sizing a Hash Table array}{130}%
\contentsline {section}{\numberline {8.3}Hash Table efficiency}{131}%
\contentsline {paragraph}{Pros:}{131}%
\contentsline {paragraph}{Cons:}{131}%
\contentsline {section}{\numberline {8.4}See also...}{132}%
