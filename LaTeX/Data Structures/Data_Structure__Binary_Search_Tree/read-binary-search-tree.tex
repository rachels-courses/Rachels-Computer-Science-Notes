
    \begin{center}
        \includegraphics[width=12cm]{images/binary-search-tree.png}
    \end{center}

    \section{Introduction to Binary Search Trees}
    
        \begin{center}
            \includegraphics[width=6cm]{images/tree-binarysearchtree.png}
        \end{center}
    
    
        Binary search trees are a type of data structure that keep the data
        it contains \textbf{ordered}. The ordering process happens when
        a new piece of data is entered by finding a location for the data
        that adheres to the ordering rules. With a binary search tree,
        \textbf{smaller values} are stored to the left and \textbf{larger values}
        are stored to the right.
        
        This means that when we're searching for data, when we land at a node
        we can figure out whether to traverse \textit{left} or \textit{right}
        by comparing the node to what we're searching for.
        
    \newpage
    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    
    \section{Architecture of a Binary Search Tree}
    
        \begin{center}
            \includegraphics[width=14cm]{Data_Structure__Binary_Search_Tree/images/linkedlistdiagram}
        \end{center}
        With a \textbf{Linked List}, we need to implement a Node structure
        and a LinkedList class, where the Node stores the data and the LinkedList
        provides an interface for users to add, remove, and search for data
        and stores a pointer to the \textbf{first} and \textbf{last} elements.
        
        \vspace{1cm}
        
        \begin{center}
            \includegraphics[width=14cm]{Data_Structure__Binary_Search_Tree/images/binarysearchtreediagram}
        \end{center}
        Similarly for a \textbf{Binary Search Tree}, we need another type of Node
        to store the data, as well as the BinarySearchTree structure that
        acts as an interface and keeps a pointer to the \textbf{root node}.
        
        \newpage
        We might also think of a Binary Search Tree as being ordered based
        on the data's \textbf{key} - some sort of unique identifier we
        assign to each node - and then containing additional data (a value)
        within the node. 
        
        ~\\
        For example, if we create our Node with two templated types like this:
        \begin{center}
            \includegraphics[width=7cm]{Data_Structure__Binary_Search_Tree/images/bstnode}
        \end{center}
        our \textbf{key} could be a unique lookup (e.g., ``employee ID''),
        and the \textbf{data}/value could be another structure that
        stores more employee data (name, department, etc.)
        \begin{center}
            \includegraphics[width=14cm]{Data_Structure__Binary_Search_Tree/images/bst-employees}
        \end{center}
        
        What's the significance of the hierarchy in this tree? 
        Nothing, really - the point of a binary search tree is that we're assuming
        the \textbf{keys} we will be pushing into the tree will be in
        a somewhat \textbf{random order}, and using the BST structure
        will help keep things ordered and somewhat faster to search through.
        
        We could use a Binary Search Tree in another application where the order
        of the keys \textit{does} have some significance, but that's all a design decision.
        
    \newpage
    
    \subsection{BinarySearchTreeNode in C++:}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
class Node
{
public:
    Node()
    {
        ptrLeft = nullptr;
        ptrRight = nullptr;
    }

    Node( TK newKey, TD newData )
    {
        key = newKey;
        data = newData;
        ptrLeft = nullptr;
        ptrRight = nullptr;
    }

    ~Node()
    {
        if ( ptrLeft != nullptr  ) { delete ptrLeft; }
        if ( ptrRight != nullptr ) { delete ptrRight; }
    }

    Node<TK, TD>* ptrLeft;
    Node<TK, TD>* ptrRight;

    TD data;
    TK key;
};
\end{lstlisting}

        The node I've written here contains a \textbf{key}, which nodes will
        be ordered by, and \textbf{data}, which can contain more
        information. ~\\
        
        As with any structure utilizing \textbf{pointers}, the pointers
        should be initialized to \texttt{nullptr} in any constructors. ~\\
        
        The \textbf{destructor} here will trigger the deletion of any
        child nodes, creating a chain reaction to clean up the entire
        tree if the root node is deleted.
    
    
    \newpage
    \subsection{BinarySearchTree in C++:}
    
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
class BinarySearchTree
{
public:
    BinarySearchTree();
    ~BinarySearchTree();

    // Basic functionality
    void Push( const TK& newKey, const TD& newData );
    bool Contains( const TK& key );
    TD& GetData( const TK& key );
    void Delete( const TK& key );

    // Traversal functions
    string GetInOrder();
    string GetPreOrder();
    string GetPostOrder();

    // Additional functionality
    TK& GetMinKey();
    TK& GetMaxKey();
    int GetCount();
    int GetHeight();

private:
    // (more here)

private:
    Node<TK, TD>* m_ptrRoot;
    int m_nodeCount;
};
\end{lstlisting}

    A Binary Search Tree, just like other data structures, can store more
    functionality than this, or less if needed. ~\\
    
    There are additional \textbf{private methods} that would be implemented.
    This declaration is just showing the \textbf{public (interface) methods}
    and the \textbf{private member variables}. I will talk about the
    private helper methods in depth later.
    
    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    
    \newpage
    \section{Efficiency of a Binary Search Tree}
    
        The Binary Search Tree ends up being a good compromise between
        choosing \textbf{faster random access but slow search/insert/delete} (like with a dynamic array)
        and \textbf{faster inserts/deletes but slow search/access} (like with a linked list).
        ~\\
        
        The Binary Search Tree ends up being slower than $O(1)$ (instant) but
        faster than $O(n)$ (linear) for all of its operations:
        
        \begin{center}
            \begin{tabular}{| l || c | c | c | c |} \hline
                \textbf{Structure}              & \textbf{Random access}    & \textbf{Search}   & \textbf{Insert}   & \textbf{Delete} 
                \\ \hline
                \textbf{Dynamic Array}          & $O(1)$                    & $O(n)$            & $O(n)$            & $O(n)$
                \\ \hline
                \textbf{Linked List}            & $O(n)$                    & $O(n)$            & $O(1)$            & $O(1)$
                \\ \hline
                \textbf{Binary Search Tree}     & $O(log(n))$               & $O(log(n))$       & $O(log(n))$       & $O(log(n))$
                \\ \hline
            \end{tabular}
        \end{center}
        
        Why is this? By the nature of its tree structure, as we traverse
        the tree we're essentially \textbf{cutting out half the tree}
        each time we choose to go \textit{left} or \textit{right}.
        Halfing the nodes to search \textit{each cycle} means we have
        the opposite of exponential growth: A logarithmic function.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.3\textwidth}
        \centering
        
        Constant growth ~\\ $O(1)$ ~\\~\\
        
        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=2,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] coordinates
              {(0,0.5) (20,0.5)};
          \end{axis}
        \end{tikzpicture}

        }
        
    \end{subfigure}%
    \begin{subfigure}{.3\textwidth}
        \centering
        
        Logarithmic growth ~\\ $O(log(n))$ ~\\~\\
        
        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=2,xticklabel=\empty,yticklabel=\empty]
            \addplot[red] {log10(x)};
          \end{axis}
        \end{tikzpicture}

        }
        
    \end{subfigure}%
    \begin{subfigure}{.3\textwidth}
        \centering
        
        Linear growth ~\\
        $O(n)$ ~\\~\\
           
        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=5,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] coordinates
              {(0,0) (5,5)};
          \end{axis}
        \end{tikzpicture}

        }
        
    \end{subfigure}
\end{figure}

    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    
    %--------------------------------------------------------------------%
    %--------------------------------------------------------------------%
    
    \newpage
    \section{The Binary Search Tree and Recursion}
    
        Many of the BinarySearchTree functions will be \textbf{recursive},
        starting at the root node and recursing down. Because of this,
        the Push function (and many others) that would actually do the work would look like this:
        
\begin{lstlisting}[style=code]
void RecursivePush( 
    TK newKey, 
    TD newData, 
    Node<TK, TD>* ptrCurrent );
\end{lstlisting}

        However, we don't want the user \textit{outside of the BinarySearchTree}
        to have to call Push and pass in the tree's node. They shouldn't even
        have access to any \texttt{Node} objects...
    
\begin{lstlisting}[style=code]
myTree.Push( 'a', "apple", ???? ); // What do I pass in?
\end{lstlisting}


        ~\\ That's why we have the \textbf{public Push function}...
        
\begin{lstlisting}[style=code]
void Push( TK newKey, TD newData );
\end{lstlisting}


        ~\\ And a \textbf{private RecursivePush function}...

\begin{lstlisting}[style=code]
void RecursivePush( TK newKey, TD newData, 
    Node<TK, TD>* ptrCurrent );
\end{lstlisting}

        ~\\ Where the user calls the \textbf{public Push} and that function
        makes the first call to \textbf{RecursivePush}, passing in the
        \textbf{root node} to begin operations on.

\begin{lstlisting}[style=code]
void Push( TK newKey, TD newData )
{
    RecursivePush( newKey, newData, m_ptrRoot );
}
\end{lstlisting}

    %\newpage
        %\subsection{Private recursive functions}
        
        %\begin{itemize}
            %\item   \texttt{void Push( TK newKey, TD newData )} calls ~\\
                    %\texttt{ RecursivePush( newKey, newData, m\_ptrRoot ); }
                    
            %\item   \texttt{bool Contains( TK key )} calls ~\\
                    %\texttt{ return RecursiveContains( key, m\_ptrRoot ); }
                    
            %\item   \texttt{string GetPreOrder()} calls ~\\
                    %\texttt{ return RecursiveGetPreOrder( m\_ptrRoot ); }
                    
            %\item   \texttt{string GetInOrder()} calls ~\\
                    %\texttt{ return RecursiveGetInOrder( m\_ptrRoot ); }
                    
            %\item   \texttt{string GetPostOrder()} calls ~\\
                    %\texttt{ return RecursiveGetPostOrder( m\_ptrRoot ); }
                    
            %\item   \texttt{Node<TK,TD>* FindNode( TK key )} calls ~\\
                    %\texttt{ return RecursiveFindNode( key, m\_ptrRoot ); }
                    
            %\item   \texttt{TK GetMaxKey()} calls ~\\
                    %\texttt{ return RecursiveGetMaxKey( m\_ptrRoot ); }
                    
            %\item   \texttt{TK GetMinKey()} calls ~\\
                    %\texttt{ return RecursiveGetMinKey( m\_ptrRoot ); }
                    
            %\item   \texttt{int GetHeight()} calls ~\\
                    %\texttt{ return RecursiveGetHeight( m\_ptrRoot ); }
        %\end{itemize}
        
        % Delete functionality



    \newpage
    \section{Functionality of a Binary Search Tree}
    
        \subsection{The Constructor}

\begin{lstlisting}[style=code]
emplate <typename TK, typename TD>
BinarySearchTree<TK,TD>::BinarySearchTree()
{
    m_ptrRoot = nullptr;
    m_nodeCount = 0;
}
\end{lstlisting}

        The constructor of the BinarySearchTree should set the \texttt{m\_ptrRoot}
        pointer to \texttt{nullptr} and initialize the \texttt{m\_nodeCount} to 0.

        \vspace{1cm}
        
        \subsection{The Destructor}
        
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
BinarySearchTree<TK,TD>::~BinarySearchTree()
{
    if ( m_ptrRoot != nullptr )
    {
        delete m_ptrRoot;
    }
}
\end{lstlisting}

        The destructor of the BinarySearchTree will check to see if
        \texttt{m\_ptrRoot} is not null - if it's not null, we will free
        that memory with the \texttt{delete} command. Since our
        \texttt{BinarySearchTreeNode} destructor deletes its left and right
        children, we don't have to do anything special to clear out the whole
        tree - that will happen automatically.
        

        \newpage
        \subsection{Push}
        
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::Push( const TK& newKey, const TD& newData )
{
    if ( Contains( newKey ) )
    {
        throw runtime_error( "Key is not unique!" );
    }
    else
    {
        RecursivePush( newKey, newData, m_ptrRoot );
    }
}
\end{lstlisting}
        
            Remember that the \texttt{Push} function is just a public-facing
            starting point that will begin the recursive process by calling
            the \texttt{RecursivePush} function, starting at the root node.
            ~\\
            
            First, before starting the recursion, use the \texttt{Contains}
            method to see if the tree \textit{already contains} the \texttt{newKey}
            passed in. 
            
            If this key is already in the tree, we'll have to decide how to handle
            that. In our example, we will just throw a \texttt{runtime\_error}
            exception, because for this design we're assuming that each key
            is a \textit{unique identifier}.
            ~\\
            
            If the key is \textit{not} already in the tree, then 
            we are going to call the \texttt{RecursivePush} function to get
            started. \texttt{RecursivePush} takes in the key, data,
            and a node, so we pass on the \texttt{newKey} and \texttt{newData}
            parameters and we start at \texttt{m\_ptrRoot}.
            
            
            
            

            If the root is already storing some data, then we call             
            \texttt{RecursivePush}, passing forward the \texttt{newKey},
            \texttt{newData}, and the \texttt{m\_ptrRoot} as the starting point.
            
            \newpage
            \subsection{RecursivePush}
            
            Within the RecursivePush function, we are looking for a position
            to place our new node at. While we haven't found an appropriate position,
            we will continue recursing left or right to ensure our keys are sorted.
            
            \subsubsection{Terminating cases:}
            
                \begin{itemize}
                    \item   If \texttt{ptrCurrent} is nullptr, then the node can be created here.
\begin{lstlisting}[style=code]
ptrCurrent = new Node<TK,TD>(newKey, newData);
m_nodeCount++;
\end{lstlisting}
                    
                    \item   If \texttt{newKey} is less than \texttt{ptrCurrent}'s key, AND
                            if \texttt{ptrCurrent} DOES NOT HAVE a left child, then we can
                            create the new node to the left of \texttt{ptrCurrent}.
\begin{lstlisting}[style=code]
ptrCurrent->ptrLeft = new Node<TK,TD>(newKey, newData);
m_nodeCount++;
\end{lstlisting}
                            
                    \item   If \texttt{newKey} is greater than \texttt{ptrCurrent}'s key, AND
                            if \texttt{ptrCurrent} DOES NOT HAVE a right child, then we can
                            create the new node to the right of \texttt{ptrCurrent}.
\begin{lstlisting}[style=code]
ptrCurrent->ptrRight = new Node<TK,TD>(newKey, newData);
m_nodeCount++;
\end{lstlisting}

                \end{itemize}
            
            \subsubsection{Recursive cases:}
            
                \begin{itemize}
                    \item   If the \texttt{newKey} is less than \texttt{ptrCurrent}'s key, AND
                            \texttt{ptrCurrent} HAS a left child, then we
                            \textbf{recurse} to the left.
\begin{lstlisting}[style=code]
// Recurse left
RecursivePush(newKey, newData, 
    ptrCurrent->ptrLeft);
\end{lstlisting}
                            
                    \item   If the \texttt{newKey} is greater than \texttt{ptrCurrent}'s key, AND
                            \texttt{ptrCurrent} HAS a right child, then we
                            \textbf{recurse} to the right.
\begin{lstlisting}[style=code]
// Recurse right
RecursivePush(newKey, newData, 
    ptrCurrent->ptrRight);
\end{lstlisting}
                \end{itemize}
            
            
            
        \newpage
        \subsection{Contains}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
bool BinarySearchTree<TK,TD>::Contains( const TK& key )
{
    return RecursiveContains( key, m_ptrRoot );
}
\end{lstlisting}

            The \texttt{Contains} method also is a public entry-point
            that will start the \texttt{RecursiveContains} method at
            the \texttt{m\_ptrRoot}.
        
        \vspace{1cm}
        
        \subsection{RecursiveContains}
            \texttt{RecursiveContains} will look at the current node \texttt{ptrCurrent}.
            If the \texttt{ptrCurrent}'s key matches the \texttt{key} we're searching for,
            then we'll return \textbf{true}. Otherwise, we will recurse left or right searching for
            the key. If we run out of nodes to search, then the key is not found and we return \textbf{false}.
            
            \subsubsection{Terminating cases:}
                \begin{itemize}
                    \item   If \texttt{ptrCurrent} is nullptr, then we've run out of nodes to search - 
                            return false.
                        
                    \item   If \texttt{ptrCurrent}'s key matches the \texttt{key} parameter, then
                            we've found the key we're looking for - return true.
                \end{itemize}
            
            
            \subsubsection{Recursive cases:}
                \begin{itemize}
                    \item   If \texttt{key} is less than \texttt{ptrCurrent}'s key, then
                            \textbf{recurse} left.
\begin{lstlisting}[style=code]
return RecursiveContains(key, ptrCurrent->ptrLeft);
\end{lstlisting}
                            
                    \item   If \texttt{key} is greater than \texttt{ptrCurrent}'s key, then
                            \textbf{recurse} right.
\begin{lstlisting}[style=code]
return RecursiveContains(key, ptrCurrent->ptrRight);
\end{lstlisting}
                \end{itemize}
            
            
        \newpage
            \subsection{GetData}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
TD& BinarySearchTree<TK,TD>::GetData( const TK& key )
{
    Node<TK, TD>* node = FindNode( key );
    if ( node == nullptr ) { throw runtime_error( "Key not found!" ); }
    return &(node->data);
}
\end{lstlisting}

            The \texttt{GetData} function takes in a \texttt{key} and returns
            the data for the given node. If that node does not exist in the tree,
            then an exception will be thrown.

            \vspace{1cm}
            \subsection{FindNode}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
Node<TK, TD>* BinarySearchTree<TK,TD>::FindNode( const TK& key )
{
    return RecursiveFindNode( key, m_ptrRoot );
}
\end{lstlisting}
            The \texttt{FindNode} function is the starting point that will
            begin the \texttt{RecursiveFindNode} at the \texttt{m\_ptrRoot}.
% \begin{lstlisting}[style=code]
% \end{lstlisting}

            \newpage
            \subsection{RecursiveFindNode}
                \texttt{RecursiveFindNode} will search the tree for the 
                \texttt{key} given, stopping only once found or once there
                are no more nodes to search.
        
            \subsubsection{Terminating cases:}
                \begin{itemize}
                    \item   If \texttt{ptrCurrent} is nullptr, then we've run out of nodes to search - 
                            return \texttt{nullptr}.
                        
                    \item   If \texttt{ptrCurrent}'s key matches the \texttt{key} parameter, then
                            we've found the key we're looking for - return the \texttt{ptrCurrent}.
                \end{itemize}
            
            
            \subsubsection{Recursive cases:}
                \begin{itemize}
                    \item   If \texttt{key} is less than \texttt{ptrCurrent}'s key, then
                            \textbf{recurse} left.
\begin{lstlisting}[style=code]
return RecursiveFindNode(key, ptrCurrent->ptrLeft);
\end{lstlisting}
                            
                    \item   If \texttt{key} is greater than \texttt{ptrCurrent}'s key, then
                            \textbf{recurse} right.
\begin{lstlisting}[style=code]
return RecursiveFindNode(key, ptrCurrent->ptrRight);
\end{lstlisting}
                \end{itemize}
        
        
        
        
        \newpage
        \subsection{GetMinKey}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMinKey()
{
    return RecursiveGetMin( m_ptrRoot );
}
\end{lstlisting}
            
            The \texttt{GetMinKey} function starts the recursive search
            for the minimum key, starting at \texttt{m\_ptrRoot}.


        \vspace{1cm}
        \subsection{RecursiveGetMin}
        
            \subsubsection{Terminating cases:}
            \begin{itemize}
                \item   If \texttt{ptrCurrent} is nullptr, throw a \texttt{runtime\_error}.
                \item   If \texttt{ptrCurrent} has no left children, then we're at the min node. Return \texttt{ptrCurrent}'s key.
            \end{itemize}
            
            \subsubsection{Recursive case:}
            \begin{itemize}
                \item   Recurse to the left.
\begin{lstlisting}[style=code]
return RecursiveGetMin(ptrCurrent->ptrLeft);
\end{lstlisting}
            \end{itemize}
        
        
        \newpage        
        \subsection{GetMaxKey}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMaxKey()
{
    return RecursiveGetMax( m_ptrRoot );
}
\end{lstlisting}
            
            The \texttt{GetMaxKey} function starts the recursive search
            for the maximum key, starting at \texttt{m\_ptrRoot}.

        \vspace{1cm}
        \subsection{RecursiveGetMax}
        
            \subsubsection{Terminating cases:}
            \begin{itemize}
                \item   If \texttt{ptrCurrent} is nullptr, throw a \texttt{runtime\_error}.
                \item   If \texttt{ptrCurrent} has no right children, then we're at the max node. Return \texttt{ptrCurrent}'s key.
            \end{itemize}
            
            \subsubsection{Recursive case:}
            \begin{itemize}
                \item   Recurse to the right.
\begin{lstlisting}[style=code]
return RecursiveGetMax(ptrCurrent->ptrRight);
\end{lstlisting}
            \end{itemize}
        
        
        \newpage
        \subsection{GetHeight}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::GetHeight()
{
    if ( m_ptrRoot == nullptr ) { return 0; }
    return RecursiveGetHeight( m_ptrRoot );
}
\end{lstlisting}
            
            The \texttt{GetHeight} function starts recursively analyzing
            the heights of each left-subtree vs. each right-subtree
            in order to find the maximum height of the tree.

        \vspace{1cm}
        \subsection{RecursiveGetHeight}
        
            Execute the commands in the following order:
            
            \begin{enumerate}
                \item   If \texttt{ptrCurrent} is nullptr, return 0.
                \item   Create two integer variables, \texttt{leftHeight} and \texttt{rightHeight}, and initialize them to 0.
                \item   If \texttt{ptrCurrent} has a left child, then recurse to the left. Store the result in \texttt{leftHeight}, and add +1.
\begin{lstlisting}[style=code]
leftHeight = RecursiveGetHeight(ptrCurrent->ptrLeft)+1;
\end{lstlisting}

                \item   If \texttt{ptrCurrent} has a right child, then recurse to the right. Store the result in \texttt{rightHeight}, and add +1.
\begin{lstlisting}[style=code]
rightHeight = RecursiveGetHeight(ptrCurrent->ptrRight)+1;
\end{lstlisting}

                \item   If \texttt{leftHeight} is greater than \texttt{rightHeight}, then return the \texttt{leftHeight}.
                \item   Otherwise, return the \texttt{rightHeight}.

            \end{enumerate}
    
        
        \newpage
        \subsection{GetInOrder}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
string BinarySearchTree<TK,TD>::GetInOrder()
{
    stringstream stream;
    RecursiveGetInOrder( m_ptrRoot, stream );
    return stream.str();
}
\end{lstlisting}
            
            The \texttt{GetInOrder} function starts recursively building
            a stream of nodes of the tree, ordered in-order.

        \vspace{1cm}
        \subsection{RecursiveGetInOrder}
        
            \subsubsection{Terminating case:}
            
            \begin{itemize}
                \item   If \texttt{ptrCurrent} is nullptr, just return.
            \end{itemize}
            
            \subsubsection{Recursive case:}
            
            The following lines will be executed as part of the recursive case:
            
            \begin{enumerate}
                \item   Recurse to the left child.
\begin{lstlisting}[style=code]
RecursiveGetInOrder(ptrCurrent->ptrLeft, stream);
\end{lstlisting}

                \item   Add the \texttt{ptrCurrent}'s key to the stream.
\begin{lstlisting}[style=code]
stream << ptrCurrent->key;
\end{lstlisting}

                \item   Recurse to the right child.
\begin{lstlisting}[style=code]
RecursiveGetInOrder(ptrCurrent->ptrRight, stream);
\end{lstlisting}
            \end{enumerate}
        
        \newpage
        \subsection{GetPreOrder}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
string BinarySearchTree<TK,TD>::GetPreOrder()
{
    stringstream stream;
    RecursiveGetPreOrder( m_ptrRoot, stream );
    return stream.str();
}
\end{lstlisting}
            
            The \texttt{GetPreOrder} function starts recursively building
            a stream of nodes of the tree, ordered pre-order.

        \vspace{1cm}
        \subsection{RecursiveGetPreOrder}
        
            \subsubsection{Terminating case:}
            
            \begin{itemize}
                \item   If \texttt{ptrCurrent} is nullptr, just return.
            \end{itemize}
            
            \subsubsection{Recursive case:}
            
            The following lines will be executed as part of the recursive case:
            
            \begin{enumerate}
                \item   Add the \texttt{ptrCurrent}'s key to the stream.
\begin{lstlisting}[style=code]
stream << ptrCurrent->key;
\end{lstlisting}

                \item   Recurse to the left child.
\begin{lstlisting}[style=code]
RecursiveGetPreOrder(ptrCurrent->ptrLeft, stream);
\end{lstlisting}

                \item   Recurse to the right child.
\begin{lstlisting}[style=code]
RecursiveGetPreOrder(ptrCurrent->ptrRight, stream);
\end{lstlisting}
            \end{enumerate}
        
        \newpage
        \subsection{GetPostOrder}
\begin{lstlisting}[style=code]
template <typename TK, typename TD>
string BinarySearchTree<TK,TD>::GetPostOrder()
{
    stringstream stream;
    RecursiveGetPostOrder( m_ptrRoot, stream );
    return stream.str();
}
\end{lstlisting}
            
            The \texttt{GetPostOrder} function starts recursively building
            a stream of nodes of the tree, ordered post-order.

        \vspace{1cm}
        \subsection{RecursiveGetPostOrder}
        
            \subsubsection{Terminating case:}
            
            \begin{itemize}
                \item   If \texttt{ptrCurrent} is nullptr, just return.
            \end{itemize}
            
            \subsubsection{Recursive case:}
            
            The following lines will be executed as part of the recursive case:
            
            \begin{enumerate}
                \item   Recurse to the left child.
\begin{lstlisting}[style=code]
RecursiveGetPostOrder(ptrCurrent->ptrLeft, stream);
\end{lstlisting}

                \item   Recurse to the right child.
\begin{lstlisting}[style=code]
RecursiveGetPostOrder(ptrCurrent->ptrRight, stream);
\end{lstlisting}

                \item   Add the \texttt{ptrCurrent}'s key to the stream.
\begin{lstlisting}[style=code]
stream << ptrCurrent->key;
\end{lstlisting}
            \end{enumerate}

