\contentsline {chapter}{\numberline {1}Boolean Algebra}{2}
\contentsline {section}{\numberline {1.1}Intro to Boolean Algebra}{2}
\contentsline {section}{\numberline {1.2}Boolean algebra properties}{3}
\contentsline {section}{\numberline {1.3}Operations in boolean algebra}{3}
\contentsline {section}{\numberline {1.4}Properties in logic, sets, and boolean algebra}{4}
\contentsline {chapter}{\numberline {2}Logic Circuits}{6}
\contentsline {section}{\numberline {2.1}Logic Gates}{6}
\contentsline {section}{\numberline {2.2}Building circuits}{6}
\contentsline {chapter}{\numberline {3}Karnaugh Maps}{7}
\contentsline {section}{\numberline {3.1}2-variable maps}{7}
\contentsline {section}{\numberline {3.2}3-variable maps}{8}
\contentsline {paragraph}{Header variables:}{8}
\contentsline {paragraph}{Region sizes:}{9}
\contentsline {paragraph}{Wrap-around:}{9}
\contentsline {paragraph}{Terms:}{9}
\contentsline {paragraph}{1x1 regions:}{10}
