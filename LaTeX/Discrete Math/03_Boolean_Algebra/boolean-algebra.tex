\setcounter{question}{0}

    %---%
    \section{Introduction to Boolean Algebra}

    Remember that when we were working with \textbf{propositional logic},
    we were concerned with \textbf{true} and \textbf{false} outcomes given
    some propositional variables ($p$, $q$, etc.) or predicates ($P(x)$, $Q(x)$, etc.).
    With \textbf{Boolean Algebra}, we are abstracting out logic into
    a way we can represent with algebraic notation.
    ~\\

    We continue using variables in boolean algebra, such as $p$ and $q$,
    and we represent ``true'' with the number 1,
    and ``false'' with the number 0. Our $\land$ (and) logical operator
    is represented as $\cdot$ or multiplication between the variables,
    and $\lor$ (or) becomes represented with $+$. And a negation $\neg$
    becomes represented as a prime-marker ($\neg p$ becomes $p'$).
    ~\\

    You may also notice a relationship between logic and boolean algebra as
    well as with sets. We are using similar patterns with each type of these problems,
    even though each type may be used for different types of problems.

    \begin{center}
        \begin{tabular}{l | c c c}
            \textbf{Symbol} & \textbf{Logic}    & \textbf{Boolean algebra}  & \textbf{Sets} \\ \hline
            Variables       & $p$, $q$, $r$     & $a$, $b$, $c$             & $A$, $B$, $C$ \\ \\
            AND operator    & $\land$           & $\cdot$                   & $\cap$ \\
            OR operator     & $\lor$            & $+$                       & $\cup$ \\
            NOT operator    & $\neg$            & $'$                       & $'$ \\ \\
            DIFFERENCE      & $a \land \neg b$  & $a \cdot b'$              & $A - B$
        \end{tabular}
    \end{center}

    \newpage
    ~\\
    In \textbf{logic}, we could have logical statements that are \textbf{contradictions} - where,
    no matter what the state of the variables, the outcome was always false -
    or statements that are \textbf{tautologies} - no matter what the state of the variables,
    the result is always true.

    ~\\
    With \textbf{sets}, we could have a set operation that results in $U$, \textbf{the universal set},
    or an operation that results in $\emptyset$, \textbf{an empty set}.

    ~\\
    And in \textbf{boolean algebra}, our statements could result in 1 or 0.

    %---%
    \subsection{Boolean algebra properties}

    As with logic and sets, we also have commutative properties, associative properties, and so on.
    
    ~\\
    Table of boolean algebra properties \footnote{Adapted from Discrete Mathematics, Ensley \& Crawley, pg 225}
    ~\\~\\
    \begin{tabular}{ l l p{4cm} p{5cm} }
           (a) & Commutative       & $a \cdot b = b \cdot a$                            & $a + b = b + a$            
        \\ (b) & Associative       & $(a \cdot b) \cdot c) = a \cdot (b \cdot c)$       & $(a + b) + c = a + (b + c)$ 
        \\ (c) & Distributive      & $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$      & $a + (b \cdot c) = (a + b) \cdot (a + c)$
        \\ (d) & Identity          & $a \cdot 1 = a$                                    & $a + 0 = a$
        \\ (e) & Negation          & $a + a' = 1$                                       & $a \cdot a' = 0$
        \\ (f) & Double negative   & $(a')' = a$
        \\ (g) & Idempotent        & $a \cdot a = a$                                    & $a + a = a$
        \\ (h) & DeMorgan's laws   & $(a \cdot b)' = a' + b'$                           & $(a + b)' = a' \cdot b'$
        \\ (i) & Universal bound   & $a + 1 = 1$                                        & $a \cdot 0 = 0$
        \\ (j) & Absorption        & $a \cdot (a + b) = a$                              & $a + (a \cdot b) = a$
        \\ (k) & Complements of    & $1' = 0$                                           & $0' = 1$
        \\     & 1 and 0

    \end{tabular}


    %---%
    \subsection{Operations in boolean algebra}

    Simplifying equations in boolean algebra isn't going to be 1-to-1 the same as
    normal algebra; for example, if you have $a + a$, the result isn't going to be $2a$ here.
    Thinking in terms of logic, $a + a$ would be ``$a$ or $a$'', and logically, that would simplify
    to just $a$. You can also refer to the \textbf{Idempotent Property}, which states
    that $a + a = a$ and $a \cdot a = a$.

    %---%
    \subsection{Properties in logic, sets, and boolean algebra}
    
    ~\\
    Tables of logic, set, and boolean algebra properties \footnote{Adapted from Discrete Mathematics, Ensley \& Crawley, pg 222, 225}
    ~\\~\\
    \begin{tabular}{ p{2cm} | p{5cm} | p{5cm} }
                                    & \textbf{Commutative Property} & \textbf{Associative Property}
        \\ \hline
        \textbf{Logic}              & $p \land q \equiv q \land p$  & $(p \land q) \land r \equiv p \land (q \land r)$
        \\                          & $p \lor q \equiv q \lor p$    & $(p \lor q) \lor r \equiv p \lor (q \lor r)$
        \\ \hline
        \textbf{Sets}               & $A \cap B = B \cap A$         & $(A \cap B) \cap C = A \cap (B \cap C)$
        \\                          & $A \cup B = B \cup A$         & $(A \cup B) \cup C = A \cup (B \cup C)$
        \\ \hline
        \textbf{Boolean}            & $a \cdot b = b \cdot a$       & $(a \cdot b) \cdot c = a \cdot (b \cdot c)$
        \\ \textbf{Algebra}         & $a + b = b + a$               & $(a + b) + c = a + (b + c)$
    \end{tabular}
    ~\\~\\~\\~\\
    \begin{tabular}{ p{2cm} | p{5cm} | p{5cm} }
                                    & \textbf{Distributive Property}                                & \textbf{Double negative}
        \\ \hline
        \textbf{Logic}              & $p \land (q \lor r) \equiv (p \land q) \lor (p \land r)$      & $\neg(\neg p) \equiv p$
        \\                          & $p \lor (q \land r) \equiv (p \lor q) \land (p \lor r)$
        \\ \hline
        \textbf{Sets}               & $A \cap (B \cup C)$                                           & $(A')' = A$
        \\                          & \tab $= (A \cap B) \cup (A \cap C)$
        \\                          & $A \cup (B \cap C)$
        \\                          & \tab $= (A \cup B) \cap (A \cup C)$
        \\ \hline
        \textbf{Boolean}            & $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$                 & $(a')' = a$
        \\ \textbf{Algebra}         & $a + (b \cdot c) = (a + b) \cdot (a + c)$
    \end{tabular}

    \newpage

    \begin{tabular}{ p{2cm} | p{3cm} | p{3cm} | p{3cm} }
                                    & \textbf{Identity}             & \textbf{Negation}         & \textbf{Idempotent}
        \\ \hline
        \textbf{Logic}              & $p \land t \equiv p$          & $p \lor \neg p \equiv t$  & $p \land p \equiv p$
        \\                          & $p \lor c \equiv p$           & $p \land \neg p \equiv c$ & $p \lor p \equiv p$
        \\ \hline
        \textbf{Sets}               & $A \cap U = A$                & $A \cup A' = U$           & $A \cap A = A$
        \\                          & $A \cup \emptyset = A$        & $A \cap A' = \emptyset$   & $A \cup A = A$
        \\ \hline
        \textbf{Boolean}            & $a \cdot 1 = a$               & $a + a' = 1$              & $a \cdot a = a$
        \\ \textbf{Algebra}         & $a + 0 = a$                   & $a \cdot a' = 0$          & $a + a = a$
    \end{tabular}
    ~\\~\\~\\~\\
    \begin{tabular}{ p{2cm} | p{4cm} | p{3cm} | p{3cm} }
                                    & \textbf{DeMorgan's Laws}                              & \textbf{Universal bound}          & \textbf{Absorption} 
    \\ \hline
    \textbf{Logic}                  & $\neg (p \land q) \equiv \neg p \lor \neg q$          & $p \lor t \equiv t$               & $p \land (p \lor q) \equiv p$
    \\                              & $\neg (p \lor q) \equiv \neg p \land \neg q$          & $p \land c \equiv c$              & $p \lor (p \land q) \equiv p$
    \\ \hline
    \textbf{Sets}                   & $(A \cap B)' = A' \cup B'$                            & $A \cup U = U$                    & $A \cap (A \cup B) = A$
    \\                              & $(A \cup B)' = A' \cap B'$                            & $A \cap \emptyset = \emptyset$    & $A \cup (A \cap B) = A$
    \\ \hline
    \textbf{Boolean}                & $(a \cdot b)' = a' + b'$                              & $a + 1 = 1$                       & $a \cdot (a + b) = a$
    \\ \textbf{Algebra}             & $(a + b)' = a' \cdot b'$                              & $a \cdot 0 = 0$                   & $a + (a \cdot b) = a$
    \end{tabular}
    ~\\~\\~\\
    \begin{tabular}{ p{2cm} | l }
                                    & \textbf{Negations of $t$ and $c$} \\
    \\ \hline
    \textbf{Logic}                  & $\neg t \equiv c$
    \\                              & $\neg c \equiv t$
    \\ \\
                                    & \textbf{Complements of $U$ and $\emptyset$}
    \\ \hline
    \textbf{Sets}                   & $U' = \emptyset$ 
    \\                              & $\emptyset' = U$
    
    \\ \\
                                    & \textbf{Complements of 1 and 0}
    \\ \hline
    \textbf{Boolean}                & $1' = 0$
    \\ \textbf{Algebra}             & $0' = 1$

    \end{tabular}
    
    %---%
    %---%
    %---%
    \section{Logic Circuits}
    
        \subsection{Logic Gates}
    
        Boolean algebra can be represented graphically by using logic gates,
        like what you would see in a computer architecture course. For
        this class, we are going to be focused on just three types of logic gates
        that we will use to represent our boolean algebra statements.
        
        \begin{center}
            \begin{tabular}{ c p{0.5cm} c p{0.5cm} c }
                AND gate & & OR gate & & NOT gate \\ \\
                
                \begin{circuitikz}
                    \node (a) at (0.2, 0.8) {$a$};
                    \node (b) at (0.2, 0.2) {$b$};
                    \node (c) at (2.8, 0.5) {$a \cdot b$};
                    \draw (2, 0.5) node[and port] (nand1) {};
                \end{circuitikz}
                &  &
                
                \begin{circuitikz}
                    \node (a) at (0.2, 0.8) {$a$};
                    \node (b) at (0.2, 0.2) {$b$};
                    \node (c) at (2.8, 0.5) {$a + b$};
                    \draw (2, 0.5) node[or port] (nand1) {};
                \end{circuitikz}
                &  &
                
                \begin{circuitikz}
                    \node (a) at (0.2, 0.5) {$a$};
                    \node (c) at (2.5, 0.5) {$a'$};
                    \draw (1.3, 0.5) node[not port] (nand1) {};
                \end{circuitikz}
            \end{tabular}
        \end{center}
        
        \subsection{Building circuits}
    
        By linking up logic gates, we can represent a boolean algebra expression.
        
        \begin{center}
            \begin{circuitikz}
                \node (a) at (0,3.3) {$a$};
                \node (b) at (0,2.7) {$b$};                
                \node (c) at (0,0.7) {$c$};
                
                \draw (0.5, 3.28) -- (3, 3.28); % a
                \draw (0.5, 2.72) -- (3, 2.72); % b
                \draw (1, 2.72) -- (1, 1.28) -- (1.5, 1.28); % b
                \draw (0.5, 0.72) -- (3, 0.72); % c
                
                \draw (4, 1) node[or port] (asdf) {};
                \draw (2, 1.28) node[not port] (asdf) {};
                \draw (4, 3) node[or port] (asdf) {};
                
                \draw (4.15,3) -- (4.65,2.3);
                \draw (4.15,1) -- (4.65,1.7);
                
                \draw (6, 2) node[and port] (asdf) {};
                \node at (8, 2) { $(a + b) \cdot (b' + c)$ };
            \end{circuitikz}
        \end{center}
        
        The order of operations flows from left to right in this case
        (diagrams can also go vertically), with the input variables
        on the left side, and the final resulting output on the right side.
        
    
    %---%
    %---%
    %---%
    \section{Karnaugh Maps}
    
        Karnaugh maps are another way to visualize - and more easily simplify -
        boolean expressions.
        
        \subsection{2-variable maps}
        
        To diagram a two-variable boolean expression, we build a 2x2 box,
        with each cell representing different types of terms:
    
        \begin{center}
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (1,0) -- (2,0);
                \draw (0,1) -- (1,1) -- (2,1);
                \draw (0,2) -- (1,2) -- (2,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$y$};
                \node at (1.5, 2.5) {$y'$};
                % Marks
                \node at (0.5, 0.5) {($xy$)};
                \node at (1.5, 0.5) {($xy'$)};
                \node at (1.5, 1.5) {($x'y'$)};
                \node at (0.5, 1.5) {($x'y$)};
            \end{tikzpicture}
        \end{center}
        
        To diagram a certain expression, we \checkmark whichever
        cells are present in the expression.
        
        \begin{center}
            $xy + xy'$
             ~\\~\\
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (1,0) -- (2,0);
                \draw (0,1) -- (1,1) -- (2,1);
                \draw (0,2) -- (1,2) -- (2,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$y$};
                \node at (1.5, 2.5) {$y'$};
                % Marks
                \node at (0.5, 0.5) { \checkmark };
                \node at (1.5, 0.5) { \checkmark };
                \node at (1.5, 1.5) {};
                \node at (0.5, 1.5) {};
            \end{tikzpicture}
        \end{center}
        
        \newpage
        We can then build sub-regions in our map. Region blocks
        can be 1x1, 1x2, 2x2, or 2x2 in the 2-variable map,
        and must contain \checkmark-ed cells that are neighbors.
        
        \begin{center}
            $xy + xy'$
             ~\\~\\
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (1,0) -- (2,0);
                \draw (0,1) -- (1,1) -- (2,1);
                \draw (0,2) -- (1,2) -- (2,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$y$};
                \node at (1.5, 2.5) {$y'$};
                % Marks
                \node at (0.5, 0.5) { \checkmark };
                \node at (1.5, 0.5) { \checkmark };
                \node at (1.5, 1.5) {};
                \node at (0.5, 1.5) {};
                % Inner boxes
                \draw[dashed,color=red] (0.2, 0.2) -- (1.8, 0.2) -- (1.8, 0.8) -- (0.2, 0.8) -- (0.2, 0.2);
            \end{tikzpicture}
        \end{center}
        
        A single region becomes a single term. With the region above, we look
        at the terms: $x$ is the same in both cells, but $y$ changes values ($y$, $y'$).
        This means that the value of $y$ doesn't really affect this term in the expression,
        so it can be simplified from $xy + xy'$ to just $x$.
        
        \subsection{3-variable maps}
        
        With a 3-variable Karnaugh map, we use a 4x2 grid. The rows
        will represent one variable, and the columns will represent
        combinations of two variables together.
        
        \begin{center}
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (4,0);
                \draw (0,1) -- (4,1);
                \draw (0,2) -- (4,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                \draw (3,0) -- (3,2);
                \draw (4,0) -- (4,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$yz$};
                \node at (1.5, 2.5) {$y'z$};
                \node at (2.5, 2.5) {$y'z'$};
                \node at (3.5, 2.5) {$yz'$};
                % Marks
                %\node at (0.5, 0.5) {\checkmark};
                %\node at (1.5, 0.5) {\checkmark};
                %\node at (1.5, 1.5) {\checkmark};
                %\node at (0.5, 1.5) {\checkmark};
                % Inner boxes
                %\draw[color=red] (0.2, 0.2) -- (1.8, 0.2) -- (1.8, 0.8) -- (0.2, 0.8) -- (0.2, 0.2);
            \end{tikzpicture}
        \end{center}
        
        \paragraph{Header variables:} Note that the column values ($yz$, $y'z$, $y'z'$, $yz'$) can be
        in any order, BUT only one variable can change state from its neighbor
        (so, $yz$ to $y'z$ or $yz$ to $yz'$ is OK, but not $yz$ to $y'z'$.)
        
        \newpage
        \paragraph{Region sizes:} With 3-variable maps our region sizes can be be heights/widths of 1, 2, and 4,
        but a region cannot be of length 3.
        
        \paragraph{Wrap-around:} Regions can also wrap-around the Karnaugh map to form a region across the divide.
        
        \begin{center}
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (4,0);
                \draw (0,1) -- (4,1);
                \draw (0,2) -- (4,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                \draw (3,0) -- (3,2);
                \draw (4,0) -- (4,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$yz$};
                \node at (1.5, 2.5) {$y'z$};
                \node at (2.5, 2.5) {$y'z'$};
                \node at (3.5, 2.5) {$yz'$};
                % Marks
                \node at (0.5, 0.5) {\checkmark};
                \node at (3.5, 0.5) {\checkmark};
                %\node at (1.5, 0.5) {\checkmark};
                %\node at (1.5, 1.5) {\checkmark};
                %\node at (0.5, 1.5) {\checkmark};
                % Inner boxes
                \draw[dashed,color=red] (-0.2, 0.2) -- (0.8, 0.2) -- (0.8, 0.8) -- (-0.2, 0.8);
                \draw[dashed,color=red] (4.2, 0.2) -- (3.2, 0.2) -- (3.2, 0.8) -- (4.2, 0.8);
            \end{tikzpicture} ~\\~\\
            
            Original expression: $xyz + xyz'$
            
            Simplified expression: $xy$
        \end{center}
        
        \paragraph{Terms:}  Each region built out from the Karnaugh map will
                            be one term of the expression (variables multiplied together)
                            and the different regions will be combined with $+$.
        
        \begin{center}
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (4,0);
                \draw (0,1) -- (4,1);
                \draw (0,2) -- (4,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                \draw (3,0) -- (3,2);
                \draw (4,0) -- (4,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$yz$};
                \node at (1.5, 2.5) {$y'z$};
                \node at (2.5, 2.5) {$y'z'$};
                \node at (3.5, 2.5) {$yz'$};
                % Marks
                \node at (0.5, 0.5) {\checkmark};
                \node at (1.5, 0.5) {\checkmark};
                \node at (1.5, 1.5) {\checkmark};
                \node at (0.5, 1.5) {\checkmark};
                \node at (2.5, 1.5) {\checkmark};
                \node at (3.5, 1.5) {\checkmark};
                % Inner boxes
                \draw[color=blue] (0.2, 0.2) -- (1.8, 0.2) -- (1.8, 1.8) -- (0.2, 1.8) -- (0.2, 0.2);
                \draw[dashed,color=red] (0.1, 1.1) -- (3.9, 1.1) -- (3.9, 1.9) -- (0.1, 1.9) -- (0.1, 1.1);
            \end{tikzpicture} ~\\~\\
            
            Original expression: $x'yz + x'y'z + x'y'z' + x'yz' + xyz + xy'z$
            
            Simplified expression: $z + x'$
        \end{center}
        
        The $z$ expression is the 2x2 box above; $z$ don't change in this region, but the $x$ and $y$ states change, so those get removed.
        
        The $x'$ expression is from the $x'$ row being completely highlighted; $x'$ doesn't change state, but $y$ and $z$ do, so those are left off.
        
        \newpage
        \paragraph{1x1 regions:} In some cases, a cell might be alone. In this case, whatever the state of all the variablesis, that will be this term.
        
        \begin{center}
            \begin{tikzpicture}
                % Box
                \draw (0,0) -- (4,0);
                \draw (0,1) -- (4,1);
                \draw (0,2) -- (4,2);
                \draw (0,0) -- (0,2);
                \draw (1,0) -- (1,2);
                \draw (2,0) -- (2,2);
                \draw (3,0) -- (3,2);
                \draw (4,0) -- (4,2);
                % Variable headers
                \node at (-0.5,0.5) {$x$};
                \node at (-0.5,1.5) {$x'$};
                \node at (0.5, 2.5) {$yz$};
                \node at (1.5, 2.5) {$y'z$};
                \node at (2.5, 2.5) {$y'z'$};
                \node at (3.5, 2.5) {$yz'$};
                % Marks
                \node at (0.5, 0.5) {\checkmark};
                
                \node at (2.5, 1.5) {\checkmark};
                \node at (2.5, 0.5) {\checkmark};
                % Inner boxes
                \draw[color=blue] (0.2, 0.2) -- (0.8, 0.2) -- (0.8, 0.8) -- (0.2, 0.8) -- (0.2, 0.2);
                \draw[dashed,color=red] (2.2, 0.2) -- (2.2, 1.8) -- (2.8, 1.8) -- (2.8, 0.2) -- (2.2, 0.2);
            \end{tikzpicture} ~\\~\\
            
            Original expression: $xyz + xy'z' + x'y'z'$
            
            Simplified expression: $xyz + y'z'$
        \end{center}
    
