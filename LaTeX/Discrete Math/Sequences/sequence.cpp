
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
using namespace std;

int Factorial( int n ) {
    int value = n;
    while ( n > 1 ) {
        n--;
        value *= n;
    }
    return value;
}

int Sequence( int n ) {
    return Factorial( n );
}

int RecursiveFormula( int n ) {
    if ( n == 1 ) 
        return 1;
    else 
        return n * RecursiveFormula( n-1 );
}




int Formula1( int n ) {
    return pow( 2, n );
}

int Formula2( int n ) {
    if ( n == 0 ) return 1;
    else return 2 * Formula2( n - 1 );
}




int Formula3( int n ) {
    return 4 * n - 3;
}

int Formula4( int n ) {
    if ( n == 1 ) return 1;
    else return 4 + Formula4( n - 1 );
}






int RecursiveTest( int n ) {
    if ( n == 1 ) { return 2; }
    else { return 2 * RecursiveTest( n - 1 ); }
}

int ClosedTest( int n ) {
    return pow(2, n);
}


int main()
{
    cout << setw( 10 ) << "n" << setw( 10 ) << "closed" << setw( 10 ) << "recursive" << endl;
    for ( int n = 1; n <= 5; n++ )
    {
        cout    << setw( 10 ) << n
                << setw( 10 ) << ClosedTest( n )
                << setw( 10 ) << RecursiveTest( n )
                << endl;
    }
    
    
    cout << endl << endl;
    
    
    
    
    //vector<int> values;
    //vector<int> differences;
    
    //cout << "RECURSIVE" << endl;
    //int previousValue = 0;
    //for ( int n = 1; n <= 7; n++ )
    //{        
        //int an = RecursiveFormula( n );
        
        //cout << setw( 10 ) << "n = " << n << setw( 10 ) << ", a[n] = " << an;
        
        //if ( n > 1 )     { cout << setw( 20 ) << " difference: " << ( an - previousValue ); }
        
        //cout << endl;
        
        //values.push_back( an );
        //previousValue = an;
    //}
    
    //cout << endl << endl;    
    //cout << "CLOSED" << endl;
    //previousValue = 0;
    //for ( int n = 1; n <= 7; n++ )
    //{
        //int an = Sequence( n );
        
        //cout << setw( 10 ) << "n = " << n << setw( 10 ) << ", a[n] = " << an;
        
        //if ( n > 1 )     { cout << setw( 20 ) << " difference: " << ( an - previousValue ); }
        
        //cout << endl;
        
        //values.push_back( an );
        //previousValue = an;
    //}
}
