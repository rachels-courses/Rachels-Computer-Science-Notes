#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
using namespace std;

// \sum_{i=1}^4 ( 3i + 2 )
int Sum2() {
    int sum = 0;
    for ( int i = 1; i <= 4; i++ )
    {
        if ( i != 1 ) { cout << " + "; }
        
        int value = (3 * i + 2);
        cout << value;
        
        sum += value;
    }
    
    cout << " = " << sum << endl;
    return sum;
}

//  \sum_{i=0}^3 ( i^2 - 1 ) 
int Sum3() {
    int sum = 0;
    for ( int i = 0; i <= 3; i++ )
    {
        if ( i != 0 ) { cout << " + "; }
        
        int value = (i * i - 1);
        cout << value;
        
        sum += value;
    }
    
    cout << " = " << sum << endl;
    return sum;
}

int main()
{
    Sum2();
    Sum3();
}
