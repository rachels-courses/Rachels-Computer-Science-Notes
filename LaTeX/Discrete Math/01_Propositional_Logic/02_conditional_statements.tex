
\begin{intro}{Conditional Statements (Implications)}
    A statement like ``if $p$ is true, then $q$ is true" is known
    as an implication or a conditional statement. 
    It can be written symbolically as $p \to q$,
    and read as ``p implies q" or ``if p, then q".
    For the implication $p \to q$, $p$ is the \textbf{hypothesis}
    and $q$ is the \textbf{conclusion}.
\end{intro}

    The \textbf{hypothesis} and the \textbf{conclusion} can be made of
    compound statements as well,
    such as $(p \lor q) \to (r \land s)$.


    \paragraph{Example:} Write the following statement symbolically,
    defining the propositional variable: 
    ``If it is Kate's birthday, then Kate will get a cake."

    ~\\ I will define two propositional variables:
    ~\\ \tab $b$ is ``It is Kate's birthday"
    ~\\ \tab $c$ is ``Kate will get a cake."

    ~\\
    And then I can write it symbolically: $b \to c$,
    which can be read aloud as ``if $b$ then $c$'', or ``$b$ implies $c$''.
    

\newpage
Some examples of conditional statements are:

\begin{itemize}
  \item   Given $t$ is the proposition ``the guest is over 48 inches tall'', ~\\
          and $r$ is the proposition ``the guest may ride the rollercoaster'',
          $$t \to r$$ 
          
          \begin{tabular}{l l l l}
            if & \cellcolor{hypothesis} \underline{the guest is over 48 inches tall} 
            \\ & \cellcolor{hypothesis} \footnotesize hypothesis 
            \\ then 
            & \cellcolor{conclusion}   \underline{the guest may ride the rollercoaster} 
            \\ & \cellcolor{conclusion}   \footnotesize conclusion
          \end{tabular}
          
  \item   Given $a$ is the proposition ``Bob is 21 or older'', ~\\
          and $b$ is the proposition ``Bob can drink beer'', ~\\
          and $s$ is the proposition ``Bob can drink soda'', then
          $$a \to (b \lor s)$$
          
          \begin{tabular}{l l l l}
            if & \cellcolor{hypothesis} \underline{Bob is 21 or older}
            \\ & \cellcolor{hypothesis} \footnotesize hypothesis 
            \\ then 
            & \cellcolor{conclusion}   \underline{Bob can drink beer} or \underline{Bob can drink soda}
            \\ & \cellcolor{conclusion}   \footnotesize conclusion
          \end{tabular}
          
  \item   Given $p$ is the proposition ``the printer has paper'', ~\\
          and $o$ is the proposition ``the printer is online'', ~\\
          and $d$ is the proposition ``you can print a document'', then
          $$ (p \land o) \to d $$
          
          \begin{tabular}{l l l l}
            if & \cellcolor{hypothesis} \underline{The printer has paper} and \underline{the printer is online}
            \\ & \cellcolor{hypothesis} \footnotesize hypothesis 
            \\ then 
            & \cellcolor{conclusion}   \underline{you can print a document}
            \\ & \cellcolor{conclusion}   \footnotesize conclusion
          \end{tabular}
\end{itemize}

%----------------------------------------------------------------------%
\newpage
\subsection{Truth tables for implications}

With an implication, ``if \textbf{hypothesis}, then \textbf{conclusion}",
    the truth table will look a bit different than what you've done so far.
    Make sure to take notice because it is easy to make an error when it
    comes to implication truth tables!

    For an implication to be logically FALSE, it must be the case that
    the \textbf{hypothesis is true, but the conclusion is false}. In
    all other cases, the implication results to \textbf{true}.

    \begin{center}
        \begin{tabular}{| c | c | | c |}
            \hline{}
            $p$     & $q$     & $p \to q$   \\ \hline
            \true   & \true   & \true       \\ \hline
            \true   & \false  & \false      \\ \hline
            \false  & \true   & \true       \\ \hline
            \false  & \false  & \true       \\ \hline
        \end{tabular}
    \end{center}

    Seems weird? Think of it this way: The only FALSE result is if
    the \textbf{hypothesis} is true, but the \textbf{conclusion}
    ends up being false. In science, it would mean our hypothesis
    has been disproven. If, however, the \textbf{hypothesis}
    is false, we can't really say anything about the conclusion
    (again thinking about science experiments)

    \paragraph{Running an experiment}
    A scientist is testing the statement,
    ``A watched pot never boils'', and they have formulated the following implication:
    ~\\
    $w \to \neg b$: 
    \textit{If \underline{you watch the pot} then \underline{the water won't boil}}.
    
    ~\\
    There are four possible outcomes that could happen:

    \begin{center}
      \begin{tabular}{c | l l l }
        \textbf{Scenario} & \textbf{Hypothesis}                               & \textbf{Conclusion}     &  \\ \hline
        1                 & \cellcolor{hypothesis} You watch the pot          & \cellcolor{conclusion} The water doesn't boil  & \includegraphics[height=1cm]{01_Propositional_Logic/images/pot1.png} \\ \hline
        2                 & \cellcolor{hypothesis} You watch the pot          & \cellcolor{conclusion} The water does boil     & \includegraphics[height=1cm]{01_Propositional_Logic/images/pot2.png} \\ \hline
        3                 & \cellcolor{hypothesis} You don't watch the pot    & \cellcolor{conclusion} The water doesn't boil  & \includegraphics[height=1cm]{01_Propositional_Logic/images/pot4.png} \\ \hline
        4                 & \cellcolor{hypothesis} You don't watch the pot    & \cellcolor{conclusion} The water does boil     & \includegraphics[height=1cm]{01_Propositional_Logic/images/pot3.png}    
      \end{tabular}
    \end{center}

\newpage
    \begin{itemize}
      \item   For scenario 1, the scientist watches the pot, and the water doesn't boil.
              They've tested their \textbf{hypothesis} and the conclusion came out to true.
              The experiment is valid and the result of $w \to \neg b$ is \truetext

      \item   For scenario 2, the scientist watches the pot, and the water DOES boil.
              They've tested their \textbf{hypothesis} and the conclusion came out to false.
              The experiment is valid and the result of $w \to \neg b$ is \falsetext

      \item   For scenarios 3 and 4, the scientist walked away from the pot - they
              didn't even perform the \textbf{hypothesis} correctly, so whether or not the
              water boiled, the experiment was not proven or disproven! Therefore,
              the result of the implication is \truetext , because it is not disproven.
    \end{itemize}
    
    The truth table for the experiment would look like this:

    \begin{center}
        \begin{tabular}{| c | c || c || c | p{7cm} |}
            \hline
                    &         & \footnotesize helper & \footnotesize implication & \footnotesize in English... \\
            $w$     & $b$     & $\neg b$  & $w \to \neg b$ &                                                            \\ \hline
            \true   & \true   & \false    & \false         & Watched the pot; water boiled 
            
            \footnotesize (experiment failed; disproven)               \\ \hline
            \true   & \false  & \true     & \true          & Watched the pot; water didn't boil 
            
            \footnotesize (experiment succeeded; proven)               \\ \hline
            \false  & \true   & \false    & \true          & Didn't watch the pot; water boiled 
            
            \footnotesize (invalid experiment; nothing disproven)               \\ \hline
            \false  & \false  & \true     & \true          & Didn't watch the pot; water didn't boil 
            
            \footnotesize (invalid experiment; nothing disproven)                \\ \hline
        \end{tabular}
    \end{center}
    
    
  
%----------------------------------------------------------------------%
\newpage
\subsection{Negations of Implications}

\begin{intro}{Negations of Implications}
  The negation of the implication $p \to q$ is the statement
  $p \land (\neg q)$.
\end{intro}

It is important to remember that \textbf{the negation of an
implication is not also an implication!} This might seem odd,
but we can see that the negation of an implication, $\neg (p \to q)$,
is logically equivalent to $p \land \neg q$.

  \begin{center}
      \begin{tabular}{| c | c || c || c || c |} \hline
                  &         & \footnotesize implication   & \footnotesize negation of implication & \\ 
          $p$     & $q$     & $p \to q$                   & $\neg( p \to q)$                      & $p \land \neg q$    \\ \hline
          \true   & \true   & \true                       & \false                                & \false              \\ \hline
          \true   & \false  & \false                      & \true                                 & \true               \\ \hline
          \false  & \true   & \true                       & \false                                & \false              \\ \hline
          \false  & \false  & \true                       & \false                                & \false              \\ \hline
      \end{tabular}
  \end{center}

To gain more context, let's look at some implications and negations:

\begin{center}
  \begin{tabular}{l l p{9cm}} 
    Implication & $p \to a$               &  If \hypothesis{they are a programmer} 
    
                                            then \conclusion{they like anime} \\
    Negation    & $p \land \neg a$        &  \textbf{They are a programmer} and(but) \textbf{they \underline{don't} like anime}.
    \\ \\
    Implication & $\neg c \to h$          &  If \hypothesis{there is no candy in the house}, 
    
                                            then \conclusion{I eat healthy} \\
    Negation    & $\neg c \land \neg h$   &  \textbf{There is no candy in the house} and \textbf{I \underline{don't} eat healthy}.
    \\ \\
    Implication & $b \to c$               &  If \hypothesis{you build it}, 
    
                                            then \conclusion{they will come} \\
    Negation    & $b \land \neg c$        &  \textbf{You build it}, and \textbf{they \underline{don't} come}.
  \end{tabular}
\end{center}


You can almost think of an implication as an assumption you might have,
and the negation as an exception to that assumption.
    
%----------------------------------------------------------------------%
\newpage
\subsection{Converse, Inverse, and Contrapositive}
Sometimes it can be helpful to reframe an implication in other ways.
If we look at an implication and its \textbf{contrapositive} they are
logically equivalent to each other. Similarly, the \textbf{converse}
of an implication is logically equivalent to the \textbf{inverse} of the
implication.

\begin{intro}{Contrapositive} \footnotesize
  Given the implication $p \to q$, the \textbf{Contrapositive} is $\neg q \to \neg p$.
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication     & contrapositive        \\ 
          $p$         & $q$         & $p \to q$       & $q \to p$             \\ \hline
          \true       & \true       & \true           & \true                 \\ \hline
          \true       & \false      & \false          & \false                \\ \hline
          \false      & \true       & \true           & \true                 \\ \hline
          \false      & \false      & \true           & \true                 \\ \hline
      \end{tabular}
  \end{center}
  
  Notice that the truth tables for $p \to q$ and $\neg q \to \neg p$ are logically equivalent!
\end{intro}

\begin{intro}{Converse} \footnotesize
  Given the implication $p \to q$, the \textbf{Converse} is $q \to p$.
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication & converse \\ 
          $p$         & $q$         & $p \to q$   & $q \to p$     \\ \hline
          \true       & \true       & \true       & \true         \\ \hline
          \true       & \false      & \false      & \true         \\ \hline
          \false      & \true       & \true       & \false        \\ \hline
          \false      & \false      & \true       & \true         \\ \hline
      \end{tabular}
  \end{center}
\end{intro}


\begin{intro}{Inverse} \footnotesize
  Given the implication $p \to q$, the \textbf{Inverse} is $\neg p \to \neg q$.
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication     & inverse               \\ 
          $p$         & $q$         & $p \to q$       & $\neg p \to \neg q$   \\ \hline
          \true       & \true       & \true           & \true                 \\ \hline
          \true       & \false      & \false          & \true                 \\ \hline
          \false      & \true       & \true           & \false                \\ \hline
          \false      & \false      & \true           & \true                 \\ \hline
      \end{tabular}
  \end{center}
\end{intro}

\newpage
Let's look at some implications and how they change as a converse, inverse, and contrapositive...

\begin{center}
  \begin{tabular}{l l p{9cm}} 
    \\ \\ Implication    & $p \to a$               & If \hypothesis{they are a programmer} 
    
                                                  then \conclusion{they like anime}
                                                  
    \\ \\ Contrapositive & $\neg a \to \neg p$     & If \hypothesis{they don't like anime}
    
                                                  then \conclusion{they aren't a programmer}
                                                  
    \\ \\ Converse       & $a \to p$               & If \hypothesis{they like anime}
    
                                                  then \conclusion{they are a programmer}
                                                  
    \\ \\ Inverse        & $\neg p \to \neg a$     & If \hypothesis{they aren't a programmer}
    
                                                  then \conclusion{they don't like anime}
    \\ \\ \\

    \\ Implication        & $r \to \neg w$          & If \hypothesis{you are a rich man} 
    
                                                    then \conclusion{you don't have to work hard}
                                                    
    \\ Contrapositive     & $w \to \neg r$          & If \hypothesis{you work hard} 
        
                                                    then \conclusion{you aren't a rich man}
                                                    
    \\ Converse           & $\neg w \to r$          & If \hypothesis{you don't have to work hard} 
        
                                                    then \conclusion{you are a rich man}
                                                    
    \\ Inverse            & $\neg r \to w$          & If \hypothesis{you aren't a rich man} 
        
                                                    then \conclusion{you have to work hard}
  \end{tabular}
\end{center}

