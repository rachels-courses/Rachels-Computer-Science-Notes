
If you've done any programming so far, you might be familiar with 
working with if statements and while loops. These both work with 
\textbf{propositional logic } to decide whether to branch the program, 
or continue looping a set of instructions. 

Learning about propositional logic can really help with writing better 
programming logic, and also help you debugging your logic. 


\subsection{Propositions}

\begin{intro}{Propositions}
  A proposition is a statement that is true or false. Often, we think 
  of these as ``yes/no'' questions.
\end{intro}
  
  ~\\ Some examples of propositional statements are:
  
  \begin{itemize}
    \item   Fran has a cat.
    \item   The cat is black.
    \item   $20 > 10$
    \item   $1/2$ and $0.5$ are equivalent.
  \end{itemize}
  
  ~\\ These are statements that can result in true (Fran DOES have a 
  cat!) or false (Actually, the cat is orange). We could use these 
  statements in a program, with an if statement... 

\begin{lstlisting}[style=code]
string catColor;
cout << "What color is the cat? ";
cin >> catColor;

if ( catColor == "black" )
{
  // proposition was true...
}
else
{
  // proposition was false...
}
\end{lstlisting}

We can essentially think of it as a yes/no question of ``Is the cat 
black?'', where we could have different sets of operations to 
perform if the statement is true (the cat is black) or if 
the statement is false (the cat is not black). 

On the flip side, we could not write a program with questions like 
``What is your favorite color?'' -- That is not a yes/no question, 
and the computer wouldn't understand what that type of question is. 
Instead, if we really wanted to define behavior based on favorite color, 
we would need a series of if / else if statements asking about 
``is favorite color red?'', ``is favorite color blue?'', and so on... 

\begin{lstlisting}[style=code]
string favColor;
cout << "What is your favorite color? ";
cin >> favColor;

if ( favColor == "red" )
{
  // Code for red as favorite color...
}
else if ( favColor == "orange" )
{
  // Code for orange as favorite color...
}
else if ( favColor == "yellow" )
{
  // Code for yellow as favorite color...
}
// ... and so on ...
\end{lstlisting}

\subsection{Compound propositions}

We can also create a compound proposition using the 
\textbf{logical operators} AND, OR, and NOT. 

  \subsubsection{AND $\land$}
  
  In C++, Java, and C\#, the \texttt{ \&\& } symbol is used for a 
  logical ``AND''. In discrete math, we often use $\land$.{}
  
  ~\\
  If we are writing a compound proposition out of two sub-statements
  (such as $p$ and $q$), giving us the proposition $p \land q$
  ($p$ AND $q$), then:
  
  \begin{itemize}
    \item   The result is only \truetext when both sub-statements are also \truetext
    \item   The result is \falsetext if one or both of the sub-statements are \falsetext
  \end{itemize}
  
  \paragraph{Fran has a cat AND Fran has a dog.}
  
  The entire compound proposition is true only if both sub-statements 
  are true. This proposition asserts that Fran has both a cat and a dog 
  at the same time, not just one or the other. 
  
  ~\\
  \begin{tabular}{ | p{4cm} | p{2cm} | p{2cm} || p{4cm} | }             \hline
    \tiny Scenario state                              &  \tiny Statement 1 & \tiny Statement 2   & \tiny Result                         \\ 
                                                      &  Fran has a cat    & Fran has a dog      & Fran has a cat OR Fran has a dog     \\ \hline
    \footnotesize Fran has a cat and Fran has a dog.  &  \true             & \true               & \true                                \\ \hline
    \footnotesize Fran has a cat but not a dog.       &  \true             & \false              & \false                               \\ \hline
    \footnotesize Fran has a dog but not a cat.       &  \false            & \true               & \false                               \\ \hline
    \footnotesize Fran has neither a cat nor a dog.   &  \false            & \false              & \false                               \\ \hline
  \end{tabular} 
  
  ~\\ \footnotesize Looking at the result column, the statement ``Fran has a cat AND Fran has a dog'' is \truetext when:
  
  \begin{itemize}
    \item   Fran has both a cat and a dog.
  \end{itemize}
  ~\\ And the statement ``Fran has a cat OR Fran has a dog'' is \falsetext when:
  
  \begin{itemize}
    \item   Fran has a cat but not a dog.
    \item   Fran has a dog but not a cat.
    \item   Fran doesn't have a dog NOR a cat!
  \end{itemize}
  \normalsize
  
  \newpage  
  \subsubsection{OR $\lor$}
  In C++, Java, and C\#, the \texttt{||} symbol is used for a logical 
  ``OR''. In discrete math, we often use $\lor$. 
  
  ~\\
  If we are writing a compound proposition out of two sub-statements
  (such as $p$ and $q$), giving us the proposition $p \lor q$
  ($p$ OR $q$), then:
  
  \begin{itemize}
    \item   The result is \truetext if at least one of the sub-statements are \truetext
    \item   The result is only \falsetext if all of the sub-statements are \falsetext
  \end{itemize}
  
  \paragraph{Fran has a cat OR Fran has a dog.}
  The entire compound proposition is true if at least one sub-statement 
  is true. This statement asserts that Fran has a cat or a dog. 
  She could have one or the other, or she could have both, and all 
  these scenarios result in a true outcome. 
  
  ~\\
  \begin{tabular}{ | p{4cm} | p{2cm} | p{2cm} || p{4cm} | }             \hline
    \tiny Scenario state                              &  \tiny Statement 1 & \tiny Statement 2   & \tiny Result                         \\ 
                                                      &  Fran has a cat    & Fran has a dog      & Fran has a cat OR Fran has a dog     \\ \hline
    \footnotesize Fran has a cat and Fran has a dog.  &  \true             & \true               & \true                                \\ \hline
    \footnotesize Fran has a cat but not a dog.       &  \true             & \false              & \true                                \\ \hline
    \footnotesize Fran has a dog but not a cat.       &  \false            & \true               & \true                                \\ \hline
    \footnotesize Fran has neither a cat nor a dog.   &  \false            & \false              & \false                               \\ \hline
  \end{tabular} 
  
  \vspace{1cm}
  
  ~\\ \footnotesize Looking at the result column, the statement ``Fran has a cat OR Fran has a dog'' is \truetext when:
  
  \begin{itemize}
    \item   Fran has both a cat and a dog.
    \item   Fran has a cat but not a dog.
    \item   Fran has a dog but not a cat.
  \end{itemize}
  
  ~\\ And the statement ``Fran has a cat OR Fran has a dog'' is only \falsetext when:
  
  \begin{itemize}
    \item   Fran doesn't have a dog NOR a cat!
  \end{itemize}
  \normalsize
  
  \newpage
  \subsubsection{NOT $\neg$}
  In C++, Java, and C\#, the \texttt{!} symbol is used for a logical ``NOT''. 
  In discrete math, we often use $\neg$.
  
  NOT operates on one statement. Let's say we have the proposition $p$. 
  
  \begin{itemize}
    \item   If $p$ is \truetext, then the result of $¬p$ is \falsetext
    \item   If $p$ is \falsetext, then the result of $¬p$ is \truetext
  \end{itemize}
  
  \paragraph{It is NOT true that... The program is done.}
  
  Usually NOT is used as a ``a is not equal to b'' scenario... 
  
\begin{lstlisting}[style=code]
if ( a != b ) // ...
\end{lstlisting}

  but it can also be used to negate logical statements, such as if we 
  wanted to keep running the program while ``done'' is not true...
  
\begin{lstlisting}[style=code]
bool done = false;
while ( !done )   // While the program is NOT done...
{
  // ... Program code is here ...
}
\end{lstlisting}
  
  ~\\
  \begin{tabular}{ | p{4cm} | p{4cm} || p{4cm} | }             \hline
    \tiny Scenario state                              & \tiny Statement         & \tiny Result                                \\ 
                                                      & The program is done     & It is not true that... The program is done  \\ \hline
    \footnotesize The program is done.                & \true                   & \false                                       \\ \hline
    \footnotesize The program is not done.            & \false                  & \true                                       \\ \hline
  \end{tabular} 
  

  
  ~\\
  \begin{center}
    \includegraphics[height=4cm]{01_Propositional_Logic/images/youandme.png}
  \end{center}
  
  
  \newpage{}
  
  \begin{center}
    \begin{tabular}{p{5cm} p{1cm} p{5cm}}
      Jim: Hey Janet, we can't print documents; the printer is offline AND the printer is out of paper.
      & &
      Janet: Alright Jim. Thanks for the heads up.
      \\
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-jim.png}
      & &
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-janet.png}

      \\ \multicolumn{3}{c}{... Later ...} \\ \\
      
      Rahaf: Hey Janet, looks like the printer is offline, but it has paper.
      & &
      Janet: Has paper?? Jim lied to me!!
      \\
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-rahaf.png}
      & &
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-janet2.png}
      
      \\ \multicolumn{3}{c}{... Later ...} \\ \\
      
      Rose: Hey Janet, the printer is online but it's out of paper.
      & &
      Janet: Well, which is it?!?!
      \\
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-rose.png}
      & &
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-janet2.png}
      
      \\ \multicolumn{3}{c}{... Later ...} \\ \\
      
      Anuraj: I just printed out some docs... of course the printer is online and it has paper!
      & &
      Janet: That's the complete opposite!!
      \\
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-anuraj.png}
      & &
      \includegraphics[width=2cm]{01_Propositional_Logic/images/av-janet4.png}
    \end{tabular} 
  \end{center}
  
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\subsection{Propositional variables}

When working with propositional logic, we commonly shorten a 
propositional statement to a variable. For example, we can represent 
``she is in Discrete Math'' with $d$, and 
``she is majoring in Computer Science'' with $c$. 
We could then write out statements like: 

\begin{itemize}
  \item   $d \land c$           : She is in Discrete Math AND she is majoring in Computer Science.
  \item   $d \land \neg c$      : She is in Discrete Math AND she is NOT majoring in Computer Science.
  \item   $\neg d \land c$      : She is NOT in Discrete Math AND she is majoring in Computer Science.
  \item   $\neg d \land \neg c$ : She is NOT in Discrete Math AND she is NOT majoring in Computer Science.
\end{itemize}

Propositional variables should be one letter in length. (though when programming, you should use longer variable names :)

\vspace{5cm}

\begin{center}
  \includegraphics[height=5cm]{01_Propositional_Logic/images/coolrabbit.png}
\end{center}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %

\newpage
\subsection{Truth Tables}

  When we're working with compound propositional statements,
  the result of the compound depends on the true/false values
  of each proposition it is built up of.
  ~\\
  We can diagram out all possible states of a compound proposition
  by using a \textbf{truth table}. In a truth table, we list
  all propositional variables first on the left, as well as
  all possible combinations of their states, and then
  the compound statement's result on the right.
  
  \begin{intro}{Truth Tables}
      \begin{tabular}{ p{5cm} p{5cm} }
        \textbf{Truth table for AND:} & 
        \begin{tabular}{ | c | c || c | }          \hline
            $p$       & $q$       & $p \land q$     \\ \hline
            \true     & \true     & \true           \\ \hline
            \true     & \false    & \false          \\ \hline
            \false    & \true     & \false          \\ \hline
            \false    & \false    & \false          \\ \hline
        \end{tabular}        
        \\ \\
        \textbf{Truth table for OR:} &
        \begin{tabular}{ | c | c || c | }      \hline
            $p$     & $q$       & $p \lor q$    \\ \hline
            \true   & \true     & \true         \\ \hline
            \true   & \false    & \true         \\ \hline
            \false  & \true     & \true         \\ \hline
            \false  & \false    & \false        \\ \hline
        \end{tabular}        
        \\ \\
        \textbf{Truth table for NOT:} &
        \begin{tabular}{ | c | | c | }  \hline
            $p$     & $\neg p$          \\ \hline
            \true   & \false            \\ \hline
            \false  & \true             \\ \hline
        \end{tabular}
      \end{tabular}
  \end{intro}
  
  \vspace{2cm}
  
  \paragraph{Example: Validating a statement with a truth table}
  
  Lets say we had the statement 
  ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both.''
  and a student comes up with the solution: $m \lor c$.   
  Let's validate it with a truth table. ~\\
  
  First, we want the outcome to be \textbf{true} for these scenarios:
  
  \begin{itemize}
    \item   Milk is in the fridge, but not cheese ($m$ is true, $c$ is false), OR
    \item   Milk is not in the fridge, but cheese is ($m$ is false, $c$ is true).
  \end{itemize}
  
  And these two scenarios should result in \textbf{false}:
  
  \begin{itemize}
    \item   Milk is in the fridge and cheese is in the fridge ($m$ is true, $c$ is true).
    \item   Milk is not in the fridge and cheese is not in the fridge ($m$ is false, $c$ is false).
  \end{itemize}
  
  If these scenarios are true, the resulting truth table should look like this: ~\\

  \begin{tabular}{ | c | c || c | l | }             \hline
      $m$     & $c$       & LOGICALLY CORRECT & Translation                                     \\ \hline
      \true   & \true     & \false            & there is milk and there is cheese               \\ \hline
      \true   & \false    & \true             & there is milk but no cheese                     \\ \hline
      \false  & \true     & \true             & there is no milk but there is cheese            \\ \hline
      \false  & \false    & \false            & there is no milk and no cheese                  \\ \hline
  \end{tabular} 
  
  ~\\
  
  However, once we fill out the truth table for $m \lor c$, we get this: ~\\
  
  \begin{tabular}{ | c | c || c | l | }             \hline
      $m$     & $c$       & $m \lor c$ & Translation                                            \\ \hline
      \true   & \true     & \true      & there is milk and there is cheese                      \\ \hline
      \true   & \false    & \true      & there is milk but no cheese                            \\ \hline
      \false  & \true     & \true      & there is no milk but there is cheese                   \\ \hline
      \false  & \false    & \false     & there is no milk and no cheese                         \\ \hline
  \end{tabular} 
  ~\\ ~\\
  
  So the student's solution is incorrect, because while two scenarios are correct (milk and no cheese / cheese and no milk),
  the scenario where milk and cheese are both in the fridge comes out to true.
  Therefore, $m \lor c$ \textit{ \textbf{ does not } } accurately reflect the statement
  ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both''.
  ~\\
  
  Trying again, perhaps the student comes up with $(m \lor c) \land \neg (m \land c)$.
  To test it out, we put it in the truth table again...

  \begin{center}
    \begin{tabular}{ | c | c || c | l | }             \hline
                &           & Student answer                               & Translation                                            \\
        $m$     & $c$       & $(m \lor c) \land \neg (m \land c)$          &                                      \\ \hline
        \true   & \true     & \false                                       & there is milk and there is cheese               \\ \hline
        \true   & \false    & \true                                        & there is milk but no cheese                     \\ \hline
        \false  & \true     & \true                                        & there is no milk but there is cheese            \\ \hline
        \false  & \false    & \false                                       & there is no milk and no cheese                  \\ \hline
    \end{tabular}
  \end{center}
  
  This one is logically correct!



