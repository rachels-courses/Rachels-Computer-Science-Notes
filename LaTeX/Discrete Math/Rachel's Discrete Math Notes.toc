\contentsline {chapter}{\numberline {1}Propositional Logic}{4}%
\contentsline {section}{\numberline {1.1}Introduction to Propositional Logic}{4}%
\contentsline {subsection}{\numberline {1.1.1}Propositions}{4}%
\contentsline {subsection}{\numberline {1.1.2}Compound propositions}{6}%
\contentsline {subsubsection}{AND $\land $}{6}%
\contentsline {paragraph}{Fran has a cat AND Fran has a dog.}{6}%
\contentsline {subsubsection}{OR $\lor $}{7}%
\contentsline {paragraph}{Fran has a cat OR Fran has a dog.}{7}%
\contentsline {subsubsection}{NOT $\neg $}{8}%
\contentsline {paragraph}{It is NOT true that... The program is done.}{8}%
\contentsline {subsection}{\numberline {1.1.3}Propositional variables}{10}%
\contentsline {subsection}{\numberline {1.1.4}Truth Tables}{11}%
\contentsline {paragraph}{Example: Validating a statement with a truth table}{11}%
\contentsline {section}{\numberline {1.2}Conditional Statements (Implications)}{13}%
\contentsline {paragraph}{Example:}{13}%
\contentsline {subsection}{\numberline {1.2.1}Truth tables for implications}{15}%
\contentsline {paragraph}{Running an experiment}{15}%
\contentsline {subsection}{\numberline {1.2.2}Negations of Implications}{17}%
\contentsline {subsection}{\numberline {1.2.3}Converse, Inverse, and Contrapositive}{18}%
\contentsline {section}{\numberline {1.3}Predicates and Quantifiers}{20}%
\contentsline {subsection}{\numberline {1.3.1}Domains}{23}%
\contentsline {subsection}{\numberline {1.3.2}Universal Quantifier $\forall $}{24}%
\contentsline {paragraph}{Example 1:}{24}%
\contentsline {paragraph}{Example 2:}{24}%
\contentsline {subsection}{\numberline {1.3.3}Existential Quantifier $\exists $}{25}%
\contentsline {paragraph}{Example 1:}{25}%
\contentsline {paragraph}{Example 2:}{26}%
\contentsline {subsection}{\numberline {1.3.4}Quantified statement}{26}%
\contentsline {subsection}{\numberline {1.3.5}Counterexamples}{27}%
\contentsline {paragraph}{Example:}{27}%
\contentsline {subsection}{\numberline {1.3.6}Negations of predicates}{27}%
\contentsline {paragraph}{Example:}{27}%
\contentsline {paragraph}{Example 1:}{28}%
\contentsline {paragraph}{Example 2:}{29}%
\contentsline {subsection}{\numberline {1.3.7}Quantified statements with multiple variables}{30}%
\contentsline {subsubsection}{Negating quantified statements with multiple variables}{31}%
\contentsline {paragraph}{Example:}{31}%
\contentsline {section}{\numberline {1.4}Laws for Propositional Logic}{32}%
\contentsline {subsection}{\numberline {1.4.1}Logical Equivalences}{32}%
\contentsline {subsubsection}{Law of Commutation}{33}%
\contentsline {paragraph}{Example:}{33}%
\contentsline {subsubsection}{Law of Association}{33}%
\contentsline {paragraph}{Example:}{33}%
\contentsline {subsubsection}{Law of Distribution}{34}%
\contentsline {paragraph}{Example:}{34}%
\contentsline {subsubsection}{DeMorgan's Law}{35}%
\contentsline {paragraph}{Example 1:}{35}%
\contentsline {paragraph}{Example 2:}{35}%
\contentsline {subsubsection}{Tautologies and Contradictions}{36}%
\contentsline {paragraph}{Example tautology:}{37}%
\contentsline {paragraph}{Example contradiction:}{38}%
\contentsline {section}{\numberline {1.5}Logical Reasoning}{39}%
\contentsline {subsection}{\numberline {1.5.1}Conjunction}{39}%
\contentsline {subsection}{\numberline {1.5.2}Addition}{40}%
\contentsline {subsection}{\numberline {1.5.3}Simplification}{40}%
\contentsline {subsection}{\numberline {1.5.4}Disjunctive syllogism}{40}%
\contentsline {subsection}{\numberline {1.5.5}Hypothetical syllogism}{41}%
\contentsline {subsection}{\numberline {1.5.6}Modus Ponens (``mode that affirms by affirming'')}{41}%
\contentsline {subsection}{\numberline {1.5.7}Modus Tollens (``mode that denies by denying'')}{41}%
\contentsline {chapter}{\numberline {2}Set Theory}{42}%
\contentsline {section}{\numberline {2.1}Introduction to Sets}{42}%
\contentsline {subsection}{\numberline {2.1.1}Basic sets}{42}%
\contentsline {subsection}{\numberline {2.1.2}Element in a set $\in $, not in a set $\not \in $}{42}%
\contentsline {subsection}{\numberline {2.1.3}Set cardinality}{43}%
\contentsline {subsection}{\numberline {2.1.4}Common sets $\mathbb {Z}$, $\mathbb {N}$, $\mathbb {Q}$ and $\mathbb {R}$}{43}%
\contentsline {subsection}{\numberline {2.1.5}Subsets}{44}%
\contentsline {subsection}{\numberline {2.1.6}Universal set}{44}%
\contentsline {subsubsection}{Sets of sets}{44}%
\contentsline {subsection}{\numberline {2.1.7}Set-builder notation}{45}%
\contentsline {subsection}{\numberline {2.1.8}Property-description style}{45}%
\contentsline {subsection}{\numberline {2.1.9}Form-description style}{45}%
\contentsline {section}{\numberline {2.2}Unions, intersections, and differences}{45}%
\contentsline {subsection}{\numberline {2.2.1}Operation basics}{45}%
\contentsline {section}{\numberline {2.3}Complement of a set}{46}%
\contentsline {subsection}{\numberline {2.3.1}Basic properties of sets}{46}%
\contentsline {section}{\numberline {2.4}Venn diagrams}{47}%
\contentsline {section}{\numberline {2.5}Partitions}{47}%
\contentsline {section}{\numberline {2.6}All possible partitions of a set}{49}%
\contentsline {section}{\numberline {2.7}Cartesian products}{49}%
\contentsline {section}{\numberline {2.8}Power Sets}{50}%
\contentsline {section}{\numberline {2.9}Set properties}{51}%
\contentsline {subsection}{\numberline {2.9.1}The duality principle}{51}%
\contentsline {section}{\numberline {2.10}Common pitfalls with set operations}{51}%
\contentsline {chapter}{\numberline {3}Boolean Algebra}{53}%
\contentsline {section}{\numberline {3.1}Introduction to Boolean Algebra}{53}%
\contentsline {subsection}{\numberline {3.1.1}Boolean algebra properties}{54}%
\contentsline {subsection}{\numberline {3.1.2}Operations in boolean algebra}{54}%
\contentsline {subsection}{\numberline {3.1.3}Properties in logic, sets, and boolean algebra}{55}%
\contentsline {section}{\numberline {3.2}Logic Circuits}{56}%
\contentsline {subsection}{\numberline {3.2.1}Logic Gates}{56}%
\contentsline {subsection}{\numberline {3.2.2}Building circuits}{57}%
\contentsline {section}{\numberline {3.3}Karnaugh Maps}{57}%
\contentsline {subsection}{\numberline {3.3.1}2-variable maps}{57}%
\contentsline {subsection}{\numberline {3.3.2}3-variable maps}{59}%
\contentsline {paragraph}{Header variables:}{59}%
\contentsline {paragraph}{Region sizes:}{60}%
\contentsline {paragraph}{Wrap-around:}{60}%
\contentsline {paragraph}{Terms:}{60}%
\contentsline {paragraph}{1x1 regions:}{61}%
\contentsline {chapter}{\numberline {4}Functions and Relations}{62}%
\contentsline {section}{\numberline {4.1}Introduction to Functions}{62}%
\contentsline {subsection}{\numberline {4.1.1}Function Diagrams}{63}%
\contentsline {subsection}{\numberline {4.1.2}Properties of Functions}{64}%
\contentsline {subsubsection}{Function}{64}%
\contentsline {subsubsection}{Onto}{64}%
\contentsline {subsubsection}{One-to-one}{65}%
\contentsline {subsubsection}{Invertible}{65}%
\contentsline {subsubsection}{Function inverse}{65}%
\contentsline {subsection}{\numberline {4.1.3}Illustrating onto, one-to-one, and invertibility}{66}%
\contentsline {paragraph}{Functions that are not onto:}{66}%
\contentsline {paragraph}{Functions that are not one-to-one:}{66}%
\contentsline {paragraph}{Functions that are onto and one-to-one:}{66}%
\contentsline {section}{\numberline {4.2}Introduction to Relations}{67}%
\contentsline {subsection}{\numberline {4.2.1}Binary Relations}{67}%
\contentsline {subsubsection}{Binary relations on $A \to A$}{67}%
\contentsline {subsection}{\numberline {4.2.2}Order Relations}{68}%
\contentsline {subsubsection}{Relations on on power sets}{69}%
\contentsline {subsection}{\numberline {4.2.3}Equivalence Relations}{70}%
\contentsline {subsection}{\numberline {4.2.4}Properties of Relations}{72}%
\contentsline {subsubsection}{Reflexive, irreflexive, or neither}{72}%
\contentsline {subsubsection}{Symmetric, antisymmetric, or neither}{73}%
\contentsline {subsubsection}{Transitive or intransitive}{74}%
\contentsline {section}{\numberline {4.3}The Composition Operation}{75}%
\contentsline {chapter}{\numberline {5}Proofs}{76}%
\contentsline {section}{\numberline {5.1}Properties of Numbers}{76}%
\contentsline {subsection}{\numberline {5.1.1}The Closure Property of Integers}{76}%
\contentsline {subsection}{\numberline {5.1.2}Definition of Even and Odd Integers}{77}%
\contentsline {subsection}{\numberline {5.1.3}Division and Modulus}{77}%
\contentsline {paragraph}{Definition of divisibility:}{78}%
\contentsline {paragraph}{Example:}{78}%
\contentsline {paragraph}{Definition of a rational number:}{78}%
\contentsline {paragraph}{The Division Theorem}{79}%
\contentsline {paragraph}{Example:}{79}%
\contentsline {paragraph}{Example:}{80}%
\contentsline {subsubsection}{Calculating Quotient and Remainder separately}{81}%
\contentsline {subsection}{\numberline {5.1.4}Primes and Composites}{82}%
\contentsline {section}{\numberline {5.2}Direct Proofs}{82}%
\contentsline {subsection}{\numberline {5.2.1}Implications and counter-examples}{82}%
\contentsline {paragraph}{Example:}{83}%
\contentsline {paragraph}{Example:}{83}%
\contentsline {subsection}{\numberline {5.2.2}Direct proofs}{83}%
\contentsline {paragraph}{Example:}{84}%
\contentsline {paragraph}{Example:}{85}%
\contentsline {paragraph}{Example:}{86}%
\contentsline {section}{\numberline {5.3}Proof by Contradiction}{86}%
\contentsline {paragraph}{Example:}{89}%
\contentsline {section}{\numberline {5.4}Proof by Contradiction}{89}%
\contentsline {paragraph}{Example:}{91}%
\contentsline {section}{\numberline {5.5}Inductive Proofs}{92}%
\contentsline {subsection}{\numberline {5.5.1}Equivalence of a Closed Formula and a Recursive Formula}{92}%
\contentsline {paragraph}{Example:}{92}%
\contentsline {paragraph}{Example:}{93}%
\contentsline {subsection}{\numberline {5.5.2}Representing the result of a Summation as a Closed Formula}{94}%
\contentsline {paragraph}{Example:}{94}%
\contentsline {paragraph}{Example:}{96}%
\contentsline {subsection}{\numberline {5.5.3}Representing the result of a Summation as a Recursive Formula}{97}%
\contentsline {paragraph}{Example:}{97}%
\contentsline {section}{\numberline {5.6}The Pigeonhole Principle}{98}%
\contentsline {section}{\numberline {5.7}Thing}{98}%
\contentsline {section}{\numberline {5.8}Number Systems}{98}%
\contentsline {section}{\numberline {5.9}Thing}{98}%
