


    %---%
    \section{Introduction to Relations}

    A \textbf{relation} is also a set of mappings from some domain set $A$ to some codomain set $B$,
    where the mappings from $A$ to $B$ come from the Cartesian product $A \times B$ (though
    not all values from $A \times B$ may be part of the relation).    
    Specifically, a \textbf{function is a type of relation} (but not vice versa). Additionally,
    if the inverse of a function is \textit{not a function}, then it is just a relation.
    
    In particular, we are going to be concerned with several different types of relations.

    %---%
    \subsection{Binary Relations}

        A \textbf{binary relation} is a relation $r : A \to B$ such that the \textbf{rule}
        (the rule mapping values from $A$ to $B$) is (or is a subset of) the Cartesian product $A \times B$. ~\\

        For example, let's take the sets $A = \{ 1, 2 \}$ as the domain and $B = \{ a, b, c \}$
        as the codomain for a relation $r : A \to B$. We can get the rule for this
        relation by taking $A \times B$:

        $$ \{ (1, a), (1, b), (1, c), (2, a), (2, b), (2, c) \} $$

        or it could just be a subset, like:

        $$ \{ (1, a), (1, c), (2, b) \}  $$

        We can diagram it as with the functions previously.

        \subsubsection{Binary relations on $A \to A$}

        When we have a binary relation whose domain and codomain come from the same set,
        then we can graph this relation as a set of relationships (arrows) between nodes (elements of $A$). ~\\

        Let's take the set $A = \{ 1, 2, 3, 4\}$ and the relation $r : A \to A$ with the rule $\{ (1, 2), (2, 3), (3, 1), (3, 4), (4, 2) \}$.
        We can graph it like this:

        \begin{center}
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \coordinate(almost1h) at (-0.9, 1);     \coordinate(almost1v) at (-1, 0.9);
                \coordinate (2) at (1,  1);     \coordinate(almost2h) at (0.9, 1);      \coordinate(almost2v) at (1, 0.9);
                \coordinate (3) at (-1, -1);    \coordinate(almost3h) at (-0.9, -1);    \coordinate(almost3v) at (-1, -0.9);    \coordinate(almost3hv) at (-0.9, -0.9);
                \coordinate (4) at (1,  -1);    \coordinate(almost4h) at (0.9, -1);     \coordinate(almost4v) at (1, -0.9);

                % Node circles
                \filldraw (1) circle (2pt) node[left]   {1};
                \filldraw (2) circle (2pt) node[right]  {2};
                \filldraw (3) circle (2pt) node[left]   {3};
                \filldraw (4) circle (2pt) node[right]  {4};

                % Directional arrows
                \draw[-stealth,ultra thick,blue]    (1) -- (almost2h);
                \draw[-stealth,ultra thick,orange]  (2) -- (almost3hv);
                \draw[-stealth,ultra thick,purple]  (3) -- (almost4h);
                \draw[-stealth,ultra thick,purple]  (3) -- (almost1v);
                \draw[-stealth,ultra thick,cyan]    (4) -- (almost2v);
            \end{tikzpicture}
        \end{center}

    \newpage
    %---%
    \subsection{Order Relations}

    An \textbf{order relation} is a type of relation $r : A \to A$ that
    creates a relationship from an input to an output based on their \textit{order}
    - such as which one is smaller or larger than the other. ~\\

    For example: Given the set $A = \{1, 2, 3\}$ and the relation $R : A \to A$,
    with the rule $(x, y) \in R$ if $x > y$. Here, we have \underline{two inputs},
    and the resulting output is ``true'' (draw an arrow from $x$ to $y$), or ``false'' (don't drawn an arrow).
    The diagram will look like this:

        \begin{center}
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \coordinate(almost1h) at (-0.9, 1);     \coordinate(almost1v) at (-1, 0.9);
                \coordinate (2) at (1,  1);     \coordinate(almost2h) at (0.9, 1);      \coordinate(almost2v) at (1, 0.9);
                \coordinate (3) at (-1, -1);    \coordinate(almost3h) at (-0.9, -1);    \coordinate(almost3v) at (-1, -0.9);

                % Node circles
                \filldraw (1) circle (2pt) node[left]   {1};
                \filldraw (2) circle (2pt) node[right]  {2};
                \filldraw (3) circle (2pt) node[left]   {3};

                % Directional arrows
                \draw[-stealth,ultra thick,blue]    (2) -- (almost1h);
                \draw[-stealth,ultra thick,purple]  (3) -- (almost2h);
                \draw[-stealth,ultra thick,purple]  (3) -- (almost1v);
            \end{tikzpicture}
        \end{center}

    In the diagram, \textbf{3} points to both \textbf{1} and \textbf{2} because it is larger than both of these.
    \textbf{2} points to \textbf{1}, and \textbf{1} points to nothing as there is nothing it is larger in within the set of $A$.
    Writing out all the elements of the rule, we would have $\{ (3, 1), (3, 2), (2, 1) \}$.

    \newpage
    \subsubsection{Relations on on power sets}

    The domain and codomain of a relation can be any kind of set. Let's take a relation $r$ whose domain and codomain is $\wp( \{1, 2\} )$.
    All the values of this set will be $\{ \emptyset, \{1\}, \{2\}. \{1, 2\} \}$. Each of the elements of this set can have
    a node, and a relationship can be drawn between them. ~\\

    For example, let's take the relation $r : \wp( \{1, 2\} ) \to \wp( \{1, 2\} )$ with the rule $(x, y) \in r$ if $x \subseteq y$.
    This means, a relationship (arrow) is drawn from input $x$ to input $y$ if $x \subseteq y$.



\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}
           ~\\ 1. First, draw out a node for each element of the domain/codomain.
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
        \centering
            \begin{tikzpicture}
                % Define points
                \coordinate (emptyset) at (-1, 1);
                \coordinate (1) at (1,  1);
                \coordinate (2) at (-1, -1);
                \coordinate (12) at (1,  -1);

                % Node circles
                \filldraw (emptyset) circle (2pt) node[left]   {$\emptyset$};
                \filldraw (1) circle (2pt) node[right]  {$\{1\}$};
                \filldraw (2) circle (2pt) node[left]   {$\{2\}$};
                \filldraw (12) circle (2pt) node[right]  {$\{1, 2\}$};

                % Directional arrows
                %\draw[-stealth,ultra thick,blue]    (1) -- (almost2h);
                %\draw[-stealth,ultra thick,orange]  (2) -- (almost3hv);
                %\draw[-stealth,ultra thick,purple]  (3) -- (almost4h);
                %\draw[-stealth,ultra thick,purple]  (3) -- (almost1v);
                %\draw[-stealth,ultra thick,cyan]    (4) -- (almost2v);
            \end{tikzpicture}
    \end{subfigure}
\end{figure}

    \vspace{1cm}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}
            ~\\ 2. Then, go through each element one at a time. Let's start with $\{1\}$.
            Compare $\{1\}$ with all elements, including itself. Draw a line between
            the two nodes if $x \subseteq y$.
            ~\\~\\
            $\{1\} \subseteq \{1\}$ is true, and $\{1\} \subseteq \{1, 2\}$ is true, so these lines are done.
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
        \centering
            \begin{tikzpicture}
                % Define points
                \coordinate (emptyset) at (-1, 1);
                \coordinate (1) at (1,  1);     \coordinate (1offset) at (1.5, 1.5);
                \coordinate (2) at (-1, -1);
                \coordinate (12) at (1,  -1);

                % Node circles
                \filldraw (emptyset) circle (2pt) node[left]   {$\emptyset$};
                \filldraw (1) circle (2pt) node[left]  {$\{1\}$};
                \filldraw (2) circle (2pt) node[left]   {$\{2\}$};
                \filldraw (12) circle (2pt) node[right]  {$\{1, 2\}$};

                % Directional arrows
                \draw[-stealth,ultra thick,blue]    (1) -- (12);
                \draw[-stealth,ultra thick,blue]    (1) to[bend left] (1.0, 1.5) to[bend left] (1.5, 1.5) to[bend left] (1.5, 1.0) to[bend left] (1.1, 1);
                %\draw[-stealth,ultra thick,orange]  (2) -- (almost3hv);
                %\draw[-stealth,ultra thick,purple]  (3) -- (almost4h);
                %\draw[-stealth,ultra thick,purple]  (3) -- (almost1v);
                %\draw[-stealth,ultra thick,cyan]    (4) -- (almost2v);
            \end{tikzpicture}
    \end{subfigure}
\end{figure}

    \vspace{1cm}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}
    ~\\ 3. We repeat this for each node, drawing an arrow if the relationship specified in the rule is true.
    ~\\~\\
    Note that the empty set $\emptyset$ is considered a subset of each other set, including itself.
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
        \centering
        \begin{tikzpicture}
            % Define points
            \coordinate (emptyset) at (-1, 1);
            \coordinate (1) at (1,  1);
            \coordinate (2) at (-1, -1);
            \coordinate (12) at (1,  -1);

            % Node circles
            \filldraw (emptyset) circle (2pt) node[left]   {$\emptyset$};
            \filldraw (1) circle (2pt) node[right]  {$\{1\}$};
            \filldraw (2) circle (2pt) node[left]   {$\{2\}$};
            \filldraw (12) circle (2pt) node[right]  {$\{1, 2\}$};

            % Directional arrows
            \draw[-stealth,ultra thick,blue]    (1) -- (12);
            \draw[-stealth,ultra thick,blue]    (1) to[bend left] (1.0, 1.5) to[bend left] (1.5, 1.5) to[bend left] (1.5, 1.0) to[bend left] (1.1, 1);
            \draw[-stealth,ultra thick,orange]  (2) -- (12);
            \draw[-stealth,ultra thick,orange]  (2) to[bend left] (-1.0, -1.5) to[bend left] (-1.5, -1.5) to[bend left] (-1.5, -1.0) to[bend left] (-1.1, -1);
            \draw[-stealth,ultra thick,purple]  (12) to[bend right] (1.0, -1.5) to[bend right] (1.5, -1.5) to[bend right] (1.5, -1.0) to[bend right] (1.1, -1);
            \draw[-stealth,ultra thick,cyan]  (emptyset) -- (1);
            \draw[-stealth,ultra thick,cyan]  (emptyset) -- (2);
            \draw[-stealth,ultra thick,cyan]  (emptyset) -- (12);
            \draw[-stealth,ultra thick,cyan]  (emptyset) to[bend right] (-1.0, 1.5) to[bend right] (-1.5, 1.5) to[bend right] (-1.5, 1.0) to[bend right] (-1.1, 1);
        \end{tikzpicture}
    \end{subfigure}
\end{figure}

    \newpage
    %---%
    \subsection{Equivalence Relations}

    An \textbf{equivalence relation} is another type of relation where
    we want to specify that two elements of the domain/codomain are
    \textit{considered equivalent}. ~\\

    For example, let's say we're flipping a coin three times. For each
    \textbf{heads} we get, we win \$5. So, the outcomes are:

    \begin{center}
        \begin{tabular}{| c | c | c | c |}
            \hline
            \textbf{ \$0 } & \textbf{ \$5 } & \textbf{ \$10 } & \textbf{ \$15 } \\ \hline
            TTT & TTH & HHT & HHH \\ \hline
                & THT & HTH & \\ \hline
                & HTT & THH & \\ \hline
        \end{tabular}
    \end{center}

    We could make a rule to say that any of the rounds that gives us \$5 (Tails-Tails-Heads, Tails-Heads-Tails, or Heads-Tails-Tails)
    are all equivalent, and same with all rounds that result in \$10. ~\\

    So, let's write out all possible game results, which will be our domain/codomain:

    $$ A = \{ TTT, TTH, THT, THH, HTT, HTH, HHT, HHH \} $$

    And our relation is

    \begin{center}
        $R = \{ (a, b) \in A \times A : $ $a$ and $b$ have hte same amount of $H$'s $\}$
    \end{center}

    We can then graph out all state nodes, and draw relationships between the states that are equivalent.

    \begin{center}
        \begin{tikzpicture}
            % Define points
            \coordinate (TTT) at (1.5, 3);
            \coordinate (TTH) at (3, 1.5);
            \coordinate (THT) at (3, -1.5);
            \coordinate (HTT) at (1.5, -3);
            \coordinate (THH) at (-1.5, -3);
            \coordinate (HTH) at (-3, -1.5);
            \coordinate (HHT) at (-3, 1.5);
            \coordinate (HHH) at (-1.5, 3);

            % Node circles
            \filldraw (TTT) circle (2pt) node[above]   {TTT};
            \filldraw (TTH) circle (2pt) node[right]   {TTH};
            \filldraw (THT) circle (2pt) node[right]   {THT};
            \filldraw (HTT) circle (2pt) node[below]   {HTT};
            \filldraw (THH) circle (2pt) node[below]   {THH};
            \filldraw (HTH) circle (2pt) node[left]   {HTH};
            \filldraw (HHT) circle (2pt) node[left]   {HHT};
            \filldraw (HHH) circle (2pt) node[above]   {HHH};

            % Directional arrows
            \draw[<->,ultra thick,blue]    (3.1, 1.4) -- (3, -1.4);    % TTH THT
            \draw[<->,ultra thick,blue]    (2.9, 1.4) -- (1.5, -2.9);  % TTH HTT
            \draw[<->,ultra thick,blue]    (3, -1.6) -- (1.6, -2.9);  % HTT THT

            \draw[<->,ultra thick,cyan]    (-3.1, 1.4) -- (-3, -1.4);    % THH HTH
            \draw[<->,ultra thick,cyan]    (-2.9, 1.4) -- (-1.5, -2.9);  % THH HHT
            \draw[<->,ultra thick,cyan]    (-3, -1.6) -- (-1.6, -2.9);  % HTH HHT

            % TTT circle
            \draw[->,ultra thick,orange]   (1.3, 3) to[bend left] (0.8, 3.5) to[bend left] (1.5, 4) to[bend left] (2.2, 3.5)  to[bend left] (1.7, 3 );
            % HHH circle
            \draw[->,ultra thick,purple]    (-1.3, 3) to[bend right] (-0.8, 3.5) to[bend right] (-1.5, 4) to[bend right] (-2.2, 3.5) to[bend right] (-1.7, 3);
            % TTH circle
            \draw[->,ultra thick,blue]     (3, 1.7) to[bend left] (3.8, 2.2) to[bend left] (4.3, 1.3) to[bend left] (3.2, 1.3);
            % THT circle
            \draw[->,ultra thick,blue]     (3, -1.7) to[bend right] (3.8, -2.2) to[bend right] (4.3, -1.3) to[bend right] (3.2, -1.3);
            % TTT circle
            \draw[->,ultra thick,blue]   (1.3, -3) to[bend right] (0.8, -3.5) to[bend right] (1.5, -4) to[bend right] (2.2, -3.5)  to[bend right] (1.7, -3);
            % TTH circle
            \draw[->,ultra thick,cyan]     (-3, 1.7) to[bend right] (-3.8, 2.2) to[bend right] (-4.3, 1.3) to[bend right] (-3.2, 1.3);
            % THT circle
            \draw[->,ultra thick,cyan]     (-3, -1.7) to[bend left] (-3.8, -2.2) to[bend left] (-4.3, -1.3) to[bend left] (-3.2, -1.3);
            % TTT circle
            \draw[->,ultra thick,cyan]   (-1.3, -3) to[bend left] (-0.8, -3.5) to[bend left] (-1.5, -4) to[bend left] (-2.2, -3.5)  to[bend left] (-1.7, -3);
        \end{tikzpicture}
    \end{center}

    The ruleset written out is: ~\\
        $ \{
            (TTT, TTT),$ ~\\~\\
            $(TTH, TTH), (TTH, THT), (TTH, HTT),$ ~\\
            $(THT, TTH), (THT, THT), (THT, HTT),$ ~\\
            $(HTT, TTH), (HHT, THT), (HHT, HHT),$ ~\\~\\
            $(HHT, HHT), (HHT, HTH), (HHT, THH),$ ~\\
            $(HTH, HHT), (HTH, HTH), (HTH, THH),$ ~\\
            $(THH, HHT), (THH, HTH), (THH, THH),$ ~\\~\\
            $(HHH, HHH)
        \} $

    \newpage
    %---%
    \subsection{Properties of Relations}

    There are several properties we are interested in when it comes to relations.
    You should be able to distinguish the properties of a relation both graphically
    and without a graph, just inspecting the ruleset.
    
    \begin{center}
        \small (Note: For the next graph I'm drawing nodes that point back to themselves as simple circles.)
    \end{center}
            
    \subsubsection{Reflexive, irreflexive, or neither}
    
        \begin{tabular}{p{6cm} p{6cm}}
            \textbf{Reflexive:} & \textbf{Irreflexive:} \\
            A relation is \textbf{reflexive} if, for all inputs $a$, $(a, a) \in R$.
            Graphically, this means \textbf{every node points back to itself}.
            &
            A relation is \textbf{irreflexive} if, for all inputs $a$, $(a, a) \not\in R$.
            Graphically, this means \textbf{no node points back to itself}.
            
            \\
            % Reflexive
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[-stealth,ultra thick,gray]    (1) -- (2);
                \draw[-stealth,ultra thick,gray]    (2) -- (4);
                \draw[-stealth,ultra thick,gray]    (4) -- (3);
                
                \draw[ultra thick,blue] (-1.4, 1) circle (10pt) node[left] {};
                \draw[ultra thick,blue] (1.4, 1) circle (10pt) node[left] {};
                \draw[ultra thick,blue] (-1.4, -1) circle (10pt) node[left] {};
                \draw[ultra thick,blue] (1.4, -1) circle (10pt) node[left] {};
            \end{tikzpicture}
            }
            &
            % Irreflexive
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[-stealth,ultra thick,gray]    (1) -- (2);
                \draw[-stealth,ultra thick,gray]    (2) -- (4);
                \draw[-stealth,ultra thick,gray]    (4) -- (3);
            \end{tikzpicture}
            }
            \\ \\
            \multicolumn{2}{l}{ \textbf{Neither:} }\\
            \multicolumn{2}{p{12cm}}{ A relation is neither reflexive nor irreflexive if there are some inputs $a$ that result in $(a, a) \in R$ and some that don't. } \\
            \multicolumn{2}{c}{
                % Neither
                \begin{tikzpicture}
                    % Define points
                    \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                    \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                    \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                    \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                    
                    \draw[-stealth,ultra thick,gray]    (1) -- (2);
                    \draw[-stealth,ultra thick,gray]    (2) -- (4);
                    \draw[-stealth,ultra thick,gray]    (4) -- (3);
                    
                    \draw[ultra thick,blue] (-1.4, 1) circle (10pt) node[left] {};
                    \draw[ultra thick,blue] (1.4, -1) circle (10pt) node[left] {};
                \end{tikzpicture}
            }
            \\
        \end{tabular}

    \newpage
    \subsubsection{Symmetric, antisymmetric, or neither}
    
        \begin{tabular}{p{6cm} p{6cm}}
            \textbf{Symmetric:} & \textbf{Antisymmetric:} \\
            A relation is \textbf{symmetric} if, for all input $a, b \in A$,
            where $a \neq b$, if $(a, b) \in R$ then $(b, a) \in R$.
            Graphically, \textbf{every arrow goes both directions.}
            &
            A relation is \textbf{antisymmetric} if, for all inputs $a, b \in A$,
            where $a \neq b$, if $(a, b) \in R$ then $(b, a) \not\in R$.
            Graphically, \textbf{every arrow goes in only ONE direction.}
            
            \\
            % Symmetric
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[<->,ultra thick,orange]    (-0.9, 1) -- (0.9, 1);
                \draw[<->,ultra thick,orange]    (1, 0.9) -- (1, -0.9);
                \draw[<->,ultra thick,orange]    (0.9, -1) -- (-0.9, -1);
                \draw[<->,ultra thick,orange]    (-0.9, -0.9) -- (0.9, 0.9);
            \end{tikzpicture}
            }
            &
            % Antisymmetric
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[->,ultra thick,blue]    (-0.9, 1) -- (0.9, 1);
                \draw[->,ultra thick,blue]    (1, 0.9) -- (1, -0.9);
                \draw[->,ultra thick,blue]    (0.9, -1) -- (-0.9, -1);
                \draw[->,ultra thick,blue]    (-0.9, -0.9) -- (0.9, 0.9);
            \end{tikzpicture}
            }
            \\ \\
            \multicolumn{2}{l}{ \textbf{Neither:} }\\
            \multicolumn{2}{p{12cm}}{ A relation is neither if, for $(a, b) \in R$, there are some $(b, a) \in R$ and some $(b, a) \not\in R$.
            Or, graphically, \textbf{some arrows point both ways and some don't.} } \\
            \multicolumn{2}{c}{
                % Neither
                \begin{tikzpicture}
                    % Define points
                    \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                    \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                    \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                    \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                    
                    \draw[->,ultra thick,cyan]    (-0.9, 1) -- (0.9, 1);
                    \draw[<->,ultra thick,cyan]    (1, 0.9) -- (1, -0.9);
                    \draw[<->,ultra thick,cyan]    (0.9, -1) -- (-0.9, -1);
                    \draw[->,ultra thick,cyan]    (-0.9, -0.9) -- (0.9, 0.9);
                \end{tikzpicture}
            }
            \\
        \end{tabular}

    \newpage
    \subsubsection{Transitive or intransitive}
    
        \begin{tabular}{p{12cm}}
            \textbf{Transitive:} \\
            A relation is \textbf{transitive} if, whenever you have $(a, b) \in R$
            and $(b, c) \in R$, then $(a, c) \in R$.
            Graphically, \textbf{if you can go from $a$ to $b$, and from $b$ to $c$,
            then you must also be able to go directly from $a$ to $c$.}
            \\
            % Symmetric
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[->,ultra thick,red]           (1) -- (0.9, 1);
                \draw[->,ultra thick,red]           (2) -- (1, -0.9);
                \draw[->,dotted,ultra thick,red]    (1) -- (0.9, -0.9);
                \draw[->,ultra thick,cyan]          (4) -- (0.9, -1);
            \end{tikzpicture}
            }
            \\
            Note here that $4$ goes to $3$, but since $3$ doesn't have any outputs,
            this does not invalidate this graph's transitivity.
            
            \\
            \textbf{Intransitive:} \\
            A relation is \textbf{intransitive} if it is not transitive.
            \\
            % Antisymmetric
            \multicolumn{1}{c}{
            \begin{tikzpicture}
                % Define points
                \coordinate (1) at (-1, 1);     \filldraw (1) circle (2pt) node[left]   {1};
                \coordinate (2) at (1, 1);      \filldraw (2) circle (2pt) node[right]   {2};
                \coordinate (3) at (1, -1);     \filldraw (3) circle (2pt) node[right]   {3};
                \coordinate (4) at (-1, -1);    \filldraw (4) circle (2pt) node[left]   {4};
                
                \draw[->,ultra thick,red]           (1) -- (0.9, 1);
                \draw[->,ultra thick,red]           (2) -- (1, -0.9);
                \draw[->,ultra thick,cyan]          (4) -- (0.9, -1);
            \end{tikzpicture}
            }
        \end{tabular}
    
    












