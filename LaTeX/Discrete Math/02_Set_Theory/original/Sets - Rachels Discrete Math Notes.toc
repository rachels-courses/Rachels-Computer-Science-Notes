\contentsline {chapter}{\numberline {1}Introduction to Sets}{2}
\contentsline {section}{\numberline {1.1}Basic sets}{2}
\contentsline {section}{\numberline {1.2}Element in a set $\in $, not in a set $\not \in $}{2}
\contentsline {section}{\numberline {1.3}Set cardinality}{3}
\contentsline {section}{\numberline {1.4}Common sets $\mathbb {Z}$, $\mathbb {N}$, $\mathbb {Q}$ and $\mathbb {R}$}{3}
\contentsline {section}{\numberline {1.5}Subsets}{4}
\contentsline {subsection}{\numberline {1.5.1}Universal set}{4}
\contentsline {subsection}{\numberline {1.5.2}Sets of sets}{4}
\contentsline {section}{\numberline {1.6}Set-builder notation}{5}
\contentsline {subsection}{\numberline {1.6.1}Property-description style}{5}
\contentsline {subsection}{\numberline {1.6.2}Form-description style}{5}
\contentsline {chapter}{\numberline {2}Unions, intersections, and differences}{6}
\contentsline {section}{\numberline {2.1}Operation basics}{6}
\contentsline {section}{\numberline {2.2}Complement of a set}{7}
\contentsline {section}{\numberline {2.3}Basic properties of sets}{7}
\contentsline {chapter}{\numberline {3}Venn diagrams}{8}
\contentsline {chapter}{\numberline {4}Partitions}{9}
\contentsline {section}{\numberline {4.1}All possible partitions of a set}{10}
\contentsline {chapter}{\numberline {5}Cartesian products}{11}
\contentsline {chapter}{\numberline {6}Power Sets}{12}
\contentsline {chapter}{\numberline {7}Set properties}{13}
\contentsline {section}{\numberline {7.1}The duality principle}{14}
\contentsline {chapter}{\numberline {8}Common pitfalls with set operations}{15}
