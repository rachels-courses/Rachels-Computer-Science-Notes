\input{../BASE-1-HEAD}
\newcommand{\laTopic}       {Sets}
\newcommand{\laTitle}       {Rachel's Discrete Math Notes}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\input{../BASE-2-HEADER}

\tableofcontents

\newpage

\chapter{Introduction to Sets}

    %---%
    \section{Basic sets}

    A \textbf{set} is an unordered collection of distinct items. For example:
    
    \begin{center}
        $ A = \{ 1, 2, 3, 4 \} $
        \tab
        $ B = \{ \$2.99, \$3.95 \} $ 
        \tab
        $ C = \{ red, orange, yellow \}$
    \end{center}
    
    Because a set is \textbf{unordered}, the sets $\{ 1, 2, 3, 4 \}$ and $\{ 2, 4, 1, 3 \}$ are considered equivalent.
    Additionally, $ \{ 1, 2 \} $ and $ \{ 1, 1, 2 \} $ are also considered equivalent since duplicate values don't matter.
    
    ~\\
    The individual items within a set are called \textbf{elements}.
    
    \begin{center}
        \begin{tabular}{ c c c c c c c }
            $D$         & $=$   & $\{$  & $a$,          & $b$,              & $c$           & $\}$  \\
            Set name    &       &       & First element & Second element    & Third element
        \end{tabular}
    \end{center}
    
    When we list all elements of a set like above, it's known as the \textbf{roster method} of describing sets.
    
    A set that contains no elements is known as an \textbf{empty set}, and can be written with either
    
    \begin{center}
        $\{ \}$ \tab or \tab $\emptyset$
    \end{center}
    
    %---%
    \section{Element in a set $\in$, not in a set $\not\in$}
    
    We can specify whether an element is or is not a member of a given set with these symbols.
    For example, let's take these sets:
    
    \begin{center}
        $A = \{ -1, 0, 1, 2 \}$ \tab $B = \{1, 2, 3\}$ \tab $C = \{2, 4, 6\}$
    \end{center}
    
    We can show that $1 \in A$ (1 is in the set $A$), $1 \in B$ (1 is in the set $B$), 
    and $1 \not\in C$ (1 is not in the set $C$).
    
    %---%
    \section{Set cardinality}
    
    The \textbf{cardinality} of a set is the count of distinct items within a set.
    Given a set $A$, we can write ``the cardinality of $A$'' as $|A|$.
    
    \begin{center}
        \begin{tabular}{l l l}
            \textbf{Set}                & \textbf{Cardinality} \\ \hline
            $A = \emptyset$             & $|A| = 0$ \\
            $B = \{ 1, 2, 3 \}$         & $|B| = 3$ \\
            $C = \{ cat, cat, dog \}$   & $|C| = 2$ & We only count each \underline{distinct} item. \\
            $\mathbb{Z}$                & $|\mathbb{Z}| = $ infinite
        \end{tabular}
    \end{center}
    
    Note that in some books, $n(A)$ is the notation used to indicate the cardinality of set $A$.
    
    %---%
    \section{Common sets $\mathbb{Z}$, $\mathbb{N}$, $\mathbb{Q}$ and $\mathbb{R}$}
    
    In mathematics you will see the following sets pretty regularly:
    
    \begin{center}
        \begin{tabular}{ l p{12cm} }
            $\mathbb{Z}$    & The set of all \textbf{integers} \\
                            & Integers are whole numbers, including positive, negative, and zero. \\
                            & Example: $\{ ..., -2, -1, 0, 1, 2, ... \}$ \\ \\
            $\mathbb{N}$    & The set of all \textbf{natural numbers} \\
                            & Natural numbers are ``counting numbers'', whole numbers 0 and up. \\ 
                            & Example: $\{0, 1, 2, 3, ...\}$ \\ \\
            $\mathbb{Q}$    & The set of all \textbf{rational numbers} \\ 
                            & Rational numbers are numbers that can be represented as a ratio. \\
                            & Example values: $\frac{1}{2}, \frac{5}{1}, \frac{5}{3}$ \\ \\
            $\mathbb{R}$    & The set of all \textbf{real numbers} \\
                            & Real numbers, which include the sets above, plus numbers with unending strings of digits after a decimal point. \\
                            & Example values: $1, \frac{1}{2}, \pi, \sqrt{3}$
        \end{tabular}
    \end{center}
    
    You can further restrict these sets by adding a $+$ superscript to mean ``positive only'',
    or a $\geq 0$ superscript to mean ``non-negative''. ~\\ 
    For example: $\mathbb{Z}^{\geq 0}$, $\mathbb{Q}^{+}$
    
    %---%
    \section{Subsets}
    
    When working with multiple sets, we may be interested to inspect how
    two sets are related to each other in different ways.
    
    \begin{center}
        \begin{tabular}{l l}
            $A \subseteq B$ & $A$ is a \textbf{subset} of $B$ if all elements from $A$ are also in $B$. \\ \\
            $A \subset B$   & $A$ is a \textbf{proper subset} of $B$ if all elements from $A$ are in $B$, \\
                            & but also $A \neq B$ (they don't share \textit{all} of the same elements).     \\ \\
            $A = B$ & The two sets $A$ and $B$ are \textbf{equal} if all elements of $A$ are also in $B$. \\
                    & If $A \subseteq B$ and $B \subseteq A$, that means $A = B$. \\ \\
        \end{tabular}
    \end{center}
    
    \subsection{Universal set}
    
    When working with multiple sets, we use the \textbf{universal set} $U$ to be a set that contains
    \textit{all elements} of \textit{all sets}.
    
    \subsection{Sets of sets}
    
    A set can also contain a set \textit{within} it. For example, let's say we have a set of pizzas available at a restaurant:
    \begin{center}
        \{ cheese \}, \{ cheese, pepperoni \}, \{ cheese, olives, onions, tomatoes \} \\
        \{ cheese, pineapple, ham \}, \{ cheese, sausage, pepperoni, bacon \}
    \end{center}
    
    And we could have a set that contains which pizzas are vegetarian:
    \begin{center}
        $V =$ \{ \{ cheese \}, \{ cheese, olives, onions, tomatoes \} \}
    \end{center}
    
    A set can contain sets as well as non-set elements all in one:
    \begin{center}
        $V =$ \{ cookie, pasta, \{ cheese \}, \{ cheese, olives, onions, tomatoes \} \}
    \end{center}
    
    And, a set can even contain empty sets within it, so note that
    \begin{center}
        $\{ \{ \} \}$   \tab ...or can be written as... \tab $ \{ \emptyset \} $
    \end{center}
    is not the same as an empty set - it is a set of one element, and that one element is an empty set.
    
    
    %---%
    \section{Set-builder notation}
    For large sets or sets with an infinite amount of elements, it wouldn't
    make sense to try to define the set by writing out all elements. In this case,
    we can use \textbf{set-builder notation} to define a set.
    
    \subsection{Property-description style}
    
    Let's say we want to specify the set of all even integers.
    We have a starting point with $\mathbb{Z}$, but that includes everything.
    We can use the \textbf{property-description} of set-builder notation like this:
    
    \begin{center}
        $ \{ x \in \mathbb{Z} : x$ is even$ \} $
    \end{center}
    
    For the first part, we define the wider domain that our element $x$ belongs in.
    The second part descripts additional properties that restrict the elements that
    belong in the set we're building.
    
    \subsection{Form-description style}
    
    Now let's specify the set of all even integers, but using the \textbf{form-description} style:
    
    \begin{center}
        $ \{ 2x : x \in \mathbb{Z} \}$
    \end{center}
    
    This is more concise, and specifies the \textit{form} elements take in the set.
    If you're familiar with number theory, 2 times any integer is an even integer,
    so given any $x$ in the domain $\mathbb{Z}$, $2x$ will be even.
    
    

\chapter{Unions, intersections, and differences}

    %---%
    \section{Operation basics}
    We can build new sets from two existing sets by combining all their elements (\textbf{unions}),
    taking only their common elements (\textbf{intersection}), or taking one set but without
    any of the elements from the other (\textbf{difference}). ~\\
    
    Let's take two sets:
    
    \begin{center}
        $A = \{ 1, 2, 3 \}$ \tab $B = \{ 2, 3, 4 \}$
    \end{center}
    
    The \textbf{intersection} of $A$ and $B$, written symbolically as $A \cap B$, is:
    
    $$ A \cap B = \{ 2, 3 \} $$
    
    The \textbf{union} of $A$ and $B$, written symbolically as $A \cup B$, is:
    
    $$ A \cup B = \{ 1, 2, 3, 4 \}$$
    
    The \textbf{difference} of $A$ and $B$, written symbolically as $A - B$, is:
    
    $$ A - B = \{ 1 \}$$
    
    The \textbf{difference} of $B$ and $A$, written symbolically as $B - A$, is:
    
    $$ B - A = \{ 4 \}$$
    
    If you take the difference of two sets with \textit{no} elements in common,
    then the result is an empty set, $\emptyset$.
    
    \section{Complement of a set}
    Remember that a \textbf{universal set} $U$. When no universal set is explicitly
    mentioned, its elements will be the elements of all sets in the problem \\ ($A \cup B \cup C$...).
    Otherwise, the universal set can be defined explicitly to something, such as $U = \mathbb{Z}$. ~\\
    
    The \textbf{complement} of a some set $A$ (written $A'$) is the universal set $U$, with the elements
    of $A$ removed. In other words, $A' = U - A$.
    
    (Note: Sometimes, the complement is written as $\bar{A}$)
    
    ~\\
    Given \tab $U = \{ 2, 4, 6, 8, 10 \}$  and  $A = \{ 2, 4 \}$, \tab $A' = \{ 6, 8, 10 \}$.
    
    \section{Basic properties of sets}
    
        \begin{tabular}{l l}
            Commutative properties          & $A \cap B = B \cap A$
            \\                              & $A \cup B = B \cup A$ 
            \\ \\
            Associative properties          & $(A \cup B) \cup C = A \cup (B \cup C)$
            \\                              & $(A \cap B) \cap C) = A \cap (B \cap C)$
            \\ \\
            Distributive properties         & $A \cup(B \cap C) = (A \cup B) \cap (A \cup C)$
            \\                              & $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
        \end{tabular}
            
        
\chapter{Venn diagrams}

    Venn diagrams can be used to visually represent sets and relationships between sets.
    A box is drawn to represent the universal set $U$, and then circles for each set are
    added. The circles overlap, and we can shade regions to indicate what parts of sets
    $A$, $B$, and $C$ are used. ~\\
    
    \def\circleA{(1.5,1.5) circle (1.0cm)}
    \def\circleB{(2.5,1.5) circle (1.0cm)}
    
    \begin{tabular}{c c c}
        %A%
        \begin{tikzpicture}
            \draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
            \draw[fill=orange] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
        \end{tikzpicture}
        &
        
        %A'%
        \begin{tikzpicture}
            \draw[fill=orange] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
            \draw[fill=white] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
        \end{tikzpicture}
        \\
        
        Diagram of $A$
        &
        Diagram of $A'$
        \\
        \\
    
        % A intersection B
         \begin{tikzpicture}
             \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
             \begin{scope}
                 \clip \circleA;
                 \fill[fill=orange] \circleB;
             \end{scope}
             \draw \circleA node[anchor=south east] {$A$};
             \draw \circleB node[anchor=south west] {$B$};
         \end{tikzpicture}
         &
         
        % A union B
        \begin{tikzpicture}
            \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
            \draw[fill=orange]  \circleA node[anchor=south east] {$A$}
                                \circleB node[anchor=south west] {$B$};
        \end{tikzpicture}
        &
        
        % A - B
         \begin{tikzpicture}
             \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
             \begin{scope}
                 \clip \circleA;{}
                 \draw[fill=orange, even odd rule]   \circleA node[anchor=south east] {$A$}
                                                     \circleB;
             \end{scope}
             \draw[]             \circleA node[anchor=south east] {$A$}
                                 \circleB node[anchor=south west] {$B$};
         \end{tikzpicture}
        
        \\
        A diagram of $A \cap B$, &
        A diagram of $A \cup B$, &
        A diagram of $A - B$, \\
        $A$ intersection $B$. &
        $A$ union $B$. &
        The difference of $A$ and $B$. 
    \end{tabular}

\chapter{Partitions}

    A \textbf{partition} of a set is a way of grouping all the elements of the set in sub-sets.
    
    For example, let's say that we have a set of courses you must take:
    
    \begin{center}
        $C = \{$ CS210, CS211, CS200, CS250 $\}$
    \end{center}
    
    We could partition this set into groups of courses you could take together in a semester, such as...
    
    ~\\
    \begin{tabular}{l p{6cm}}
        \{ \{ CS210, CS211, CS200, CS250 \} \}      & Take all courses in one semester 
        \\ \\
        
        \{ \{ CS210, CS200 \}, \{ CS211, CS250 \} \} & Take CS 210 and CS 200 together in one semester,
                                                       and take CS 211 and CS 250 together in another.
        \\ \\
        \{ \{ CS210 \}, \{ CS211 \}, \{ CS200 \}, \{ CS250 \} \} & Take each class by itself in each semester.
    \end{tabular}
    
    And so on. ~\\
    
    Note that a partition will always be a \textbf{set of sets}, 
    with each of the subsets containing elements from
    the original set we're making a partition of.
    
    Additionally, each element of the original set will only show up in \underline{one} subset; not zero or two.
    All elements must be present in the partition, and there cannot be any empty sets in the partition.
    
    \newpage
    In other words, given some set $A$ that is partitioned into a partition set $P$,
    which contains subsets (called \textbf{parts}) $S_1$, $S_2$, and so on...
    
    \begin{itemize}
        \item   None of the parts $S_i$ in the partition $P$ are empty. ($S_i \neq \emptyset$)
        \item   If you take the \textbf{union} of all parts together, you get the original set.
                \\ $(S_1 \cup S_2 \cup ... \cup S_n) = A$
        \item   No two parts in the partition have anything in common.
                \\ $S_i \cap S_j = \emptyset$ for all subsets $i$ and $j$ (where $S_i$ is not equal to $S_j$).
    \end{itemize}
    
    \section{All possible partitions of a set}
    
    For finite sets, it is possible to list out all possible partitions one could build from the original set.
    
    \begin{center}
        \begin{tabular}{l l l | l l}
            Set         & All partitions    & & Set & All partitions
            \\ \hline
            $\{a\}$     & $\{ \{a\} \}$         & & $\{a, b, c\}$     & $\{ \{a, b, c\} \}$ \\
                                                                & & & & $\{ \{a, b\}, \{c\} \}$ \\
            $\{a, b\}$  & $\{ \{a, b\} \}$                        & & & $\{ \{a\}, \{b, c\} \}$ \\
                        & $\{ \{a\}, \{b\} \}$                    & & & $\{ \{a, c\}, \{b\} \}$ \\
                                                                & & & & $\{ \{a\}, \{b\}, \{c\} \}$
        \end{tabular}
    \end{center}


\chapter{Cartesian products}

    Given two sets, $A$ and $B$, the \textbf{Cartesian product} of $A$ and $B$, written $A \times B$,
    is a set containing \textbf{ordered pairs}, pairing all elements from $A$ (in the $x$ position)
    with all elements from $B$ (in the $y$ position).
    
    ~\\
    Given two sets $A = \{1, 2\}$ and $B = \{a, b, c\}$, we can find $A \times B$ like this:
    
    \begin{center}
        \begin{tabular}{c | c | c | c}
                        & $B_1 = a$     & $B_2 = b$     & $B_3 = c$ \\ \hline
            $A_1 = 1$   & $(1, a)$      & $(1, b)$      & $(1, c)$ 
            \\
            $A_2 = 2$   & $(2, a)$      & $(2, b)$      & $(2, c)$
        \end{tabular}
    \end{center}
    
    So the final result is:
    
    \begin{center}
        $A \times B = \{ (1, a), (1, b), (1, c), (2, a), (2, b), (2, c) \}$
    \end{center}
    


\chapter{Power Sets}

    Given some set $A$, the \textbf{power set} of $A$ (written $\wp(A)$) is
    the set of all possible subsets of $A$, INCLUDING the empty set. ~\\
    
    ~\\
    \begin{tabular}{ p{3cm} l l }
    Set & Power set \\ \hline
    $A = \{ x \}$       & $\wp(A) =$ & $\{ \emptyset, \{x\} \}$
    \\ \\
    
    $B = \{ x, y \}$    & $\wp(B) =$ & $\{ \emptyset, \{x\}, \{y\}, \{x, y\} \}$
    \\ \\
    
    $C = \{ x, y, z \}$ & $\wp(C) =$ &  $\{ \emptyset, \{x\}, \{y\}, \{z\},$ \\
                        &            &  $ \{x, y\}, \{y, z\}, \{x, z\},$ \\
                        &            &  $ \{x, y, z\} \}$
    \\ \\
    $D = \{ w, x, y, z \}$  & $\wp(D)=$     & $\{ \emptyset, \{w\}, \{x\}, \{y\}, \{z\},$ \\
                            &               &  $\{w, x\}, \{w, y\}, \{w, z\}, \{x, y\}, \{x, z\}, \{y, z\}$ \\
                            &               &  $\{w, x, y\}, \{w, x, z\}, \{w, y, z\}, \{x, y, z\}$ \\
                            &               &  $\{ w, x, y, z \} \}$ \\
    \end{tabular}
    
    ~\\
    
    With the partition, you were simply moving elements into parts, with all the elements
    appearing only once. With a power set, each subset listed is a set
    that contains 0 to all elements of the original set; it's a list of all possible subsets you could get from that original set.

    
\chapter{Set properties}

    \begin{tabular}{l l}
        Commutative     & $A \cap B = B \cap A$
        \\              & $A \cup B = B \cup A$ 
        \\ \hline
        Associative     & $(A \cup B) \cup C = A \cup (B \cup C)$
        \\              & $(A \cap B) \cap C) = A \cap (B \cap C)$
        \\ \hline
        Distributive    & $A \cup(B \cap C) = (A \cup B) \cap (A \cup C)$
        \\              & $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
        \\ \hline
        Identity        & $A \cup \emptyset = A$
        \\              & $A \cap U = A$
        \\ \hline
        Complement      & $A \cup A' = U$
        \\              & $A \cap A' = \emptyset$
        \\ \hline
        Idempotent      & $A \cup A = A$
        \\              & $A \cap A = A$
        \\ \hline
        Domination      & $A \cup U = U$
        \\              & $A \cap \emptyset = \emptyset$
        \\ \hline
        Absorption      & $A \cup (A \cap B) = A$
        \\              & $A \cap (A \cup B) = A$
        \\ \hline
        DeMorgan's laws & $(A \cup B)' = A' \cap B'$
        \\              & $(A \cap B)' = A' \cup B'$
        \\ \hline
        Double complement   & $(A')'  A$
        \\ \hline
        Complements of $U$ and $\emptyset$  & $\emptyset' = U$
        \\                                  & $U' = \emptyset$
        \\ \hline
        Reflexivity     & $A \subseteq A$
        \\ \hline
        Antisymmetry    & $A \subseteq B$ and $B \subseteq A$ if and only if $A = B$
        \\ \hline
        Transitivity    & If $A \subseteq B$ and $B \subseteq C$ then $A \subseteq C$
    \end{tabular}
    \footnote{From https://en.wikipedia.org/wiki/Algebra\_of\_sets}
    
    \section{The duality principle}
    
    Each true set equivalence, can be transformed by
    replacing all $\cup \leftrightarrow \cap$ and $\emptyset \leftrightarrow U$.
    
    You can see this illustrated in each pair of identities.
    
    \begin{center}
        $A \cup \emptyset = A$ \tab and \tab $A \cap U = A$
    \end{center}

\chapter{Common pitfalls with set operations}
    Over the years I see the following items as a common mistake that students make,
    so this is just to make you aware so that you can try to avoid the mistakes:
    
    \begin{itemize}
        \item   Partitions: Result in a \underline{set of parts}, where parts are also sets.
        \item   Power sets: Result in a \underline{set of all subsets} that can be built from the original set.
        \item   Cartesian products: Result in a \underline{set of ordered pairs}, combining elements from two sets.
    \end{itemize}
    
    Make sure your notation is correct, and you're using curly-braces \{ \} and parentheses ( )
    in the appropriate spots.
    
\input{../BASE-4-FOOT}
