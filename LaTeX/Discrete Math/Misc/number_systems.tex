
    %---%
    \section{Properties of Numbers}
    
        \subsection{Digits}
        For the decimal number $2,368$, we can write this as its
            individual digits:

            \begin{center}
                \begin{tabular}{ | c | c | c | c | }
                    \hline
                    \textbf{Thousands}   ($10^{3}$) &
                    \textbf{Hundreds}    ($10^{2}$) &
                    \textbf{Tens}        ($10^{1}$) &
                    \textbf{Ones}        ($10^{0}$)
                    \\ \hline
                    2 & 3 & 6 & 8
                    \\ \hline
                \end{tabular}
            \end{center}

            And then we can build out $2,368$ as the mathematical equation:

            $$ 2 \cdot 10^{3} + 3 \cdot 10^{2} + 6 \cdot 10^{1} + 8 \cdot 10^{0} $$

            Likewise, for the binary number 0101 1001, we can write it as:

            \begin{center}
                \begin{tabular}{ | c | c | c | c | c | c | c | c | }
                    \hline
                    $2^{7}$ &
                    $2^{6}$ &
                    $2^{5}$ &
                    $2^{4}$ &
                    $2^{3}$ &
                    $2^{2}$ &
                    $2^{1}$ &
                    $2^{0}$
                    \\ \hline
                    0 & 1 & 0 & 1 & 1 & 0 & 0 & 1
                    \\ \hline
                \end{tabular}
            \end{center}

            And into the equation:

            $$1 \cdot 2^{6} + 1 \cdot 2^{4} + 1 \cdot 2^{3} + 1 \cdot 2^{0} $$
            
            \newpage
            There are also formal definitions for these as well, where you multiply each number
            by the number place it belongs to, adding them altogether...
            
            \begin{hint}{Definition, base-10 number \footnote{From Discrete Mathematics, Ensley and Crawley, page 151}} 
                Given a positive integer $X$, the decimal representation for $X$ is a string
                consisting of digits from $\{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 \}$ that looks like ~\\
                $d_n d_{n-1} ... d_1 d_0$, where
                
                $$X = \sum_{i=0}^{n} (d_i \cdot 10^i)$$
                
                $= d_n \cdot 10^n + d_{n-1} \cdot 10^{n-1} + ... + d_2 \cdot 10^2 + d_1 \cdot 10^1 + d_0 \cdot 10^0 $
            \end{hint}
            
            
            \begin{hint}{Definition, base-2 number \footnote{From Discrete Mathematics, Ensley and Crawley, page 152}} 
                The base-2 representation of an integer $X$ is a string of digits from $\{ 0, 1 \}$
                that looks like $b_n b_{n-1} ... b_2 b_1 b_0$, where
                
                $$X = \sum_{i=0}^{n} (b_i \cdot 2^i)$$
                
                $= b_n \cdot 2^n + b_{n-1} \cdot 2^{n-1} + ... + b_2 \cdot 2^2 + b_1 \cdot 2^1 + b_0 \cdot 2^0 $
            \end{hint}
            
            And, this can be abstracted for any base - just replace $10^i$ or $2^i$ with the base we're talking about.
            
            
        \newpage
        \subsection{Hexadecimal (Base-16)}
            Hexadecimal numbers go from 0 to 15, but the two-digit numbers (10 through 15)
            are usually represented with letters so that you can write out
            a hex number with single characters.

            \begin{center}
                A = 10  \tab    B = 11  \tab    C = 12  \tab    D = 13  \tab    E = 14 \tab F = 15
            \end{center}
        
        \hrulefill
        \subsection{Converting between bases}
        
            \subsubsection{Base-10 to others...}
        
                \begin{intro}{Algorithm for converting a decimal number to base $b$:}
                    \begin{enumerate}
                        \item   Input a natural number $n$
                        \item   While $n > 0$, do the following:
                            \begin{enumerate}
                                \item   Divide $n$ by $b$ and get a quotient $q$ and remainder $r$.
                                \item   Write $r$ as the next (right-to-left) digit.
                                \item   Replace the value of $n$ with $q$, and repeat.
                            \end{enumerate}
                    \end{enumerate}
                \end{intro}
                
            \paragraph{Example:} Convert $(14)_{10}$ to base-2.
            
            \begin{center}
                \begin{tabular}{l l l}
                    \textbf{$n$ value}  & \textbf{(a)}                      & \textbf{Binary string} \\ \hline
                    $n = 14$            & $14 / 2$: $q = 7$, $r = 0$        & \texttt{0}
                    \\
                    $n = 7$             & $7 / 2$: $q = 3$, $r = 1$         & \texttt{10}
                    \\
                    $n = 3$             & $3 / 2$: $q = 1$, $r = 1$         & \texttt{110}
                    \\
                    $n = 1$             & $1 / 2$: $q = 0$, $r = 1$         & \texttt{1110}
                \end{tabular}
            \end{center}
            
            So the result is (\texttt{1110})$_2$
            
                
            \newpage
            \subsubsection{From others to Base-10...}
            
            Use the definition of a number, writing out the base-$B$ string as an equation.
            Solving this equation will get you the base-10 number.
            
            \begin{hint}{Definition of a base-B number} 
                $$ (D)_{10} =  \sum_{i=0}^n ( x_i \cdot B^i ) $$
                
                Where $B$ is the base (Base 4, base 5, base 6, ...) and $x_i$ is the $i$th number in the number string
                (For ``123'', 3 is at the 0th position, 2 at the 1st position, 1 at the 2nd position...)
            \end{hint}
            
            \paragraph{Example:} Convert $(543)_{6}$ to base-10:
            
            I like to write a table to show the places each digit belongs...
            
            \begin{center}
                \begin{tabular}{ l | c | c | c }
                    $B^i$ & $6^2$ & $6^1$ & $6^0$ \\ \hline
                    $x_i$ & 5 & 4 & 3 
                \end{tabular}
            \end{center}
            
            So our equation is:
            $$ 5 \cdot 6^2 + 4 \cdot 6^1 + 3 \cdot 6^0 $$
            $$ = 5 \cdot 36 + 4 \cdot 6 + 3 \cdot 1 $$
            $$ = 180 + 24 + 3 $$
            $$ = (207)_{10} $$
            
            \newpage
            \subsubsection{Base-2 to Base-16 and vice versa...}
            
                Often in computers, we write binary strings as hexadecimal
                to save space and make it easier to read.

                \begin{center}
                    \begin{tabular}{l c c c c c c c c}
                        Hex & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7
                        \\
                        Binary & 0000 & 0001 & 0010 & 0011 & 0100 & 0101 & 0110 & 0111
                        \\ \hline
                        Hex & 8 & 9 & A & B & C & D & E & F
                        \\
                        Binary & 1000 & 1001 & 1010 & 1011 & 1100 & 1101 & 1110 & 1111
                    \end{tabular}
                \end{center}

                \paragraph{Example:}
                Convert 11001 from binary to hexadecimal \\
                    1. Write out in chunks of four. Add leading 0's to the left side. \\
                        \tab 0001 1001 \\
                    2. Swap out each ``nibble" with hexadecimal \\
                        \tab 0001 = 1 \tab 1001 = 9 \\ \\
                    So, (0001 1001)$_{2}$ = (19)$_{16}$

                \paragraph{Example:}
                Convert $DAD$ from hexadecimal to binary \\
                    1. Convert each digit back to binary. \\
                        \tab D = 1101 \tab A = 1010 \tab D = 1101
                    \\ \\ So, (DAD)$_{16}$ = (1101 1010 1101)$_{2}$
        
        
        
        
        

