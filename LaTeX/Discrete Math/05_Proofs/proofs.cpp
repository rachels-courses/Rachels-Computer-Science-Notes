#include <iostream>
using namespace std;

int main()
{
    
    int a = 100;
    int b = 6;
    int q = a / b;
    int r = a % b;

    cout << "a: " << a << endl
        << "b: " << b << endl
        << "q: " << q << endl
        << "r: " << r << endl;

    cout << a << " = " << b << " * " << q << " + " << r << endl;

    return 0;
}
