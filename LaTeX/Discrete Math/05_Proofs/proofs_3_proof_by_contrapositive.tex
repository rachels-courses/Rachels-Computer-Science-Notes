Let's look back at when we were working with \textbf{implications}
during the Propositional Logic section. Remember that an implication
(a conditional statement) had a truth table of:

\begin{center}
  Implication ~\\
  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $p \to q$ \\ \hline
      \true   & \true   & \true       \\  \hline
      \true   & \false  & \false      \\ \hline
      \false  & \true   & \true       \\ \hline
      \false  & \false  & \true       \\ \hline
  \end{tabular}
\end{center}

~\\ We also looked at the truth tables of Converse, Inverse, and Contrapositives: 
\begin{center}
\begin{tabular}{c c c}
  Converse & Inverse & Contrapositive \\

  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $q \to p$ \\ \hline
      \true   & \true   & \true         \\ \hline
      \true   & \false  & \true         \\ \hline
      \false  & \true   & \false        \\ \hline
      \false  & \false  & \true         \\ \hline
  \end{tabular}
  &
  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $\neg p \to \neg q$ \\ \hline
      \true   & \true   & \true             \\ \hline
      \true   & \false  & \true             \\ \hline
      \false  & \true   & \false            \\ \hline
      \false  & \false  & \true             \\ \hline
  \end{tabular}
  &
  \begin{tabular}{ | c | c || c | } \hline
      $p$     & $q$     & $\neg q \to \neg p$ \\ \hline
      \true   & \true   & \true             \\ \hline
      \true   & \false  & \false            \\ \hline 
      \false  & \true   & \true             \\ \hline
      \false  & \false  & \true             \\ \hline
  \end{tabular}
\end{tabular}
\end{center}

Notice that the truth table for the \textbf{implication} 
and the \textbf{contrapositive} are the same: The result is
the same, given the same state of $p$ and $q$ together.
This means, we can build proofs using the \textbf{contrapositive} if,
for some reason, it is difficult to prove it using just the normal
implication.


\newpage
\begin{hint}{Review: Implications}
    \begin{tabular}{l p{5.5cm} l}
        Implication     & $p \to q$ &
        \begin{tabular}{ c c | c }
            $p$ & $q$ & $p \to q$ \\ \hline
            T & T & T \\
            T & F & F \\
            F & T & T \\
            F & F & T 
        \end{tabular}
        \\ \\
        Negation        & $p \land \neg q$ &
        \begin{tabular}{ c c | c }
            $p$ & $q$ & $p \land \neg q$ \\ \hline
            T & T & F \\
            T & F & T \\
            F & T & F \\
            F & F & F 
        \end{tabular}
        \\ \\
        Converse        & $q \to p$ &
        \begin{tabular}{ c c | c }
            $p$ & $q$ & $q \to p$ \\ \hline
            T & T & T \\
            T & F & T \\
            F & T & F \\
            F & F & T 
        \end{tabular}
        \\ \\
        Inverse         & $\neg p \to \neg q$ &
        \begin{tabular}{ c c | c }
            $p$ & $q$ & $\neg p \to \neg q$ \\ \hline
            T & T & T \\
            T & F & T \\
            F & T & F \\
            F & F & T 
        \end{tabular}
        \\ \\
        Contrapositive  & $\neg q \to \neg p$ &
        \begin{tabular}{ c c | c }
            $p$ & $q$ & $\neg q \to \neg p$ \\ \hline
            T & T & T \\
            T & F & F \\
            F & T & T \\
            F & F & T 
        \end{tabular}
        \\ \\
    \end{tabular}
\end{hint}


\newpage

\paragraph{Example:} For any integer $n$, if $n^2$ is even, then $n$ is even. \footnote{Problem from Discrete Mathematics by Ensley and Crawley} ~\\

How might we make a direct proof?

\begin{itemize}
    \item   Hypothesis: $n^2$ is even
    \item   Conclusion: $n$ is even
\end{itemize}

How would we go from $n^2 = 2k$ to $n = $ (definition of some even integer)?
It would be eaiser to go from $n = 2j$ to $n^2 =$ (definition of some even integer)...
so, let's do that by using the contrapositive!

~\\
Original: ``if $n^2$ is even, then $n$ is even.'' ~\\
Contrapositive: ``if $n$ is not even, then $n^2$ is not even.''

~\\ Now we do a direct proof...

\begin{enumerate}
    \item   Hypothesis: $n$ is not even     ~\\ ($n = 2k + 1$ as an alias)
    \item   Conclusion: $n^2$ is not even  ~\\ (Don't need an alias for this, just the hypothesis.)
    \item   Work out the direct proof
    \begin{enumerate}
        \item   $n$ is not even, so $n^2$ is not even.
        \item   $2k+1$ is not even, so $(2k+1)^2$ is not even.
        \item   $(2k+1)^2 = (2k+1)(2k+1) = 4k^2 + 2k + 2k + 1 = 4k^2 + 4k + 1$
        \item   Factor into definition: $4k^2 + 4k + 1 = 2( 2k^2 + 2k) + 1$
        \item   $2k^2 + 2k$ is just ``some integer'' (e.g., $j$),
                so $2( 2k^2 + 2k ) + 1$ is ~\\ ``2 times some integer plus 1'' ($2j + 1$),
                which is the definition of an odd integer. So we have proven the statement.
    \end{enumerate}
\end{enumerate}






