
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
using namespace std;

int RecursiveTest( int n ) {
    cout << "a[" << n << "] = ";
    if ( n == 1 ) { 
        cout << "2" << endl;
        return 2; 
    }
    else { 
        cout << "2 * a[" << n-1 << "]" << endl;
        return 2 * RecursiveTest( n - 1 ); 
    }
}

int ClosedTest( int n ) {
    cout << 2 << "^(n-1)" << "";
    return pow(2, n-1);
}

int SummationTest( int n ) {
    int sum = 0;
    
    for ( int i = 1; i <= n; i++ ){
        if ( i != 1 ) { cout << " + "; }
        cout << 2 * (i-1);
        sum += (2*(i-1));
    }
    
    return sum;
}


int main()
{        
    int result;
    for ( int n = 1; n <= 10; n++ )
    {
        cout << "-- n = " << n << " ---------------------------" << endl;
        cout << setw(10) << "Closed formula: " << setw(10);
        result = ClosedTest( n );
        cout << setw(0) << " = " << result << endl;
        
        cout << setw(10) << "Summation:      " << setw(10);
        result = SummationTest( n );
        cout << setw(0) << " = " << result << endl << endl;
    }
    
    
    cout << endl << endl;

    return 0;
}
