

    \begin{wrapfigure}{r}{0.4\textwidth}
        \vspace{-20pt}
            \begin{center}
            \includegraphics[width=0.38\textwidth]{Introduction/images/writing}
            \end{center}
        \vspace{-10pt}
    \end{wrapfigure}

    \section{Welcome}
        It feels weird to start a collection of notes (or a ``textbook'')
        without some sort of welcome, though at the same time I know that
        people are probably \textit{not} going to read the introduction\footnote{Unless I put some cute art and interesting footnotes, maybe.}. 
        
        ~\\        
        I think that I will welcome you to my notes by addressing anxiety. 
        
        \paragraph{Belonging.}
        
        Unfortunately there is a lot of bias in STEM fields and over
        decades there has been a narrative that computer science is \textit{for}
        a certain type of person - antisocial, nerdy, people who started coding
        when they were 10 years old.

        Because of this, a lot of people who don't fit this description can
        be hesitant to get into computers or programming because they don't
        see people like themselves in media portrayals. Or perhaps previous
        professors or peers have acted like you're not a \textit{real programmer}
        if you didn't start programming as a child.
        
        \begin{center}
            \textbf{If you want to learn about coding, then you belong here.
            ~\\ 
            There are no prerequisites.}
        \end{center}
        ~\\
        
        \newpage
        
        I will say from my own experience, I know developers who fell in
        love with programming by accident as an adult after having to take
        a computer class for a \textit{different degree}. I know developers who
        are into all sorts of sports, or into photography, or into fashion.
        There is no specific ``type'' of programmer. You can be any religion,
        any gender, any color, from any country, and be a programmer.
        
        \begin{center}
            \includegraphics[width=0.65\textwidth]{Introduction/images/gatekeepingbaby}
            ~\\
            \footnotesize 
            Don't be like Gatekeeper Baby. 
            You can begin coding at any age!
            \normalsize
        \end{center}
        
        \paragraph{Challenge.}
        Programming can be hard sometimes. There are many aspects of learning
        to write software (or websites, apps, games, etc.) and you \textbf{will}
        get better at it with practice and with time. But I completely understand
        the feeling of hitting your head against a wall wondering \textit{why won't this work?!}
        and even wondering \textit{am I cut out for this?!} - Yes, you are.
        ~\\
        
        \begin{wrapfigure}{r}{0.45\textwidth}
            \vspace{-20pt}
                \begin{center}
                \includegraphics[width=0.42\textwidth]{Introduction/images/justwork}
                \end{center}
            \vspace{-10pt}
        \end{wrapfigure}
        
        I will tell you right now, I \textit{have} cried over programming assignments,
        over work, over software. I have taken my laptop with me to a family
        holiday celebration because I \textit{couldn't figure out this program}
        and I \textit{had to get it done!!} ~\\
        
        All developers struggle. Software is a really unique field. It's very
        intangible, and there are lots of programming languages, and all sorts
        of tools, and various techniques. Nobody knows everything, and there's
        always more to learn.
        
        \begin{center}
        Just because something is \textit{hard} doesn't mean that it's \textit{impossible}.
        \end{center}
        
        It's completely natural to hit roadblocks. To have to step away from
        your program and come back to it later with a clear head. It's natural
        to be confused. It's natural to \textit{not know}. ~\\
        
        But some skills you will learn to make this process smoother are
        how to plan out your programs, test and verify your programs,
        how to phrase what you don't know as a question, how to ask for help.
        These are all skills that you will build up over time.
        Even if it feels like you're not making progress, I promise that you are,
        and hopefully at the end of our class you can look back to the start of it
        and realize how much you've learned and grown.

        \paragraph{Learning.}
        First and foremost, I am here to help you \textbf{learn}. ~\\
        
        My teaching style is influenced on all my experiences throughout
        my learning career, my software engineer career, and my teaching career.
        
        
        I have personally met teachers who have tried to \textit{scare me away}
        from computers, I've had teachers who really encouraged me, 
        I've had teachers who barely cared, I've had teachers who made
        class really fun and engaging.
        
        I've worked professionally in software and web development, and
        independently making apps and video games. I know what it's like
        to apply for jobs and work with teams of people and experience
        a software's development throughout its lifecycle.
        
        And as a teacher I'm always trying to improve my classes -
        making the learning resources easily available and accessible,
        making assignments help build up your knowledge of the topics,
        trying to give feedback to help you design and write good programs.
        ~\\
    
        As a teacher, I am not here to trick you with silly questions
        or decide whether you're a \textit{real programmer} or not;
        I am here to help guide you to learn about programming,
        learn about design, learn about testing, and learn how to teach yourself.
        
        \begin{center}
            \includegraphics[width=5cm]{Introduction/images/buildingblocks.png}
        \end{center}
        
    \section{Roadmap}
    
        \begin{center}
            \includegraphics[width=\textwidth]{Introduction/images/roadmap.png}
        \end{center}
    
        When you're just starting out, it can be hard to know what all
        you're going to be learning about. I have certainly read course
        descriptions and just thought to myself ``I have no idea what
        any of that meant, but it's required for my degree, so I guess
        I'm taking it!'' ~\\
        
        Here's kind of my mental map of how the courses I teach work:
        
        \begin{itemize}
            \item   \textbf{CS 200: Concepts of Programming with C++} ~\\
                    You're learning the language. Think of it like actually
                    learning a human language; I'm teaching you words and
                    the grammar, and at first you're just parroting what I say,
                    but with practice you'll be able to build your own sentences.
                    
            \item   \textbf{CS 235: Object-Oriented Programming with C++} ~\\
                    You're learning more about software development practices,
                    design, testing, as well as more advanced object oriented concepts.
            
            \item   \textbf{CS 250: Basic Data Structures with C++} ~\\
                    You're learning about data, how to store data, how to
                    assess how efficient algorithms are. Data data data.
        \end{itemize}
        
        In addition to learning about the language itself, I also try to
        sprinkle in other things I've learned from experience that I think
        you should know as a software developer (or someone who codes for
        whatever reason), like
        
        \begin{itemize}
            \item   How do you validate that what you wrote \textit{actually works?}
                    (Spoilers: How to write tests, both manual and automated.)
            \item   What tools can you use to make your programming life easier?
                    (And are used in the professional world?)
            \item   How do you design a solution given just some requirements?
            \item   How do you network in the tech field?
            \item   How do you find jobs, even?
            \item   What are some issues facing tech fields today?
        \end{itemize}
        
        Something to keep in mind is that, if you're studying \textbf{Computer Science}
        as a degree (e.g., my Bachelor's degree is in Computer Science),
        technically that field is about \textit{``how do computers work?''},
        not about \textit{``how do I write software good?''\footnote{I know that's not grammatical but I live on the internet and this is basically what I would tweet.}}, 
        but I still find these topics important to go over.
        
        \begin{center}
            \includegraphics[width=0.75\textwidth]{Introduction/images/search.png}
        \end{center}
        
        \hrulefill
        
        That's all I can really think of to write here.
        If you have any questions, let me know. Maybe I'll
        add on here.
        
        %\newpage
%\begin{lstlisting}[style=pycode]
%print( "Hello World" )
%\end{lstlisting}

%\begin{lstlisting}[style=code]
%#include <iostream>
%using namespace std;

%int main()
%{
    %cout << "Hello World" << endl;

    %return 0;
%}
%\end{lstlisting}

%\begin{lstlisting}[style=cscode]
%class Program
%{
    %static void Main(string[] args)
    %{
      %Console.WriteLine("Hello World!");    
    %}
%}
%\end{lstlisting}

%\begin{lstlisting}[style=javacode]
%public class Main {
    %public static void main(String[] args) {
        %System.out.println("Hello World");
    %}
%}
%\end{lstlisting}


