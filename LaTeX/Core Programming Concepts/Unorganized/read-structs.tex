\newpage
\section{Structs}

Structs are a way we can group related data together in one place.
Creating a struct lets us basically create our own \textbf{data types},
which we can then use to create variables of that type.
~\\

For example, let's say we're writing a program that stores information
about a building and the building has multiple rooms. We could then
define a \textbf{Room} as an object with a \textbf{width}, \textbf{length},
and \textbf{height}, as well as other helpful data like perhaps a room name.

\begin{center}
  \includegraphics[width=10cm]{Unorganized/structs-images/room.png}
\end{center}

We can \textbf{declare a struct} that contains three floats, 
representing the width, length, and height, and if we want a name
for the room it can be a string.

\begin{lstlisting}[style=code]
struct Room
{
  float width;
  float length;
  float height;
  string name;
}; // MUST HAVE ; HERE!
\end{lstlisting}

\newpage
After the Room struct is created, we can then declare variables
whose data types are Rooms elseware in the program, such as in main():

\begin{lstlisting}[style=code]
Room bathroom;
Room bedroom;
Room livingroom;
\end{lstlisting}

And we can modify its ``internal'' variables (known as \textbf{member variables})
by using the \textbf{dot operator .}:

\begin{lstlisting}[style=code]
bathroom.width = 10;
bathroom.length = 20;
bathroom.height = 9;
bathroom.name = "Bathroom";
\end{lstlisting}

Beyond belonging to the ``bathroom'' variable, the width, length, height,
and name member variables are just normal variables and can be used
as you would use any other variable.

\begin{lstlisting}[style=code]
cout << "Enter the width of the " 
     << bedroom.name << ": ";
cin >> bedroom.width;
\end{lstlisting}

\subsection{Adding a Struct in your program}

Whenever you write a Struct for your program, the standard coding style
is to give each Struct (or family of structs) its own \textbf{header file},
a C++ source file that ends in .h. 

All .h files need to have the following to start with:

\begin{lstlisting}[style=code]
#ifndef _LABEL
#define _LABEL

// code goes in here

#endif
\end{lstlisting}

Where \texttt{\_LABEL} should be unique labels for each .h file. Often,
people use the same name as the file itself, but in all caps, and maybe
ending with \texttt{\_H} as well.

\newpage
\begin{lstlisting}[style=code]
#ifndef _ROOM_H
#define _ROOM_H

#include <string>
using namespace std;

struct Room
{
  float width;
  float length;
  float height;
  string name;
}; // MUST HAVE ; HERE!

#endif
\end{lstlisting}

These are known as \textbf{file guards} and their purpose is so that
the C++ compiler doesn't read the same .h file more than once,
which it will do if multiple .cpp files are \#including it and you
don't have file guards.
~\\

Additionally, since this Room struct contains a \textbf{string},
we need to include the string library here. Any code file that has
strings needs to include the string library, and similarly if your
file contains \texttt{cout} statements (iostream) or has \texttt{ofstream}
and \texttt{ifstream} types (fstream).
~\\

Back in \texttt{main.cpp}, you can now add an \#include for your header file
in order to use it within your code in main(). When including files that are
\textit{in your project}, you will use double quotes instead of $<$ and $>$:

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;
#include "Room.h"

int main()
{
  Room myRoom;
  cout << "Enter name for room: ";
  getline( cin, myRoom.name );
  // ... etc ...

  return 0;
}
\end{lstlisting}
