

    Most programs feature interactivity in some way or another.
    This can be moving a mouse around and clicking on things,
    tapping on a touch screen, using a gamepad joysticks and buttons,
    but we're going to be mostly interfacing with our C++ programs
    through keyboard input to our programs.
    Feedback from our C++ programs will come in the form of text
    output to the screen as well. We are writing terminal programs.
    \footnote{I know it's not very exciting but it's simple and keeps
    us focused on learning core programming concepts instead of,
    say, designing graphical user interfaces.}
    
    \begin{center}
     \includegraphics[width=8cm]{Basics/images/terminal}
    \end{center}
    
    We will also get into reading from and writing to text files
    (or other file formats) as well later on, but for now let's
    focus on the terminal/console.
    
    \begin{center}
     \includegraphics[width=4cm]{Basics/images/terminal-hi}
    \end{center}
    
    \newpage
    \section{Outputting information with \texttt{cout}}
    
        The \texttt{cout} command (I say it as ``c-out'' for ``console-out'')
        is used to write information to the screen. This can be outputting
        a string literal...
        
\begin{lstlisting}[style=code]
cout << "Hello, world!" << endl;
\end{lstlisting}

        ... a variable value...
        
\begin{lstlisting}[style=code]
cout << yourName << endl;
\end{lstlisting}
        
        ... or stringing multiple things together...
        
\begin{lstlisting}[style=code]
cout << "Hello, " << yourName << "!" << endl;
\end{lstlisting}

            \begin{intro}{cout formats}
                \begin{enumerate}
                    \item   \texttt{cout << VARIABLENAME; }
                    \item   \texttt{cout << VARIABLENAME << endl; }
                    \item   \texttt{cout << "string literal"; }
                    \item   \texttt{cout << "string literal" << endl; }
                    \item   \texttt{cout << "string literal" << VARIABLENAME; }
                    \item   \texttt{cout << endl; }
                \end{enumerate}
                
                ~\\ (You can combine any of these together)
            \end{intro}
            

        In C++, we use the \textbf{output stream operator} \texttt{<<}
        to string together multiple things to our output.
        
        \subsection{Newlines with \texttt{endl}}
        The \texttt{endl} command stands for ``end-line'', and will
        make sure that there's a vertical space between that cout statement
        and the next one. ~\\
        
        For example, if we write two \texttt{cout} statements like this:
        
\begin{lstlisting}[style=code]
cout << "Hello";
cout << "World";
\end{lstlisting}

        we don't have any \texttt{endl} parts, so the output will be this:
        
\begin{lstlisting}[style=output]
HelloWorld
\end{lstlisting} ~\\

        If we want to separate them on two different lines, instead we
        can write:
    
\begin{lstlisting}[style=code]
cout << "Hello" << endl;
cout << "World";
\end{lstlisting}

        And our output will be:
        
\begin{lstlisting}[style=output]
Hello
World
\end{lstlisting} ~\\

        Remember that C++ ends a statement with a semicolon \texttt{;}
        so you can make your \texttt{cout} statement span multiple
        lines, as long as you're chaining items together with
        \texttt{<<} and only adding \texttt{;} on the last line:

\begin{lstlisting}[style=code]
cout    << "Name:   " << name
        << "Age:    " << age
        << "State:  " << state << endl;
\end{lstlisting}

\newpage
    \section{Inputting information with \texttt{cin}}
    
        When we want to have the user enter a value for a variable
        via the keyboard, we use the \texttt{cin} command (``c-in'' or
        ``console-in'').
        
        \subsection{cin $>>$ var;}
            For variables like ints and floats, you will always use
            this format to store data from the keyboard into the variable:

            \begin{intro}{cin formats}
                \begin{enumerate}
                    \item   \texttt{cin >> VARIABLENAME;} ~\\
                            This will read input up until a space or a new line into the variable.
                    \item   \texttt{cin >> VARIABLENAME1 >> VARIABLENAME2 >> ETC;} ~\\
                            You can chain cin statements together to read multiple values for
                            multiple variables. Values are separated by spaces. ~\\
                            For example, for \texttt{cin >> a >> b;}, if I typed
                            \texttt{ hello there }, then \texttt{a} would have
                            ``hello'' and \texttt{b} would have ``there''.
                \end{enumerate} \vspace{0.3cm}
            \end{intro}
            
            The \texttt{cin} command uses the \textbf{input stream operator}
            \texttt{>>}, going from the \texttt{cin} to the variable.
            
            \paragraph{Strings and \texttt{cin >>}}
            
            If you're going to use \texttt{cin >>} with a string variable,
            keep in mind that it will only read up until the first whitespace
            character - this means you can't capture any spaces or tabs
            or newlines in a string variable doing this.
            
            ~\\ Here's a simple program that gets the user's name:
\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
    string name;
    cout << "Enter name: ";
    cin >> name;

    cout << "Your name is: " << name << endl;

    return 0;
}
\end{lstlisting}

            ~\\ And the output is: 
\begin{lstlisting}[style=output]
Enter name: Rachel Singh
Your name is: Rachel
\end{lstlisting}

            This works fine if we just want to capture one word at a time
            and not an entire sentence or multiple words with spaces.
            If we want to capture spaces, we need to use a different function.
             
            \begin{center}
             \includegraphics[width=\textwidth]{Basics/images/message}
            \end{center}
            
        
        \newpage
        \subsection{getline( cin, var );}
        
            You can use the \textbf{getline} function but \underline{only with strings}.
            These will get an entire line of text and store it in some string,
            where the line of text ends at the first new-line (when the user hits ENTER).            
            
            ~\\ Here's the program updated with getline:
\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
    string name;
    cout << "Enter name: ";
    getline( cin, name );

    cout << "Your name is: " << name << endl;

    return 0;
}
\end{lstlisting}

            ~\\ And the output is: 
\begin{lstlisting}[style=output]
Enter name: Rachel Singh
Your name is: Rachel Singh
\end{lstlisting}
        
        \subsection{Mixing \texttt{cin >> var;} and \texttt{getline( cin, var );}}
        
            The \texttt{cin >> var;} format of input and the \texttt{getline( cin, var );}
            format of input work differently from each other, and it causes
            a small issue that can be confusing if you don't know about it.
            
            \paragraph{Hidden text:} When you use \texttt{cin >> var;} and
            hit ENTER, a secret final character is stored in the input buffer
            of \texttt{cin}: \texttt{'\textbackslash n'} . The \texttt{\textbackslash n} character is
            an \textbf{escape sequence} that means newline (it's the same as
            \texttt{endl}).
            
            Anyway, because that \texttt{\textbackslash n} is still in the input buffer
            after using \texttt{cin >> var;}, it causes the \texttt{getline( cin, var );}
            function to read that \texttt{\textbackslash n} and ignore the next 
            thing the user types in.
            
            \paragraph{Workaround:}
            
            In order to get around this buffer problem, what you have to do
            is add a \texttt{cin.ignore();} statement. But where?
            
            The \texttt{cin.ignore();} must go before the \texttt{getline( cin, var );}
            statement, but only if the input \textit{before that getline}
            was a \texttt{cin >> var;} statement.

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
  float price;
  cout << "Enter product price: ";
  cin >> price;
  
  string name;
  cout << "Enter product name: ";
  cin.ignore();
  getline( cin, name );

  cout << name << " costs " << price << " USD" << endl;

  return 0;
}
\end{lstlisting}

    \begin{center}
     \includegraphics[width=\textwidth]{Basics/images/cin}
     ~\\
     \footnotesize
     I just see cin as the trouble-maker, making problems for
     getline()...
     \normalsize
    \end{center}
    
    \newpage
    \section{Escape sequences}
        There are special characters that you can use in your
        \texttt{cout} statements as well, which mean certain things:
        
        \paragraph{\texttt{\textbackslash n}: newline}
        
        The newline charcter lets you insert a newline inside of a string
        literal, instead of having to end the string literal and add an \texttt{endl}
        to your string:
        
        ~\\ Example:
\begin{lstlisting}[style=code]
cout << "hello\nworld" << endl;
\end{lstlisting}

        ~\\ Output:
\begin{lstlisting}[style=output]
hello
world
\end{lstlisting}

        \paragraph{\texttt{\textbackslash t}: tab}
        
        This character lets you add a tab into your text:

        ~\\ Example:
\begin{lstlisting}[style=code]
cout << "A \t B \t C" << endl;
cout << "1 \t 2 \t 3" << endl;
\end{lstlisting}

        ~\\ Output:
\begin{lstlisting}[style=output]
A 	 B 	 C
1 	 2 	 3
\end{lstlisting}

        \paragraph{\texttt{\textbackslash "}: double-quote}
        
        Because we use the \texttt{"} character when writing out string literals
        we need to use the escape sequence
        \texttt{\textbackslash "} to actually \textit{display} a double-quote.
 
        ~\\ Example:
\begin{lstlisting}[style=code]
cout << "He said \"Hi!\" to me!" << endl;
\end{lstlisting}

        ~\\ Output:
\begin{lstlisting}[style=output]
He said "Hi!" to me!
\end{lstlisting}

    % math library
    % random numbers
    % escape sequences
    
