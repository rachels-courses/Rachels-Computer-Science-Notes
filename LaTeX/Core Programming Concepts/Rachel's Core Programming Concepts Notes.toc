\contentsline {chapter}{\numberline {1}Introduction}{5}%
\contentsline {section}{\numberline {1.1}Welcome}{5}%
\contentsline {paragraph}{Belonging.}{5}%
\contentsline {paragraph}{Challenge.}{6}%
\contentsline {paragraph}{Learning.}{7}%
\contentsline {section}{\numberline {1.2}Roadmap}{8}%
\contentsline {chapter}{\numberline {2}Writing programs}{10}%
\contentsline {section}{\numberline {2.1}A brief history of software}{10}%
\contentsline {section}{\numberline {2.2}What is a program?}{12}%
\contentsline {paragraph}{Inputs:}{12}%
\contentsline {paragraph}{Outputs:}{12}%
\contentsline {paragraph}{Variables:}{12}%
\contentsline {paragraph}{Branching and looping:}{13}%
\contentsline {section}{\numberline {2.3}C++ Programs}{15}%
\contentsline {subsection}{main(): The starting point}{15}%
\contentsline {subsection}{Basic C++ syntax}{16}%
\contentsline {paragraph}{Lines of code:}{16}%
\contentsline {paragraph}{Variable names:}{16}%
\contentsline {paragraph}{Code blocks:}{16}%
\contentsline {paragraph}{Comments:}{17}%
\contentsline {paragraph}{Whitespace and code cleanliness:}{17}%
\contentsline {section}{\numberline {2.4}Diagnosing, testing, and debugging}{18}%
\contentsline {subsection}{Synax, Runtime, and Logic errors}{19}%
\contentsline {paragraph}{Syntax errors:}{19}%
\contentsline {paragraph}{Logic errors:}{19}%
\contentsline {paragraph}{Runtime errors:}{19}%
\contentsline {subsection}{Debugging}{20}%
\contentsline {paragraph}{Output here, there, everywhere:}{20}%
\contentsline {subsection}{Testing}{21}%
\contentsline {subsection}{Coding techniques for newbies}{21}%
\contentsline {chapter}{\numberline {3}Variables and Data Types}{22}%
\contentsline {section}{\numberline {3.1}Data types}{23}%
\contentsline {section}{\numberline {3.2}Declaring variables and assigning values to variables}{24}%
\contentsline {subsection}{Variable declaration}{24}%
\contentsline {subsection}{Variable assignment}{25}%
\contentsline {subsection}{Naming conventions}{26}%
\contentsline {subsection}{Unsigned data types}{26}%
\contentsline {subsection}{Modern C++: \texttt {auto}}{27}%
\contentsline {section}{\numberline {3.3}Basic operations on variables}{27}%
\contentsline {subsection}{Outputting variable values}{27}%
\contentsline {subsection}{Inputting variable values}{28}%
\contentsline {subsection}{Math operations}{28}%
\contentsline {paragraph}{Operations:}{28}%
\contentsline {paragraph}{Make sure to put the result somewhere!}{28}%
\contentsline {paragraph}{Compound operations:}{29}%
\contentsline {subsection}{String operations}{29}%
\contentsline {paragraph}{Concatenating strings:}{29}%
\contentsline {paragraph}{Letter-of-the-string:}{29}%
\contentsline {section}{\numberline {3.4}Named constants}{30}%
\contentsline {paragraph}{Named constant naming convention:}{30}%
\contentsline {chapter}{\numberline {4}Input and Output}{31}%
\contentsline {section}{\numberline {4.1}Outputting information with \texttt {cout}}{32}%
\contentsline {subsection}{Newlines with \texttt {endl}}{32}%
\contentsline {section}{\numberline {4.2}Inputting information with \texttt {cin}}{34}%
\contentsline {subsection}{cin $>>$ var;}{34}%
\contentsline {paragraph}{Strings and \texttt {cin >>}}{34}%
\contentsline {subsection}{getline( cin, var );}{36}%
\contentsline {subsection}{Mixing \texttt {cin >> var;} and \texttt {getline( cin, var );}}{36}%
\contentsline {paragraph}{Hidden text:}{36}%
\contentsline {paragraph}{Workaround:}{36}%
\contentsline {section}{\numberline {4.3}Escape sequences}{38}%
\contentsline {paragraph}{\texttt {\textbackslash n}: newline}{38}%
\contentsline {paragraph}{\texttt {\textbackslash t}: tab}{38}%
\contentsline {paragraph}{\texttt {\textbackslash "}: double-quote}{38}%
\contentsline {chapter}{\numberline {5}Control Flow}{39}%
\contentsline {section}{\numberline {5.1}Boolean Expressions}{40}%
\contentsline {subsection}{Introduction}{40}%
\contentsline {subsection}{And, Or, and Not operators}{42}%
\contentsline {paragraph}{Not operator: \texttt {!}}{42}%
\contentsline {paragraph}{And operator: \texttt {\&\&}}{42}%
\contentsline {paragraph}{Or operator: \texttt {||}}{43}%
\contentsline {subsection}{Truth tables}{43}%
\contentsline {paragraph}{Truth table for NOT:}{43}%
\contentsline {paragraph}{Truth table for AND:}{44}%
\contentsline {paragraph}{Truth table for OR:}{45}%
\contentsline {subsection}{DeMorgan's Laws}{46}%
\contentsline {paragraph}{Opposite of \texttt {a \&\& b}:}{46}%
\contentsline {paragraph}{Opposite of \texttt {a || b}:}{47}%
\contentsline {subsection}{Summary}{47}%
\contentsline {section}{\numberline {5.2}Branching}{47}%
\contentsline {subsection}{If statements}{48}%
\contentsline {subsubsection}{If statements}{49}%
\contentsline {subsubsection}{If/Else statements}{51}%
\contentsline {subsubsection}{If/Else if/Else statements}{53}%
\contentsline {subsection}{Switch statements}{55}%
\contentsline {paragraph}{The default case}{55}%
\contentsline {paragraph}{The break; statement}{55}%
\contentsline {section}{\numberline {5.3}Loops}{57}%
\contentsline {subsection}{While loops}{57}%
\contentsline {paragraph}{Example: Counting up}{58}%
\contentsline {paragraph}{Example: Validating user input}{58}%
\contentsline {paragraph}{Example: Program loop}{59}%
\contentsline {subsubsection}{continue}{59}%
\contentsline {subsubsection}{break}{60}%
\contentsline {subsection}{Do... while loops}{60}%
\contentsline {subsection}{For loops}{61}%
\contentsline {section}{\numberline {5.4}Nesting code blocks}{62}%
\contentsline {subsection}{Nesting if statements}{62}%
\contentsline {subsection}{Nesting loops}{63}%
\contentsline {chapter}{\numberline {6}Arrays}{66}%
\contentsline {section}{\numberline {6.1}Array basics}{66}%
\contentsline {subsection}{What are arrays?}{66}%
\contentsline {paragraph}{Declaring arrays:}{67}%
\contentsline {paragraph}{\underline {Array size} named constant:}{68}%
\contentsline {paragraph}{\underline {Element count} variable:}{68}%
\contentsline {paragraph}{Array data types:}{68}%
\contentsline {paragraph}{Valid indices:}{69}%
\contentsline {section}{\numberline {6.2}Accessing indices with variables}{70}%
\contentsline {section}{\numberline {6.3}Using for loops with arrays}{70}%
\contentsline {paragraph}{Initializing an array:}{70}%
\contentsline {paragraph}{Asking the user to enter all elements:}{71}%
\contentsline {paragraph}{Displaying all items in an array:}{71}%
\contentsline {section}{\numberline {6.4}Using multiple arrays}{72}%
\contentsline {section}{\numberline {6.5}Arrays as arguments}{72}%
\contentsline {section}{\numberline {6.6}Array management}{74}%
\contentsline {paragraph}{Clear array:}{74}%
\contentsline {paragraph}{Display all elements:}{74}%
\contentsline {paragraph}{Add new element to array:}{74}%
\contentsline {paragraph}{Deleting an element from an array}{76}%
\contentsline {paragraph}{Inserting an element into an array}{77}%
\contentsline {section}{\numberline {6.7}Multidimensional arrays}{77}%
\contentsline {section}{\numberline {6.8}C++ Standard Template Library: Vectors}{79}%
\contentsline {paragraph}{Declaring a vector:}{79}%
\contentsline {paragraph}{Adding data:}{80}%
\contentsline {paragraph}{Getting the size of the vector:}{80}%
\contentsline {paragraph}{Clearing the vector:}{80}%
\contentsline {paragraph}{Accessing elements by index:}{80}%
\contentsline {paragraph}{Iterating over a vector:}{80}%
\contentsline {chapter}{\numberline {7}Strings}{82}%
\contentsline {section}{\numberline {7.1}Strings as arrays}{83}%
\contentsline {subsection}{Subscript operator - get one char with [ ]}{83}%
\contentsline {section}{\numberline {7.2}String functionality}{84}%
\contentsline {subsection}{Size of the string}{84}%
\contentsline {paragraph}{Example: Outputting a string's length}{84}%
\contentsline {paragraph}{Example: Counting z's}{85}%
\contentsline {subsection}{Concatenating strings with +}{86}%
\contentsline {subsection}{Finding text with find()}{87}%
\contentsline {subsection}{Finding substrings with substr()}{88}%
\contentsline {subsection}{Comparing text with compare()}{89}%
\contentsline {subsection}{Inserting text into a string with insert()}{90}%
\contentsline {subsection}{Erasing a chunk of text with erase()}{91}%
\contentsline {subsection}{Replacing a region of text with replace()}{92}%
\contentsline {chapter}{\numberline {8}File Input and Output}{93}%
\contentsline {section}{\numberline {8.1}Output streams}{93}%
\contentsline {paragraph}{Different file types...}{95}%
\contentsline {section}{\numberline {8.2}Input streams}{96}%
\contentsline {subsection}{Reading an entire file}{97}%
\contentsline {paragraph}{Reading chunks of data:}{97}%
\contentsline {paragraph}{Reading lines of data:}{98}%
\contentsline {section}{\numberline {8.3}Saving and loading data}{98}%
\contentsline {subsection}{Parsing files}{98}%
\contentsline {chapter}{\numberline {9}Functions}{102}%
\contentsline {section}{\numberline {9.1}Program Structure}{102}%
\contentsline {paragraph}{Program flow and functions}{103}%
\contentsline {section}{\numberline {9.2}Function Basics}{104}%
\contentsline {subsection}{Uses of functions}{104}%
\contentsline {paragraph}{Functions for formulas}{104}%
\contentsline {paragraph}{Functions to validate}{105}%
\contentsline {paragraph}{Functions to get input}{107}%
\contentsline {paragraph}{Functions to output}{108}%
\contentsline {paragraph}{Other uses}{108}%
\contentsline {subsection}{Anatomy of a function}{109}%
\contentsline {paragraph}{Function Declaration}{109}%
\contentsline {paragraph}{Function Definition}{109}%
\contentsline {paragraph}{Function Header}{109}%
\contentsline {paragraph}{Function Parameters}{109}%
\contentsline {paragraph}{Function Return Type}{109}%
\contentsline {paragraph}{Function Body}{109}%
\contentsline {paragraph}{Function Call}{110}%
\contentsline {paragraph}{Function Arguments}{110}%
\contentsline {paragraph}{Common syntax errors with function calls}{111}%
\contentsline {section}{\numberline {9.3}Variable scope}{111}%
\contentsline {paragraph}{main():}{111}%
\contentsline {paragraph}{Variables declared within if statements and loops:}{112}%
\contentsline {paragraph}{Functions:}{112}%
\contentsline {paragraph}{Parameters:}{112}%
\contentsline {paragraph}{Same names?}{113}%
\contentsline {section}{\numberline {9.4}Parameters and arguments}{113}%
\contentsline {subsection}{Pass-by-value}{113}%
\contentsline {subsection}{Pass-by-reference}{115}%
\contentsline {paragraph}{Pass-by-reference instead of return}{116}%
\contentsline {paragraph}{Pass-by-reference of large things}{116}%
\contentsline {subsection}{Default parameters}{117}%
\contentsline {subsection}{Summary: Ways to pass data to/from functions}{118}%
\contentsline {section}{\numberline {9.5}Function Overloading}{118}%
\contentsline {chapter}{\numberline {10}Basic Object Oriented Programming}{120}%
\contentsline {section}{\numberline {10.1}Introduction to Object Oriented Programming}{120}%
\contentsline {subsection}{Programming paradigms}{120}%
\contentsline {paragraph}{Machine code:}{121}%
\contentsline {paragraph}{Procedural Languages:}{121}%
\contentsline {paragraph}{Object Oriented Programming:}{121}%
\contentsline {subsection}{Design ideals}{121}%
\contentsline {subsection}{Example: Moving to an OOP design}{123}%
\contentsline {subparagraph}{Cons of this style:}{123}%
\contentsline {paragraph}{Video game}{127}%
\contentsline {paragraph}{College}{128}%
\contentsline {section}{\numberline {10.2}Structs}{128}%
\contentsline {subsection}{Structs vs. Classes}{128}%
\contentsline {subsection}{Example structs}{129}%
\contentsline {subsection}{Declaring object variables}{131}%
\contentsline {section}{\numberline {10.3}Classes}{132}%
\contentsline {subsection}{Accessibility}{132}%
\contentsline {subsection}{Header and Source files}{132}%
\contentsline {subsubsection}{Defining member functions \textit {inside} the class declaration}{134}%
\contentsline {subsubsection}{Should you define functions in the .hpp or the .cpp?}{135}%
\contentsline {subsection}{Function vs. Method}{136}%
\contentsline {subsection}{Getters and Setters}{136}%
\contentsline {paragraph}{Setters:}{137}%
\contentsline {paragraph}{Getters:}{137}%
\contentsline {subsection}{Constructors and Destructors}{138}%
\contentsline {paragraph}{Example: Text file wrapper}{138}%
\contentsline {subsection}{Default Constructors}{141}%
\contentsline {subsection}{Parameterized Constructors}{142}%
\contentsline {subsection}{Copy Constructors}{143}%
\contentsline {paragraph}{Design:}{143}%
\contentsline {paragraph}{Default copy constructor:}{143}%
\contentsline {paragraph}{Ways to copy:}{144}%
\contentsline {subsection}{Example program: House Builder}{145}%
\contentsline {paragraph}{Room.hpp:}{146}%
\contentsline {paragraph}{Room.cpp:}{147}%
\contentsline {paragraph}{House.hpp:}{148}%
\contentsline {paragraph}{House.cpp:}{149}%
\contentsline {paragraph}{main.cpp:}{150}%
\contentsline {chapter}{\numberline {11}Intermediate Object Oriented Programming}{151}%
\contentsline {section}{\numberline {11.1}Const member methods}{151}%
\contentsline {section}{\numberline {11.2}this}{152}%
\contentsline {section}{\numberline {11.3}Friends}{153}%
\contentsline {paragraph}{Friend function:}{153}%
\contentsline {paragraph}{Friend class:}{153}%
\contentsline {paragraph}{However - it doesn't go both ways.}{154}%
\contentsline {section}{\numberline {11.4}Operator Overloading}{155}%
\contentsline {subsection}{Arithmetic operators}{155}%
\contentsline {subsection}{Relational operators}{156}%
\contentsline {subsection}{Stream operators}{157}%
\contentsline {subsection}{Subscript operator}{158}%
\contentsline {subsection}{Assignment operator}{158}%
\contentsline {section}{\numberline {11.5}Static Variables and Functions}{160}%
\contentsline {section}{\numberline {11.6}Inheritance}{162}%
\contentsline {subsection}{Design ideas}{162}%
\contentsline {subsubsection}{Reusing functionality}{162}%
\contentsline {subsubsection}{The iostream and fstream}{163}%
\contentsline {subsection}{Inheritance (Is-A) vs. Composition (Has-A)}{163}%
\contentsline {subsubsection}{Example: Game objects}{164}%
\contentsline {subsection}{Implementing inheritance}{167}%
\contentsline {subsection}{Inheriting from one parent}{167}%
\contentsline {subsection}{Inheriting from multiple parents}{168}%
\contentsline {subsubsection}{Private, Public, and Protected}{169}%
\contentsline {paragraph}{Member access}{169}%
\contentsline {paragraph}{Inheritance style}{169}%
\contentsline {subsubsection}{Function Overriding}{170}%
\contentsline {paragraph}{Calling the parents' version of a method}{171}%
\contentsline {subsubsection}{Constructors and Destructors in the family}{172}%
\contentsline {chapter}{\numberline {12}Advanced Object Oriented Programming}{174}%
\contentsline {section}{\numberline {12.1}Polymorphism}{174}%
\contentsline {subsection}{Design and polymorphism}{174}%
\contentsline {subsubsection}{Example: Quizzer and multiple question types}{175}%
\contentsline {subsubsection}{Other design considerations}{176}%
\contentsline {subsection}{Review: Class inheritance and function overriding}{177}%
\contentsline {subsection}{Review: Pointers to class objects}{177}%
\contentsline {subsection}{Which version of the method is called?}{178}%
\contentsline {paragraph}{No virtual methods - Which \texttt {AskQuestion()} is called?}{179}%
\contentsline {paragraph}{Virtual methods - Which \texttt {AskQuestion()} is called?}{180}%
\contentsline {subsection}{Virtual methods, late binding, and the Virtual Table}{181}%
\contentsline {paragraph}{The \texttt {virtual} keyword}{181}%
\contentsline {subsection}{When should we use \texttt {virtual}?}{182}%
\contentsline {paragraph}{Destructors should always be virtual.}{182}%
\contentsline {paragraph}{Constructors cannot be marked \texttt {virtual}.}{182}%
\contentsline {paragraph}{Not every function needs to be virtual.}{182}%
\contentsline {subsection}{Designing interfaces with pure virtual functions and abstract classes}{183}%
\contentsline {paragraph}{What is an Interface?}{183}%
\contentsline {paragraph}{Declarations:}{185}%
\contentsline {paragraph}{Definitions:}{186}%
\contentsline {paragraph}{Function calls:}{187}%
\contentsline {section}{\numberline {12.2}Templates}{188}%
\contentsline {subsection}{Before Templates}{188}%
\contentsline {subsection}{What are Templates?}{188}%
\contentsline {subsection}{Templated functions}{189}%
\contentsline {paragraph}{Calling the templated function:}{190}%
\contentsline {paragraph}{Program output:}{190}%
\contentsline {subsection}{Templated classes}{191}%
\contentsline {paragraph}{Example TemplatedArray (Full):}{192}%
\contentsline {paragraph}{Using the TemplatedArray}{196}%
\contentsline {chapter}{\numberline {13}All about \texttt {const}}{197}%
\contentsline {section}{\numberline {13.1}Named Constants}{197}%
\contentsline {section}{\numberline {13.2}Const Parameters}{198}%
\contentsline {paragraph}{Const pass-by-value:}{198}%
\contentsline {paragraph}{Const pass-by-reference:}{199}%
\contentsline {paragraph}{Const pointer passing:}{199}%
\contentsline {section}{\numberline {13.3}Const Returns}{200}%
\contentsline {section}{\numberline {13.4}Const Pointers}{200}%
\contentsline {paragraph}{Pointer to const integer:}{201}%
\contentsline {paragraph}{Const pointer to variable integer:}{201}%
\contentsline {section}{\numberline {13.5}Const Member Methods}{202}%
\contentsline {chapter}{\numberline {14}Pointers, memory management, and dynamic variables and arrays}{203}%
\contentsline {section}{\numberline {14.1}Memory Addresses}{204}%
\contentsline {section}{\numberline {14.2}Pointers}{207}%
\contentsline {subsection}{Creating pointer variables}{207}%
\contentsline {subsection}{Assigning pointers to addresses}{208}%
\contentsline {subsection}{Dereferencing pointers to get values}{210}%
\contentsline {subsection}{Pointer cheat sheet}{211}%
\contentsline {subsection}{Invalid memory access with pointers}{212}%
\contentsline {paragraph}{How do we check whether memory address is valid?}{212}%
\contentsline {section}{\numberline {14.3}Dynamically allocating memory}{212}%
\contentsline {subsection}{Dynamic variables}{212}%
\contentsline {subsubsection}{new and delete}{213}%
\contentsline {paragraph}{Allocating memory for a single item:}{213}%
\contentsline {paragraph}{Deallocating memory for a single item:}{213}%
\contentsline {subsection}{Dynamic arrays}{214}%
\contentsline {subsection}{new[ ] and delete[ ]}{214}%
\contentsline {paragraph}{Allocate memory for an array:}{214}%
\contentsline {paragraph}{Deallocate memory for an array:}{214}%
\contentsline {subsubsection}{``resizing'' a dynamic array}{215}%
\contentsline {paragraph}{Resizing steps}{216}%
\contentsline {subsection}{Dynamic array example: Movie Library}{217}%
\contentsline {paragraph}{MovieLibrary.hpp:}{217}%
\contentsline {paragraph}{MovieLibrary.cpp:}{218}%
\contentsline {subparagraph}{Constructor:}{218}%
\contentsline {subparagraph}{Destructor:}{218}%
\contentsline {subparagraph}{ViewAllMovies:}{219}%
\contentsline {subparagraph}{ClearAllMovies:}{219}%
\contentsline {subparagraph}{IsFull:}{219}%
\contentsline {subparagraph}{AddMovie:}{220}%
\contentsline {subparagraph}{Resize:}{221}%
\contentsline {subsection}{STL Vectors and Lists}{222}%
\contentsline {subsubsection}{Vector - A dynamic array}{222}%
\contentsline {paragraph}{Pros and Cons:}{222}%
\contentsline {subsection}{List - A linked structure}{223}%
\contentsline {paragraph}{Pros and Cons:}{223}%
\contentsline {section}{\numberline {14.4}Memory Management}{224}%
\contentsline {subsection}{Types of memory errors}{224}%
\contentsline {paragraph}{Invalid memory access:}{224}%
\contentsline {paragraph}{Memory leak:}{224}%
\contentsline {paragraph}{Missing allocation:}{224}%
\contentsline {subsection}{The Stack and the Heap}{224}%
\contentsline {subsubsection}{The Stack}{225}%
\contentsline {subsubsection}{The Heap}{225}%
\contentsline {chapter}{\numberline {15}Exception handling with try/catch}{226}%
\contentsline {section}{\numberline {15.1}Writing robust software}{226}%
\contentsline {paragraph}{What kind of errors can we listen for?}{228}%
\contentsline {section}{\numberline {15.2}The C++ Exception object}{229}%
\contentsline {paragraph}{Exceptions:}{229}%
\contentsline {section}{\numberline {15.3}Detecting errors and throwing exceptions}{230}%
\contentsline {section}{\numberline {15.4}Listening for exceptions with try}{230}%
\contentsline {section}{\numberline {15.5}Dealing with exceptions with catch}{231}%
\contentsline {paragraph}{Handling the error:}{231}%
\contentsline {paragraph}{Coding with others' code:}{231}%
\contentsline {chapter}{\numberline {16}Algorithm Efficiency}{232}%
\contentsline {section}{\numberline {16.1}Introduction: Algorithm efficiency (and why we care)}{232}%
\contentsline {paragraph}{Processing power}{232}%
\contentsline {paragraph}{And some computers today are slow.}{233}%
\contentsline {paragraph}{Let's say our algorithm's time to execute increases \textit {quadratically}.}{233}%
\contentsline {paragraph}{From a design standpoint}{234}%
\contentsline {paragraph}{As an example,}{234}%
\contentsline {paragraph}{Fibonacci sequence, iterative:}{235}%
\contentsline {paragraph}{Fibonacci sequence, recursive:}{235}%
\contentsline {section}{\numberline {16.2}Big-O Notation and Growth Rates}{237}%
\contentsline {paragraph}{Constant time, $O(1)$}{238}%
\contentsline {paragraph}{Logarithmic time, $O(log(n))$}{238}%
\contentsline {paragraph}{Linear time, $O(n)$}{239}%
\contentsline {paragraph}{Quadratic time, $O(n^2)$}{239}%
\contentsline {paragraph}{Cubic time, $O(n^3)$}{240}%
\contentsline {paragraph}{Exponential time, $O(2^n)$}{240}%
\contentsline {paragraph}{Growth rate comparisons:}{241}%
\contentsline {chapter}{\numberline {17}Recursion}{242}%
\contentsline {section}{\numberline {17.1}Examples of recursion in math}{243}%
\contentsline {paragraph}{Summation:}{243}%
\contentsline {paragraph}{Factorials:}{245}%
\contentsline {section}{\numberline {17.2}Recursion in programming}{246}%
\contentsline {subsection}{Recursion basics}{247}%
\contentsline {subsection}{Breaking down problems into recursive solutions}{248}%
\contentsline {paragraph}{Summation:}{248}%
\contentsline {subparagraph}{Solution for recursive summation:}{249}%
\contentsline {paragraph}{Example - Draw a line:}{249}%
\contentsline {paragraph}{Counting Up:}{250}%
\contentsline {subparagraph}{Solution for recursive count up:}{251}%
\contentsline {subsection}{A case for recursion}{251}%
\contentsline {subsubsection}{Searching a File System}{251}%
\contentsline {subsubsection}{Solving a maze}{254}%
\contentsline {chapter}{\numberline {18}The Standard Template Library}{255}%
\contentsline {chapter}{\numberline {19}Testing}{256}%
\contentsline {section}{\numberline {19.1}Introduction: Validating your work}{256}%
\contentsline {paragraph}{Test case:}{257}%
\contentsline {subsection}{The role of testing in software development}{258}%
\contentsline {section}{\numberline {19.2}Designing tests}{258}%
\contentsline {subsection}{Test cases}{258}%
\contentsline {section}{\numberline {19.3}Manual testing}{260}%
\contentsline {section}{\numberline {19.4}Automated unit tests}{261}%
\contentsline {subsection}{Unit tests in the real world}{261}%
\contentsline {subsubsection}{Fixing errors with failing tests}{262}%
\contentsline {subsection}{Other automated tests}{264}%
\contentsline {chapter}{\numberline {20}New sections (CS200)}{265}%
\contentsline {section}{\numberline {20.1}Enumerations}{265}%
\contentsline {subsection}{Traditional enums}{265}%
\contentsline {paragraph}{Storing categories with strings:}{265}%
\contentsline {paragraph}{Storing categories with numbers:}{266}%
\contentsline {paragraph}{OK, so what is better, then??}{266}%
\contentsline {subsubsection}{More info about enums}{267}%
\contentsline {paragraph}{Declaring an enum variable:}{267}%
\contentsline {paragraph}{Numbers can be assigned in any order:}{267}%
\contentsline {paragraph}{Numbers not required:}{268}%
\contentsline {paragraph}{Outputting an enum gives you a number:}{268}%
\contentsline {subsection}{Enum classes (Since C++11)}{268}%
\contentsline {section}{\numberline {20.2}Namespaces}{269}%
\contentsline {section}{\numberline {20.3}Interfaces}{270}%
\contentsline {paragraph}{Virtual functions:}{270}%
\contentsline {paragraph}{Pure virtual functions:}{270}%
\contentsline {paragraph}{Not defining the functions?}{271}%
\contentsline {section}{\numberline {20.4}Structs}{272}%
\contentsline {subsection}{Adding a Struct in your program}{273}%
\contentsline {chapter}{\numberline {21}New sections (CS250)}{275}%
\contentsline {section}{\numberline {21.1}Searching}{275}%
\contentsline {paragraph}{When we're searching for items in an \underline {unsorted} linear structure}{275}%
\contentsline {paragraph}{OK, but what if the structure \textit {is} sorted?}{276}%
\contentsline {paragraph}{Example}{278}%
\contentsline {paragraph}{Step 1:}{278}%
\contentsline {paragraph}{Step 2:}{278}%
\contentsline {paragraph}{Step 3:}{278}%
\contentsline {section}{\numberline {21.2}Sorting}{279}%
\contentsline {subsection}{Bubble Sort}{280}%
\contentsline {subsection}{Insertion Sort}{281}%
\contentsline {subsection}{Selection Sort}{282}%
\contentsline {subsection}{Merge Sort}{283}%
\contentsline {subsection}{Quick Sort}{285}%
