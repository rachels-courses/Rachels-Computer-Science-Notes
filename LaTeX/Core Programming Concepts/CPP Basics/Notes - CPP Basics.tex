\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Variables}
\newcommand{\laTitle}       {Rachel's Computer Science Notes}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{Introduction to C++}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont Core Computer Science Notes:}
                {\fontsize{2cm}{3cm}\selectfont Introduction/C++ Basics}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{../images/Cuties/terminal-hi.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                An overview compiled by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{../images/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}
    
	\tableofcontents

    \chapter{Basic C++ Programs} %------------------------------------%
    
    In C++, every program begins at the \texttt{main()} \textbf{function}.

\begin{lstlisting}[style=code]
int main()
{
	// Program code goes here

	return 0;
}
\end{lstlisting}

	We will eventually be writing our own functions, but to start out with
	we will be putting all of our program logic within \texttt{main},
	after the opening curly-brace \{ and before the \texttt{return 0;}.
	
	\paragraph{int main():} ``main'' is the name of the function,
	and every C++ program begins at the main function. If you named
	this something else, C++ would complain because it wouldn't know
	where to start! The main has a return type ``int'', which means that
	the program expects some integer to be returned at the end.
	
	\paragraph{\{ \}}: The opening and closing curly braces in C++ (and
	similar languages like Java, C\#, and JavaScript) denote the beginning
	and ending of a \textbf{code-block} - some code that belongs together.
	
	\paragraph{return 0;:} This returns a ``normal status code'' of 0.
	In older software, we'd use number codes to represent different errors,
	with 0 being ``no errors''. Since main has an ``int'' return type,
	we must return 0 at the end.
	
	~\\
	This might not make perfect sense to you right off the bat. Sometimes,
	you need to take certain things for granted, and you will learn about them
	more in-depth as you learn more about C++.
	
	\section{Comments} %----------------------------------------------%
	In programming, it is often useful to add \textbf{comments} to your code.
	The computer doesn't use these comments at all, but it is useful for
	other programmers (or for yourself in the future) to be able to
	see comments to understand what's going on.
	
	\paragraph{Single-line comments:} You can write a single-line comment like this:
	
\begin{lstlisting}[style=code]
// The following gets the distance between 2 points
\end{lstlisting}

	\paragraph{Multi-line comments:} You can also add comments on multiple
	lines like this:
	
\begin{lstlisting}[style=code]
/*
My program
By me
This program calculates distances
*/
\end{lstlisting}

    \section{Variables} %------------------------------%
    
    In a basic C++ program, you will usually at least have \textbf{variables}
    in order to store data. If you're not storing data and processing that
    data, then what's the point of writing a program?
    
    \paragraph{Declaring a variable:}
    In C++, when you want to start using a new variable, you must
    \textbf{declare} it first. At minimum, you need to specify the
    variable's \textbf{data type} (what kind of data it stores) and
    the variable's \textbf{name or identifier} (what you call the variable).
    
\begin{lstlisting}[style=code]
int candiesPerKid;
\end{lstlisting}

	The first item (int) is the \textbf{data type} and the second item
	(candiesPerKid) is the \textbf{variable name}.
	
	\paragraph{Variable names:}
	Variable names in C++ can consist of \textbf{letters (upper and lower case)},
	\textbf{numbers} (except it cannot start with a number), and \textbf{underscores}.
	Other symbol types cannot be used, and certain words cannot be used if they're
	special keywords in C++ (for example, ``if'' is a keyword, so it can't be a variable name.)
	
	~\\
	In C++, variable names are generally written in camelcase, with the first
	letter staying lower-case:
	
\begin{lstlisting}[style=code]
int howManyDays;
float howMuchMoney;
string studentName;
\end{lstlisting}
	
	\paragraph{Initializing a variable:}
	The first time you assign a value to a variable it is known as
	\textbf{initializing} it. You can declare a variable and initialize
	it later on...
    
\begin{lstlisting}[style=code]
int candiesPerKid;
// stuff happens
candiesPerKid = 2;
\end{lstlisting}
	~\\
	Or you can declare a variable and initialize it at the same time...
    
\begin{lstlisting}[style=code]
int candiesPerKid = 2;
\end{lstlisting}

	\paragraph{Variables and memory:}
	
	When a variable is declared, it's going to take up a little space
	in RAM - in memory. When variables aren't used anymore, they will
	be freed for other programs to use.

	\paragraph{Garbage values:}
	C++ doesn't automatically initialize your variables with information.
	If you declare a variable and \textit{never initialize it}, there is
	no way to know what value that variable has. In other words,
	it will store ``garbage'': data it pulls from memory, any sort of
	``old data'' that was used by a previous program or variable that
	has since stopped using that address.
	
	\subsection{Named constants}
	In many cases, we will want to use \textbf{named constants} that
	look like and behave like variables, but whose value cannot be changed
	after declaration. This is useful when we're going to use a number a
	lot of times (e.g., ``maxStudents = 30'') but we don't want to hard-code
	the number 30 all through our program. Named constants are declared
	like variables, but use the \texttt{const} keyword at the beginning,
	and must be initialized with a value right away:
	
\begin{lstlisting}[style=code]
const int MAX_SEATS_IN_CAR = 5;
const string CURRENCY = "USD";
\end{lstlisting}

	This also helps in that, if we want to change the number later on,
	we only have to update it in \underline{one location} - where the
	const is being declared.
	
	~\\
	Name constants usually are named with ALL\_UPPER\_CASE\_LETTERS,
	with underscores between each word.
	
	\paragraph{Example:} ~\\
	

\begin{lstlisting}[style=code]
const int CANDIES_PER_KID = 2;

int kids;
cout << "How many kids? ";
cin >> kids;

int totalCandies = kids * CANDIES_PER_KID;

cout << "Please order " << totalCandies << " candies." << endl;
\end{lstlisting}
	
	\newpage
    \subsection{Data types}
    When declaring a variable, we must specify what \textbf{data type}
    it is: what kind of data it will store. In some languages like Python,
    a variable can store integers one minute then strings the next -
    this is not possible in C++. Once a variable is declared, it can only
    store data of the same type.
    
    \begin{center}
    \begin{tabular}{p{3cm} p{2cm} p{6cm} }
		\textbf{Data type} 	& \textbf{Size} 		& 	\textbf{Description} \\ \hline
		Booleans			& 1 byte				& 	Stores \texttt{true} or \texttt{false}.
		
														\texttt{bool savedGame = true;}
														
														\texttt{bool programDone = false;}
														
		\\ \hline
		Characters			& 1 byte				& 	Stores a single character. Char literals must be in single quotes.
		
														\texttt{char currency = '\$';}
														
														\texttt{char answer='y';}
		\\ \hline
		Integers			& 2 bytes				& 	Stores whole numbers (positive/negative/zero).
		
														\texttt{int totalStudents = 10;}
														
														\texttt{int year = 1980;}
		\\ \hline
		Floats				& 4 bytes				& 	Stores numbers with a decimal component.
		
														\texttt{float price = 9.99;}
														
														\texttt{float ratio = 0.50;}
		\\ \hline
		Doubles				& 8 bytes				& 	Same as a \texttt{float}, but double the precision.
		\\ \hline
		Strings				& (variable)			& 	Stores any kind of data. String literals must be within double quotes.
		
														\texttt{string name = "JCCC";}
														
														\texttt{string area = "(913)";}
		
    \end{tabular}
    \end{center}
    
    \newpage
    
    \section{Input and Output} %--------------------------------------%
    Some programs don't interact with the user at all. These are usually
    called \textbf{scripts} and do a series of commands to speed up
    multi-step processes. However, our programs will often have a
    \textbf{user interface} that the user can interact with. We will
    output information to the screen and get input from the user.
    
    ~\\
    To be able to do input and output in C++, we need to make sure to
    include the \textbf{iostream} library at the top of our program.
    
\begin{lstlisting}[style=code]
#include <iostream>		// cout and cin
using namespace std;

int main()
{
	// Program code goes here

	return 0;
}
\end{lstlisting}

	\subsection{Outputting data}
	
	We use the \texttt{cout} command to output data to the screen.
	We also use \texttt{<<}, the \textbf{output-stream operator}
	to link \textbf{string literals} and \textbf{variables} in our
	output statement.
	
	\paragraph{Outputting a string literal:} 
	
	A \textbf{string literal} is a hard-coded string that you write in
	your program. String literals must be written in double-quotes,
	and will be displayed as-is.
   
~\\ Code:
\begin{lstlisting}[style=code]
cout << "Hello!" << endl;
\end{lstlisting}
~\\ Output:
\begin{lstlisting}[style=output]
Hello!
\end{lstlisting}
	
	\newpage
	\paragraph{Outputting a variable value:}
	
	If you use \texttt{cout} and a variable's name, it will display
	the value stored in the variable:
   
~\\ Code:
\begin{lstlisting}[style=code]
int cakes = 10;
cout << cakes << endl;
\end{lstlisting}
~\\ Output:
\begin{lstlisting}[style=output]
10
\end{lstlisting}

	Often, just outputting the variable on its own isn't enough
	data for the user. You will usually need to add a string literal
	to write out a label for what is being shown:

~\\ Code:
\begin{lstlisting}[style=code]
int cakes = 10;
cout << "Total cakes: " << cakes << endl;
\end{lstlisting}
~\\ Output:
\begin{lstlisting}[style=output]
Total cakes: 10
\end{lstlisting}

	\paragraph{New lines:}
	C++ won't automatically put line breaks in your program output.
	If you did several \texttt{cout} statements like this,
	the output would be all on the same line:

~\\ Code:
\begin{lstlisting}[style=code]
string name1 = "Microcenter";
string name2 = "TableTop";
string street = "Metcalf";

cout << name1;
cout << name2;
cout << street;
\end{lstlisting}
~\\ Output:
\begin{lstlisting}[style=output]
MicrocenterTableTopMetcalf
\end{lstlisting}

	You can manually add new lines by linking \texttt{endl} into your
	stream, or you can add the string literal \texttt{"\textbackslash n"}.
	
~\\ Code:
\begin{lstlisting}[style=code]
string name1 = "Microcenter";
string name2 = "TableTop";
string street = "Metcalf";

cout << name1 << endl;
cout << name2 << "\n";
cout << street;
\end{lstlisting}
~\\ Output:
\begin{lstlisting}[style=output]
Microcenter
TableTop
Metcalf
\end{lstlisting}

	\paragraph{Special characters:}
	You can add special characters in your string literal to do different things:
	
	\begin{center}
		\begin{tabular}{c l}
			\textbf{Character} 			& \textbf{What it does} \\ \hline
			\texttt{"\textbackslash n"} & New line \\
			\texttt{"\textbackslash t"} & Add a tab \\
			\texttt{"\textbackslash \textbackslash"} & Display a backslash \\
			\texttt{"\textbackslash " "} & Write a double-quote \\
			\texttt{"\textbackslash a"} & Make a bell sound \\
		\end{tabular}
	\end{center}
	
	\newpage
	\subsection{Getting input}
	One way to get input from the user is to use the  \texttt{cin} command and the \textbf{input-stream operator}
	\texttt{>>}. This is good for getting integers, floats, doubles, and one-word strings.
	Any time we get input from the user, we \underline{must} store their input in a variable somewhere,
	so make sure you declare a variable before using \texttt{cin}.

\begin{lstlisting}[style=code]
string username;
int age;
float money;

cout << "Enter your username: ";
cin >> name;

cout << "Enter your age: ";
cin >> age;

cout << "Enter your money: ";
cin >> money;
\end{lstlisting}

	Besides needing a variable declared before each \texttt{cin} statement,
	you should usually also display output with \texttt{cout} to let the
	user know that you're expecting some information from them.
	
	\paragraph{Getting a line of text}
	In some cases, you will want to get a \texttt{string} from the user
	that may include spaces. Using \texttt{cin >>} won't work on strings
	with spaces, so we have to use a special function: \texttt{getline}.
	
\begin{lstlisting}[style=code]
string fullname;
cout << "Enter your full name then press ENTER: ";
getline( cin, fullname );
\end{lstlisting}

	Using \texttt{getline} requires open and closing parentheses
	(because it's a function), and two pieces of data: \texttt{cin}
	and the variable to store the data in.
	
	\paragraph{Program errors:}
	
	If you inter-mix \texttt{cin >> ...} and \texttt{getline()},
	you will probably run into \textbf{buffer errors} in your program - 
	often, certain inputs will be skipped. When you're using both of these
	command types...
	
	\begin{center}
	\textbf{Always use \texttt{cin.ignore();} \\ before a \texttt{getline()}
	that was preceeded by a \texttt{cin >>}!}
	\end{center}




\end{document}
