\contentsline {chapter}{\numberline {1}Introduction to Object Oriented Programming}{2}%
\contentsline {section}{\numberline {1.1}Programming paradigms}{2}%
\contentsline {paragraph}{Machine code:}{2}%
\contentsline {paragraph}{Procedural Languages:}{2}%
\contentsline {paragraph}{Object Oriented Programming:}{2}%
\contentsline {section}{\numberline {1.2}Design ideals}{3}%
\contentsline {section}{\numberline {1.3}Example: Moving to an OOP design}{4}%
\contentsline {subparagraph}{Cons of this style:}{4}%
\contentsline {paragraph}{Video game}{8}%
\contentsline {paragraph}{College}{9}%
\contentsline {chapter}{\numberline {2}Structs}{10}%
\contentsline {section}{\numberline {2.1}Structs vs. Classes}{10}%
\contentsline {section}{\numberline {2.2}Example structs}{10}%
\contentsline {section}{\numberline {2.3}Declaring object variables}{12}%
\contentsline {chapter}{\numberline {3}Classes}{13}%
\contentsline {section}{\numberline {3.1}Class basics}{13}%
\contentsline {subsection}{\numberline {3.1.1}Accessibility}{13}%
\contentsline {subsection}{\numberline {3.1.2}Header and Source files}{14}%
\contentsline {subsubsection}{Defining member functions \textit {inside} the class declaration}{15}%
\contentsline {subsubsection}{Should you define functions in the .hpp or the .cpp?}{16}%
\contentsline {subsection}{\numberline {3.1.3}Function vs. Method}{17}%
\contentsline {subsection}{\numberline {3.1.4}Getters and Setters}{17}%
\contentsline {paragraph}{Setters:}{18}%
\contentsline {paragraph}{Getters:}{18}%
\contentsline {subsection}{\numberline {3.1.5}Constructors and Destructors}{19}%
\contentsline {paragraph}{Example: Text file wrapper}{19}%
\contentsline {subsection}{\numberline {3.1.6}Default Constructors}{22}%
\contentsline {subsection}{\numberline {3.1.7}Parameterized Constructors}{23}%
\contentsline {subsection}{\numberline {3.1.8}Copy Constructors}{24}%
\contentsline {paragraph}{Design:}{24}%
\contentsline {paragraph}{Default copy constructor:}{24}%
\contentsline {paragraph}{Ways to copy:}{25}%
\contentsline {subsection}{\numberline {3.1.9}Example program: House Builder}{26}%
\contentsline {paragraph}{Room.hpp:}{27}%
\contentsline {paragraph}{Room.cpp:}{28}%
\contentsline {paragraph}{House.hpp:}{29}%
\contentsline {paragraph}{House.cpp:}{30}%
\contentsline {paragraph}{main.cpp:}{31}%
\contentsline {section}{\numberline {3.2}More advanced topics}{32}%
\contentsline {subsection}{\numberline {3.2.1}Const member methods}{32}%
\contentsline {subsection}{\numberline {3.2.2}this}{33}%
\contentsline {subsection}{\numberline {3.2.3}Friends}{34}%
\contentsline {paragraph}{Friend function:}{34}%
\contentsline {paragraph}{Friend class:}{34}%
\contentsline {paragraph}{However - it doesn't go both ways.}{35}%
\contentsline {subsection}{\numberline {3.2.4}Operator Overloading}{36}%
\contentsline {subsubsection}{Arithmetic operators}{36}%
\contentsline {subsubsection}{Relational operators}{37}%
\contentsline {subsubsection}{Stream operators}{38}%
\contentsline {subsubsection}{Subscript operator}{39}%
\contentsline {subsubsection}{Assignment operator}{39}%
\contentsline {subsection}{\numberline {3.2.5}Static Variables and Functions}{41}%
