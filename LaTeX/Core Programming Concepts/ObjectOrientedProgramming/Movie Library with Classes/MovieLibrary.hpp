#ifndef _MOVIE_LIBRARY_HPP
#define _MOVIE_LIBRARY_HPP

#include "Movie.hpp"

#include <string>
using namespace std;

const int MAX_MOVIES = 100;

class MovieLibrary
{
    public:
    MovieLibrary();

    void AddMovie();
    void UpdateMovie();
    void ClearMovies();
    void DisplayAllMovies();
    void LoadList( string path );
    void SaveList( string path );

    private:
    int totalMovies;
    Movie movieList[ MAX_MOVIES ];
};

#endif
