#include "Room.hpp"

#include <iostream>
using namespace std;

void Room::Setup()
{
    cout << "Enter room name: ";
    cin >> name;

    cout << "Enter width: ";
    cin >> width;

    cout << "Enter length: ";
    cin >> length;
}

string Room::GetName()
{
    return name;
}

int Room::GetWidth()
{
    return width;
}

int Room::GetLength()
{
    return length;
}

int Room::GetArea()
{
    return width * length;
}
