#include "House.hpp"

#include <iostream>
#include <fstream>
using namespace std;

House::House()
{
    roomCount = 0;
}

House::~House()
{
    cout << "Outputted to house.txt" << endl;
    ofstream output( "house.txt" );
    for ( int i = 0; i < roomCount; i++ )
    {
        output << rooms[i].GetName() << "\t"
                << rooms[i].GetWidth() << "x"
                << rooms[i].GetLength() << "\t"
                << "(" << rooms[i].GetArea() << " sqft)" << endl;
    }
    output.close();
}

void House::AddRoom()
{
    if ( roomCount == MAX_ROOMS )
    {
        cout << "House is full! Cannot fit any more rooms!" << endl;
        return;
    }

    rooms[ roomCount ].Setup();
    roomCount++;
}
