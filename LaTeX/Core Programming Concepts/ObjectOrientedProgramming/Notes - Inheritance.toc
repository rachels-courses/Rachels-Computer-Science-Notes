\contentsline {chapter}{\numberline {1}Inheritance}{2}
\contentsline {section}{\numberline {1.1}Design ideas}{2}
\contentsline {subsection}{\numberline {1.1.1}Reusing functionality}{2}
\contentsline {subsection}{\numberline {1.1.2}The iostream and fstream}{3}
\contentsline {subsection}{\numberline {1.1.3}Inheritance (Is-A) vs. Composition (Has-A)}{3}
\contentsline {subsection}{\numberline {1.1.4}Example: Game objects}{4}
\contentsline {section}{\numberline {1.2}Implementing inheritance}{7}
\contentsline {subsection}{\numberline {1.2.1}Inheriting from one parent}{7}
\contentsline {subsection}{\numberline {1.2.2}Inheriting from multiple parents}{8}
\contentsline {subsection}{\numberline {1.2.3}Private, Public, and Protected}{9}
\contentsline {subsubsection}{Member access}{9}
\contentsline {subsubsection}{Inheritance style}{9}
\contentsline {subsection}{\numberline {1.2.4}Function Overriding}{10}
\contentsline {subsubsection}{Calling the parents' version of a method}{11}
\contentsline {subsection}{\numberline {1.2.5}Constructors and Destructors in the family}{12}
