

\begin{figure}[h]
    \begin{center}
        \includegraphics[width=12cm]{images/complexity.png}
    \end{center}
\end{figure}

    %-------------------------------------------------------------------%
    \section{Introduction: Algorithm efficiency (and why we care)}

        \paragraph{Processing power}
        continues increasing as time moves forward.
        Many things process so quickly that we barely notice it,
        if at all. That doesn't mean we can just write code
        as inefficiently as possible and just let it run because
        \textit{``computers are fast enough, right?''} -
        as technology evolves, we crunch more and more data,
        and as ``big data'' becomes a bigger field, we need
        to work with this data efficiently - because it doesn't
        matter how powerful a machine is, processing large
        quantities of data with an inefficient algorithm can
        still take a long time.

        \paragraph{And some computers today are slow.}
        Not usually the computers that the average person is
        using\footnote{Even though it might feel that way if they're
        running Windows.}. Some computers that handle systems
        are pretty basic, or they're small, and don't have the
        luxury of having great specs. Fitness trackers,
        dedicated GPS devices, a thermostat, etc. For
        systems like these, processing efficiency is important.

        \paragraph{Let's say our algorithm's time to execute
        increases \textit{quadratically}.} That means,
        as we increase the amount of items to process ($n$),
        the time to process that data goes up \textit{quadratically}.

        \begin{center}
            \begin{tikzpicture}
              \begin{axis}[grid=major,xmin=0,xmax=10,ymax=25,xlabel={Amount of data $n$},ylabel={Time complexity $O(n^2)$}]
                \addplot[red] {x^2};
              \end{axis}
            \end{tikzpicture}
        \end{center}

        For processing 1 piece of data, it takes 1 unit of time
        (we aren't focused so much on the actual unit of time,
        since each machine runs at different speeds) - that's
        fine. Processing 2 pieces of data? 4 units of time.
        8 pieces of data? 64 units of time. We don't just
        \textit{double} the amount of time it takes when
        we double the data - we \textit{square} it. ~\\

        How much data do you think is generated every day
        on a social media website that has millions of users?
        Now imagine the \textit{processing time} for an
        algorithm with a quadratic increase...

        \begin{center}
            \begin{tabular}{| c | c |} \hline
                \textbf{Data $n$} & \textbf{Time units} \\ \hline
                1 & 1 \\ \hline
                2 & 4 \\ \hline
                3 & 9 \\ \hline
                4 & 16 \\ \hline
                5 & 25 \\ \hline
                ... & ... \\ \hline
                100 & 10,000 \\ \hline
                1,000 & 1,000,000 \\ \hline
                10,000 & 100,000,000 \\ \hline
                1,000,000 & 1,000,000,000,000 \\ \hline
            \end{tabular}
        \end{center}

        \paragraph{From a design standpoint} we also need
        to know what the efficiency of different algorithms
        are (such as the algorithm to find data in a Linked List,
        or the algorithm to resize a dynamic array) in order
        to make design decisions on what is the best option
        for our software.

        \paragraph{As an example,} you shouldn't use a recursive
        algorithm to generate a fibonacci sequence\footnote{Each number $F_n$ in the sequence is equal to the sum of the two previous items: $F_n = F_{n-1} + F_{n-2}$.}.
        It's just not as efficient! Let's look at
        generating the string of numbers:

        \begin{center}
            1, 1, 2, 3, 5, 8, 13, 21, 34, 55
        \end{center}

        The most efficient way to do this would be with an 
        \textit{iterative} approach  using a loop. However,
        we \textit{could} also figure it out with a recursive
        approach, though the recursive approach is much less efficient.

        \newpage
        \paragraph{Fibonacci sequence, iterative:} ~\\

\begin{lstlisting}[style=code]
int GetFib_Iterative( int n )
{
    if ( n == 0 || n == 1 ) { return 1; }

    int n_prev2 = 1;    // F[n-2]
    int n_prev1 = 1;    // F[n-1]
    int ni;             // F[n]

    for ( int i = 2; i <= n; i++ )
    {
        ni = n_prev2 + n_prev1;
        n_prev2 = n_prev1;
        n_prev1 = ni;
    }

    return ni;
}
\end{lstlisting}

        This algorithm has a simple loop that iterates $n$ times
        to find a given number of the Fibonacci sequence.
        The amount of times the loop \textbf{iterates} based on $n$ is:

        \begin{center}
            \begin{tabular}{| l | c | c | c | c | c | c | c | c | c | c |} \hline
            $n$                     & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & ... & 20
            \\ \hline
            \textbf{iterations}     & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & ... & 19
            \\ \hline
            \end{tabular}
        \end{center}

        \paragraph{Fibonacci sequence, recursive:} ~\\

\begin{lstlisting}[style=code]
int GetFib_Recursive( int n )
{
    if ( n == 0 || n == 1 ) { return 1; }
    else
    {
        iters++;
        // F[n] = F[n-2] + F[n-1]
        int ni = GetFib_Recursive( n-2 ) + GetFib_Recursive( n-1 );
        return ni;
    }
}
\end{lstlisting}

        The way this version is written, any time we call this function
        with any value $n$, it has to compute the Fibonacci number for
        $Fib(n-1)$, $Fib(n-2)$, $Fib(n-3)$, ... $Fib(1)$, $Fib(0)$ ... \textit{twice}.
        This produces duplicate work because it effectively \textit{doesn't ``remember''}
        what $Fib(3)$ was after it computes it, so for $Fib(n)$ it has to recompute
        $Fib(n-1)$ and $Fib(n-2)$ each time.

        \begin{center}
            \begin{tabular}{| l | c | c | c | c | c | c | c | c | c | c |} \hline
            $n$                     & 3 & 4 & 5 & 6  & 7  & 8  & 9  & 10 & ... & 20
            \\ \hline
            \textbf{iterations}     & 2 & 4 & 7 & 12 & 20 & 33 & 54 & 88 & ... & 10,945
            \\ \hline
            \end{tabular}
        \end{center}

        The growth rate of the iterative version ends up being \textbf{linear}:
        As $n$ rises linearly, the amount of iterations also goes up \textbf{linearly}. ~\\

        The growth rate of the recursive version is \textbf{exponential}:
        As $n$ rises linearly, the amount of iteration goes up \textbf{exponentially}. ~\\

        On the next page, you can see the graph comparisons at two different
        comparisons to show the differences.

        \begin{center}
            \begin{tikzpicture}
              \begin{axis}[grid=major,xmin=3,xmax=20,ymax=25,xlabel={Fib($n$)},ylabel={Iterations}]
                \addplot[red] table {
                    3 2
                    4 4
                    5 7
                    6 12
                    7 20
                    8 33
                    %9 54
                    %10 88
                    %11 143
                    %12 232
                    %13 376
                    %14 609
                    %15 986
                    %16 1596
                    %17 2583
                    %18 4180
                    %19 6764
                    %20 10945
                };
                \addplot[blue,dashed] table {
                    3 2
                    4 3
                    5 4
                    6 5
                    7 6
                    8 7
                    9 8
                    10 9
                    11 10
                    12 11
                    13 12
                    14 13
                    15 14
                    16 15
                    17 16
                    18 17
                    19 18
                    20 19
                };
              \end{axis}
            \end{tikzpicture}
            ~\\ \textbf{Comparing iterative vs. recursive},
            ~\\ Iterative is blue/dashed, recursive is red/solid.
            ~\\ Y scale from 0 to 25 (the 0 to 25th Fibonacci number to generate).
        \end{center}

            For small amounts this might not be too bad. However, if we were generating
            a Fibonacci number further in the list, it would continue getting even slower...

        \begin{center}
            \begin{tikzpicture}
              \begin{axis}[grid=major,xmin=3,xmax=20,ymax=11000,xlabel={Fib($n$)},ylabel={Iterations}]
                \addplot[red] table {
                    3 2
                    4 4
                    5 7
                    6 12
                    7 20
                    8 33
                    9 54
                    10 88
                    11 143
                    12 232
                    13 376
                    14 609
                    15 986
                    16 1596
                    17 2583
                    18 4180
                    19 6764
                    20 10945
                };
                \addplot[blue,dashed] table {
                    3 2
                    4 3
                    5 4
                    6 5
                    7 6
                    8 7
                    9 8
                    10 9
                    11 10
                    12 11
                    13 12
                    14 13
                    15 14
                    16 15
                    17 16
                    18 17
                    19 18
                    20 19
                };
              \end{axis}
            \end{tikzpicture}
            ~\\ \textbf{Comparing iterative vs. recursive},
            ~\\ Iterative is blue/dashed, recursive is red/solid.
            ~\\ Y scale from 0 to 11,000 (The 0 to 11,000th Fibonacci number to generate).
        \end{center}

        If we are trying to generate the 11,000th item in the sequence,
        the iterative approach requires 11,000 iterations in a loop.
        That linear increase is still \textit{much faster} than Each
        $Fib(n)$ calling $Fib(n-1)$ and $Fib(n-2)$ recursively.

    \hrulefill
    %-------------------------------------------------------------------%
    %\chapter{Counting iterations - Running time T(n)}

    %-------------------------------------------------------------------%
    \section{Big-O Notation and Growth Rates}

        For the most part, we don't care much about the exact amount
        of times a loop runs. After all, while the program is running,
        $n$ could be changing depending on user input, or how much data is stored,
        or other scenarios. Instead, we are more interested in looking
        at the big picture: the generalized \textbf{growth rate} of the algorithm. ~\\

        We use something called ``Big-O notation'' to indicate the growth rate
        of an algorithm. Some of the rates we care about are:

        \begin{center}
            \begin{tabular}{ | c | c | c | c | c | c | } \hline
                $O(1)$ & $O(log(n))$ & $n$ & $n^2$ & $n^3$ & $2^n$
                \\ \hline
                Constant & Logarithmic & Linear & Quadratic & Cubic & Exponential
                \\ \hline
            \end{tabular}
        \end{center}

        \newpage

        \paragraph{Constant time, $O(1)$} ~\\~\\
        We think of any single command as being constant time.
        The operation
        $$ a = b + c $$ will take the same amount of computing time no matter what
        the values of $a$, $b$, and $c$ are.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}

\begin{lstlisting}[style=code]
int F(int x)
{
    return 3 * x + 2;
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=2,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] coordinates
              {(0,1) (20,1)};
          \end{axis}
        \end{tikzpicture}

        }

        \centering
    \end{subfigure}
\end{figure}

        \hrulefill

        \paragraph{Logarithmic time, $O(log(n))$} ~\\~\\
        Having an algorithm that halves its range
        each iteration of the loop will result in
        a logarithmic growth rate.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}

\begin{lstlisting}[style=code]
int Search(int l, int r,
    int search, int arr[])
{
  while ( l <= r )
  {
    int m = l + (r-l) / 2;
    if ( arr[m] == search )
    { return m; }
    else if ( arr[m] < search )
    { l = m+1; }
    else if ( arr[m] > search )
    { r = m-1; }
  }
  return -1;
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
        %\begin{mdframed}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=2,xticklabel=\empty,yticklabel=\empty]
            \addplot[red] {log10(x)};
          \end{axis}
        \end{tikzpicture}

        }

        %\end{mdframed}
        \centering
    \end{subfigure}
\end{figure}




        \newpage

        \paragraph{Linear time, $O(n)$} ~\\~\\
        Having a single loop that iterates from
        start to end from 0 to $n$ with no interruptions
        (like breaks) will be a linear time.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}


\begin{lstlisting}[style=code]
int Sum( int n )
{
  int sum = 0;
  for (int i=1; i<=n; i++)
  {
    sum += n;
  }
  return sum;
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=5,ymax=5,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] coordinates
              {(0,0) (5,5)};
          \end{axis}
        \end{tikzpicture}

        }
    \end{subfigure}
\end{figure}

        If there is some scenario that causes the loop
        to halve its range each time, then its growth rate
        would be \textit{less than linear}: as $O(log(n))$.

        \hrulefill

        \paragraph{Quadratic time, $O(n^2)$} ~\\~\\
        Quadratic time comes into play when you have
        one loop iterating $n$ times nested within an
        other loop that also iterates $n$ times.
        For example, if we were writing out times tables from 1 to 10 ($n = 10$),
        then we would need 10 rows and 10 columns, giving us $10^2 = 100$ cells.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}


\begin{lstlisting}[style=code]
void TimesTables(int n)
{
  for (int y=1; y<=n; y++)
  {
    for (int x=1; x<=n; x++)
    {
      cout << x << "*"
           << y << "="
           << x*y << "\t";
    }
    cout << endl;
  }
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=20,ymax=20,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] {x^2};
          \end{axis}
        \end{tikzpicture}

        }

        \centering
        \resizebox{.5\textwidth}{!}{%

        \begin{tikzpicture}
            \draw (0,0) -- (10,0) -- (10,10) -- (0,10) -- (0,0);

            \draw (1,0) -- (1,10); \draw (2,0) -- (2,10); \draw (3,0) -- (3,10);
            \draw (4,0) -- (4,10); \draw (5,0) -- (5,10); \draw (6,0) -- (6,10);
            \draw (7,0) -- (7,10); \draw (8,0) -- (8,10); \draw (9,0) -- (9,10);

            \draw (0,1) -- (10,1); \draw (0,2) -- (10,2); \draw (0,3) -- (10,3);
            \draw (0,4) -- (10,4); \draw (0,5) -- (10,5); \draw (0,6) -- (10,6);
            \draw (0,7) -- (10,7); \draw (0,8) -- (10,8); \draw (0,9) -- (10,9);

            \node at (0.5, 10.5) { 1 }; \node at (1.5, 10.5) { 2 }; \node at (2.5, 10.5) { 3 };
            \node at (3.5, 10.5) { 4 }; \node at (4.5, 10.5) { 5 }; \node at (5.5, 10.5) { 6 };
            \node at (6.5, 10.5) { 7 }; \node at (7.5, 10.5) { 8 }; \node at (8.5, 10.5) { 9 }; \node at (9.5, 10.5) { 10 };

            \node at (-0.5, 9.5) { 1 }; \node at (-0.5, 8.5) { 2 };\node at (-0.5, 7.5) { 3 };
            \node at (-0.5, 6.5) { 4 }; \node at (-0.5, 5.5) { 5 };\node at (-0.5, 4.5) { 6 };
            \node at (-0.5, 3.5) { 7 }; \node at (-0.5, 2.5) { 8 };\node at (-0.5, 1.5) { 9 };\node at (-0.5, 0.5) { 10 };

            \node at (0.5, 9.5) { 1 }; \node at (1.5, 9.5) { 2 }; \node at (2.5, 9.5) { 3 }; \node at (3.5, 9.5) { 4 }; \node at (4.5, 9.5) { 5 }; \node at (5.5, 9.5) { 6 }; \node at (6.5, 9.5) { 7 }; \node at (7.5, 9.5) { 8 }; \node at (8.5, 9.5) { 9 }; \node at (9.5, 9.5) { 10 };
            \node at (0.5, 8.5) { 2 }; \node at (1.5, 8.5) { 4 }; \node at (2.5, 8.5) { 6 }; \node at (3.5, 8.5) { 8 }; \node at (4.5, 8.5) { 10}; \node at (5.5, 8.5) { 12}; \node at (6.5, 8.5) { 14}; \node at (7.5, 8.5) { 16}; \node at (8.5, 8.5) { 18}; \node at (9.5, 8.5) { 20 };
            \node at (0.5, 7.5) { 3 }; \node at (1.5, 7.5) { 6 }; \node at (2.5, 7.5) { 3 }; \node at (3.5, 7.5) { 12}; \node at (4.5, 7.5) { 15}; \node at (5.5, 7.5) { 18}; \node at (6.5, 7.5) { 21}; \node at (7.5, 7.5) { 24}; \node at (8.5, 7.5) { 27}; \node at (9.5, 7.5) { 30 };
            \node at (0.5, 6.5) { 4 }; \node at (1.5, 6.5) { 8 }; \node at (2.5, 6.5) { 12}; \node at (3.5, 6.5) { 16}; \node at (4.5, 6.5) { 20}; \node at (5.5, 6.5) { 24}; \node at (6.5, 6.5) { 28}; \node at (7.5, 6.5) { 32}; \node at (8.5, 6.5) { 36}; \node at (9.5, 6.5) { 40 };
            \node at (0.5, 5.5) { 5 }; \node at (1.5, 5.5) { 10}; \node at (2.5, 5.5) { 15}; \node at (3.5, 5.5) { 20}; \node at (4.5, 5.5) { 25}; \node at (5.5, 5.5) { 30}; \node at (6.5, 5.5) { 35}; \node at (7.5, 5.5) { 40}; \node at (8.5, 5.5) { 45}; \node at (9.5, 5.5) { 50 };
            \node at (0.5, 4.5) { 6 }; \node at (1.5, 4.5) { 12}; \node at (2.5, 4.5) { 18}; \node at (3.5, 4.5) { 24}; \node at (4.5, 4.5) { 30}; \node at (5.5, 4.5) { 36}; \node at (6.5, 4.5) { 42}; \node at (7.5, 4.5) { 48}; \node at (8.5, 4.5) { 54}; \node at (9.5, 4.5) { 60 };
            \node at (0.5, 3.5) { 7 }; \node at (1.5, 3.5) { 14}; \node at (2.5, 3.5) { 21}; \node at (3.5, 3.5) { 28}; \node at (4.5, 3.5) { 35}; \node at (5.5, 3.5) { 42}; \node at (6.5, 3.5) { 49}; \node at (7.5, 3.5) { 56}; \node at (8.5, 3.5) { 63}; \node at (9.5, 3.5) { 70 };
            \node at (0.5, 2.5) { 8 }; \node at (1.5, 2.5) { 16}; \node at (2.5, 2.5) { 24}; \node at (3.5, 2.5) { 32}; \node at (4.5, 2.5) { 40}; \node at (5.5, 2.5) { 48}; \node at (6.5, 2.5) { 56}; \node at (7.5, 2.5) { 64}; \node at (8.5, 2.5) { 72}; \node at (9.5, 2.5) { 80 };
            \node at (0.5, 1.5) { 9 }; \node at (1.5, 1.5) { 18}; \node at (2.5, 1.5) { 27}; \node at (3.5, 1.5) { 36}; \node at (4.5, 1.5) { 45}; \node at (5.5, 1.5) { 54}; \node at (6.5, 1.5) { 63}; \node at (7.5, 1.5) { 72}; \node at (8.5, 1.5) { 81}; \node at (9.5, 1.5) { 90 };
            \node at (0.5, 0.5) { 10 };\node at (1.5, 0.5) { 20}; \node at (2.5, 0.5) { 30}; \node at (3.5, 0.5) { 40}; \node at (4.5, 0.5) { 50}; \node at (5.5, 0.5) { 60}; \node at (6.5, 0.5) { 70}; \node at (7.5, 0.5) { 80}; \node at (8.5, 0.5) { 90}; \node at (9.5, 0.5) { 100};
        \end{tikzpicture}

        }
    \end{subfigure}
\end{figure}



    \newpage

        \paragraph{Cubic time, $O(n^3)$} ~\\~\\
        Just like nesting two loops iterating $n$ times each gives us a $n^2$ result,
        having three nested loops iterating $n$ times each gives us a $O(n^3)$
        growth rate.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}


\begin{lstlisting}[style=code]
void CubicThing(int n)
{
  for (int z=1; z<=n; z++)
  {
    for (int y=1; y<=n; y++)
    {
      for (int x=1; x<=n; x++)
      {
        cout << x << " "
             << y << " "
             << z << endl;
      }
    }
  }
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=20,ymax=20,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] {x^3};
          \end{axis}
        \end{tikzpicture}

        }
    \end{subfigure}
\end{figure}

    \hrulefill

        \paragraph{Exponential time, $O(2^n)$} ~\\~\\
        With an exponential function, each step \textit{increases} the complexity
        of the operation. The Fibonacci example is a good illustration:
        Figuring out \texttt{Fib(0)} and \texttt{Fib(1)} are constant,
        but \texttt{Fib(2)} requires calling \texttt{Fib(0)} and \texttt{Fib(1)},
        and \texttt{Fib(3)} calls \texttt{Fib(1)} and \texttt{Fib(2)},
        with \texttt{Fib(2)} calling \texttt{Fib(0)} and \texttt{Fib(1)},
        and each iteration adds that many more operations.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}


\begin{lstlisting}[style=code]
int Fib(int n)
{
  if (n == 0 || n == 1)
    return 1;

  return
    Exponential_Fib(n-2)
    + Exponential_Fib(n-1);
}
\end{lstlisting}

    \end{subfigure}%
    \begin{subfigure}{.02\textwidth} \tab[0.1cm] \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}
          \begin{axis}[xmin=0,ymin=0,xmax=20,ymax=20,xticklabel=\empty,yticklabel=\empty,]
            \addplot[red] {2^x};
          \end{axis}
        \end{tikzpicture}

        }
    \end{subfigure}
\end{figure}

    \newpage

        \paragraph{Growth rate comparisons:}
        ~\\

        \resizebox{\textwidth}{!}{%

        \begin{tikzpicture}[
              line1/.style={shape=circle, draw=colorblind_dark, line width=2},
              line2/.style={shape=circle, draw=colorblind_medium_blue, line width=2},
              line3/.style={shape=circle, draw=colorblind_light_orange, line width=2},
              line4/.style={shape=circle, draw=colorblind_medium_orange, dashed, line width=2},
            ]

          \begin{axis}[xmin=0,ymin=0,xmax=15,ymax=4000]
            \addplot[domain=0:15,line1]      {x^2};
            \addplot[domain=0:15,line2]      {x^3};
            \addplot[domain=0:15,line3]      {2^x};
            \addplot[domain=0:15,line4]      {x};
          \end{axis}

          \matrix [fill=white,draw,below left] at (current bounding box.north west) {
              \node [line4,label=right:$x$] {}; \\
              \node [line1,label=right:$x^2$] {}; \\
              \node [line2,label=right:$x^3$] {}; \\
              \node [line3,label=right:$2^x$] {}; \\
            };

        \end{tikzpicture}

        }~\\
