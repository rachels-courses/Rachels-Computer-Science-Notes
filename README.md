# Rachels Discrete Math Books

BOOKS:

* [Core Programming Concepts](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf) (CS200, CS235, some CS250)
* [Data Structures Notes](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Data%20Structures/Rachel's%20Data%20Structures%20Notes.pdf) (CS250)
* [Discrete Math Notes](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Discrete%20Math/Rachel's%20Discrete%20Math%20Notes.pdf) (CS210)
